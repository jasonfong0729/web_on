.
├── next-ts-app
│   ├── -AllHosts​​​​​​​
│   ├── api
│   │   ├── admin.ts
│   │   ├── customer.ts
│   │   ├── dashboard.ts
│   │   ├── report.ts
│   │   ├── transaction.ts
│   │   ├── transactionFixedAsset.ts
│   │   ├── transactionInventory.ts
│   │   ├── url.ts
│   │   ├── user.ts
│   │   └── vendor.ts
│   ├── components
│   │   ├── Footer.tsx
│   │   ├── Header.tsx
│   │   ├── Layout.tsx
│   │   ├── NavItem.tsx
│   │   ├── Sidebar.tsx
│   │   ├── admin
│   │   │   ├── Usertable.tsx
│   │   │   └── adminModel.ts
│   │   ├── bill
│   │   │   ├── BillTable.tsx
│   │   │   └── billData.tsx
│   │   ├── customers
│   │   │   ├── CustomersTable.tsx
│   │   │   └── customersData.tsx
│   │   ├── dashboard
│   │   │   ├── ActiveTimeline.tsx
│   │   │   ├── AddTransaction.tsx
│   │   │   ├── CashFlow.tsx
│   │   │   ├── MiniStatistic.tsx
│   │   │   ├── MyProfile.tsx
│   │   │   ├── RTransactions.tsx
│   │   │   └── Searchbar.tsx
│   │   ├── datepicker
│   │   │   └── DatePicker.tsx
│   │   ├── invoice
│   │   │   ├── InvoiceTable.tsx
│   │   │   ├── InvoicesRow.tsx
│   │   │   ├── invoiceData.tsx
│   │   │   └── invoiceModal.tsx
│   │   ├── profile
│   │   │   ├── Header.tsx
│   │   │   ├── PersonalInfo.tsx
│   │   │   └── model.tsx
│   │   ├── selectInput
│   │   │   └── SelectInput.tsx
│   │   ├── transaction
│   │   │   ├── AlertDeleteDialog.tsx
│   │   │   ├── DrawerEdit.tsx
│   │   │   ├── TransDialog
│   │   │   │   ├── QuestionCard.tsx
│   │   │   │   ├── basicDialog.tsx
│   │   │   │   ├── fixedAsset.tsx
│   │   │   │   └── inventory.tsx
│   │   │   ├── TransTable.tsx
│   │   │   └── transData.tsx
│   │   ├── vendorss
│   │   │   ├── VendorsRow.tsx
│   │   │   ├── VendorsTable.tsx
│   │   │   └── vendorsData.tsx
│   │   ├── withAdmin.tsx
│   │   └── withAuth.tsx
│   ├── context
│   │   └── state.tsx
│   ├── helpers
│   │   ├── Scroll.tsx
│   │   ├── checkIOS.tsx
│   │   ├── checkStandalone.ts
│   │   └── lorem.tsx
│   ├── hooks
│   │   ├── useAddToHomescreenPrompt.ts
│   │   ├── useRedirect.ts
│   │   └── useWindowsSize.ts
│   ├── lib
│   │   └── gtag.js
│   ├── next-env.d.ts
│   ├── next-i18next.config.js
│   ├── next.config.js
│   ├── out
│   │   ├── 404.html
│   │   ├── 500.html
│   │   ├── AdminPage.html
│   │   ├── Expiry.html
│   │   ├── _next
│   │   │   ├── Rg9j1SWL7JN-c6qOmzaRe
│   │   │   └── static
│   │   │       ├── Rg9j1SWL7JN-c6qOmzaRe
│   │   │       │   ├── _buildManifest.js
│   │   │       │   ├── _middlewareManifest.js
│   │   │       │   └── _ssgManifest.js
│   │   │       ├── chunks
│   │   │       │   ├── 0c428ae2-b4f47c5d25bc1a4c.js
│   │   │       │   ├── 0f1ac474-e73bdfdce9383804.js
│   │   │       │   ├── 1a48c3c1-b58050ff0ce6c548.js
│   │   │       │   ├── 1bfc9850-ebb5cc61cabf28ca.js
│   │   │       │   ├── 209-b50c927eaf93f7c4.js
│   │   │       │   ├── 2438-16c343b45a170369.js
│   │   │       │   ├── 252f366e-0576afd49cadc0bc.js
│   │   │       │   ├── 2773-d2f7f79aacccaac7.js
│   │   │       │   ├── 2893-8f6a9fd17f6f9e03.js
│   │   │       │   ├── 36bcf0ca-f7da0ba17f70f240.js
│   │   │       │   ├── 4215-deacac1c1e31080d.js
│   │   │       │   ├── 4452-f4456ed6757ae741.js
│   │   │       │   ├── 4689-8dd307c1ced8dbf3.js
│   │   │       │   ├── 4769-d98ec9d32413978f.js
│   │   │       │   ├── 5333-9bdf8e8a3e4b4df4.js
│   │   │       │   ├── 5891-13a90bf63d58e26b.js
│   │   │       │   ├── 7061-76619d79586ebca1.js
│   │   │       │   ├── 7398-afe762cb43125700.js
│   │   │       │   ├── 75fc9c18-db1eb17a2fd0d52e.js
│   │   │       │   ├── 7814-08d801723c416b9b.js
│   │   │       │   ├── 7983-2f3fea5cf4ff4d07.js
│   │   │       │   ├── 8527-a8550bc4115f5aba.js
│   │   │       │   ├── 9624-527c6eab0f7ecc25.js
│   │   │       │   ├── 9736-89d0f23991c246f1.js
│   │   │       │   ├── ae51ba48-ff7f6b3b9310c8ff.js
│   │   │       │   ├── d64684d8-f628ced66f25a17d.js
│   │   │       │   ├── d7eeaac4-e7b9e267c60d2262.js
│   │   │       │   ├── framework-89f2f7d214569455.js
│   │   │       │   ├── main-13696be18266d115.js
│   │   │       │   ├── pages
│   │   │       │   │   ├── 404-587b35672491aedb.js
│   │   │       │   │   ├── 500-9796f44d3f0467ac.js
│   │   │       │   │   ├── AdminPage-77c2d909b8b00d20.js
│   │   │       │   │   ├── Expiry-1df2374ca03464e2.js
│   │   │       │   │   ├── _app-d4056cf8518f9a6e.js
│   │   │       │   │   ├── _error-7921b3c054354aa5.js
│   │   │       │   │   ├── aboutAuth-c68355f3853e4f41.js
│   │   │       │   │   ├── bill-49a991b4cb1aa3a1.js
│   │   │       │   │   ├── billForm-e74a9361fbe6e011.js
│   │   │       │   │   ├── contact-1ae24c511e45157e.js
│   │   │       │   │   ├── customers
│   │   │       │   │   │   └── [customerId]-f9aa5a2fc38c57e2.js
│   │   │       │   │   ├── customers-75910516544d25a1.js
│   │   │       │   │   ├── customersForm-bd860627f0334b5c.js
│   │   │       │   │   ├── dashboard-6b3963b9ee373f17.js
│   │   │       │   │   ├── index-f1712a7a64d8977f.js
│   │   │       │   │   ├── invoice-dba64125233cea84.js
│   │   │       │   │   ├── invoiceForm-6770c2109a6e1723.js
│   │   │       │   │   ├── profile-5b8c13ef9b7bf4b3.js
│   │   │       │   │   ├── report-51fbc03009e7db0f.js
│   │   │       │   │   ├── signin-7fdf4fa922152f23.js
│   │   │       │   │   ├── signup-581302747d8b7d7a.js
│   │   │       │   │   ├── transaction-9d933c436fd7fe6b.js
│   │   │       │   │   ├── vendors-5e23b4ac332f97e2.js
│   │   │       │   │   └── vendorsForm-caa82b82ee86f087.js
│   │   │       │   ├── polyfills-5cd94c89d3acac5f.js
│   │   │       │   └── webpack-309fbebe2073f18c.js
│   │   │       └── css
│   │   │           └── 5af79a3fe60cca86.css
│   │   ├── aboutAuth.html
│   │   ├── app.css
│   │   ├── bill.html
│   │   ├── billForm.html
│   │   ├── contact.html
│   │   ├── customers
│   │   │   └── [customerId].html
│   │   ├── customers.html
│   │   ├── customersForm.html
│   │   ├── dashboard.html
│   │   ├── images
│   │   │   ├── BgSignUp.png
│   │   │   ├── EB-logo.png
│   │   │   ├── ProfileBackground.png
│   │   │   ├── avatar
│   │   │   │   ├── danielho.jpg
│   │   │   │   └── default.png
│   │   │   ├── signInImage.png
│   │   │   ├── signInImage2.png
│   │   │   └── site_logo.svg
│   │   ├── index.html
│   │   ├── invoice.html
│   │   ├── invoiceForm.html
│   │   ├── locales
│   │   │   ├── de
│   │   │   │   ├── common.json
│   │   │   │   ├── footer.json
│   │   │   │   └── second-page.json
│   │   │   └── en
│   │   │       ├── common.json
│   │   │       ├── footer.json
│   │   │       └── second-page.json
│   │   ├── profile.html
│   │   ├── report.html
│   │   ├── signin.html
│   │   ├── signup.html
│   │   ├── sw.js
│   │   ├── transaction.html
│   │   ├── vendors.html
│   │   ├── vendorsForm.html
│   │   └── workbox-6316bd60.js
│   ├── package.json
│   ├── pages
│   │   ├── 404.tsx
│   │   ├── 500.tsx
│   │   ├── AdminPage.tsx
│   │   ├── Expiry.tsx
│   │   ├── _app.tsx
│   │   ├── _document.tsx
│   │   ├── aboutAuth.tsx
│   │   ├── bill.tsx
│   │   ├── billForm.tsx
│   │   ├── contact.tsx
│   │   ├── customers.tsx
│   │   ├── customersForm.tsx
│   │   ├── dashboard.tsx
│   │   ├── index.tsx
│   │   ├── invoice.tsx
│   │   ├── invoiceForm.tsx
│   │   ├── profile.tsx
│   │   ├── report.tsx
│   │   ├── signin.tsx
│   │   ├── signup.tsx
│   │   ├── transaction.tsx
│   │   ├── vendors.tsx
│   │   └── vendorsForm.tsx
│   ├── postcss.config.js
│   ├── prettier.config.js
│   ├── public
│   │   ├── app.css
│   │   ├── images
│   │   │   ├── BgSignUp.png
│   │   │   ├── EB-logo.png
│   │   │   ├── ProfileBackground.png
│   │   │   ├── avatar
│   │   │   │   ├── danielho.jpg
│   │   │   │   └── default.png
│   │   │   ├── signInImage.png
│   │   │   ├── signInImage2.png
│   │   │   └── site_logo.svg
│   │   └── locales
│   │       ├── de
│   │       │   ├── common.json
│   │       │   ├── footer.json
│   │       │   └── second-page.json
│   │       └── en
│   │           ├── common.json
│   │           ├── footer.json
│   │           └── second-page.json
│   ├── react-table-config.d.ts
│   ├── redux
│   │   ├── admin
│   │   │   ├── actions.ts
│   │   │   ├── model.ts
│   │   │   ├── reducer.ts
│   │   │   ├── state.ts
│   │   │   └── thunks.ts
│   │   ├── auth
│   │   │   ├── actions.ts
│   │   │   ├── model.ts
│   │   │   ├── reducer.ts
│   │   │   ├── state.ts
│   │   │   └── thunks.ts
│   │   ├── customer
│   │   │   ├── actions.ts
│   │   │   ├── reducer.ts
│   │   │   ├── state.ts
│   │   │   └── thunk.ts
│   │   ├── dashboard
│   │   │   ├── actions.ts
│   │   │   ├── reducer.ts
│   │   │   ├── state.ts
│   │   │   └── thunk.ts
│   │   ├── redirect
│   │   │   ├── actions.ts
│   │   │   ├── reducer.ts
│   │   │   └── state.ts
│   │   ├── report
│   │   │   ├── actions.ts
│   │   │   ├── reducer.ts
│   │   │   ├── state.ts
│   │   │   └── thunk.ts
│   │   ├── store.ts
│   │   ├── transaction
│   │   │   ├── actions.ts
│   │   │   ├── reducer.ts
│   │   │   ├── state.ts
│   │   │   └── thunk.ts
│   │   ├── transactionFixedAsset
│   │   │   ├── actions.ts
│   │   │   ├── reducer.ts
│   │   │   ├── state.ts
│   │   │   └── thunk.ts
│   │   ├── transactionInventory
│   │   │   ├── actions.ts
│   │   │   ├── reducer.ts
│   │   │   ├── state.ts
│   │   │   └── thunk.ts
│   │   └── vendor
│   │       ├── actions.ts
│   │       ├── reducer.ts
│   │       ├── state.ts
│   │       └── thunk.ts
│   ├── styles
│   │   ├── Home.module.css
│   │   ├── date-picker.css
│   │   ├── globals.css
│   │   ├── globals.scss
│   │   └── scss_files
│   │       ├── _global_styles.scss
│   │       ├── _gmap_marker.scss
│   │       ├── _variables.scss
│   │       └── bootstrap-5.0.2
│   │           ├── CODE_OF_CONDUCT.md
│   │           ├── LICENSE
│   │           ├── README.md
│   │           ├── SECURITY.md
│   │           ├── composer.json
│   │           ├── config.yml
│   │           ├── dist
│   │           │   ├── css
│   │           │   │   ├── bootstrap-grid.css
│   │           │   │   ├── bootstrap-grid.css.map
│   │           │   │   ├── bootstrap-grid.min.css
│   │           │   │   ├── bootstrap-grid.min.css.map
│   │           │   │   ├── bootstrap-grid.rtl.css
│   │           │   │   ├── bootstrap-grid.rtl.css.map
│   │           │   │   ├── bootstrap-grid.rtl.min.css
│   │           │   │   ├── bootstrap-grid.rtl.min.css.map
│   │           │   │   ├── bootstrap-reboot.css
│   │           │   │   ├── bootstrap-reboot.css.map
│   │           │   │   ├── bootstrap-reboot.min.css
│   │           │   │   ├── bootstrap-reboot.min.css.map
│   │           │   │   ├── bootstrap-reboot.rtl.css
│   │           │   │   ├── bootstrap-reboot.rtl.css.map
│   │           │   │   ├── bootstrap-reboot.rtl.min.css
│   │           │   │   ├── bootstrap-reboot.rtl.min.css.map
│   │           │   │   ├── bootstrap-utilities.css
│   │           │   │   ├── bootstrap-utilities.css.map
│   │           │   │   ├── bootstrap-utilities.min.css
│   │           │   │   ├── bootstrap-utilities.min.css.map
│   │           │   │   ├── bootstrap-utilities.rtl.css
│   │           │   │   ├── bootstrap-utilities.rtl.css.map
│   │           │   │   ├── bootstrap-utilities.rtl.min.css
│   │           │   │   ├── bootstrap-utilities.rtl.min.css.map
│   │           │   │   ├── bootstrap.css
│   │           │   │   ├── bootstrap.css.map
│   │           │   │   ├── bootstrap.min.css
│   │           │   │   ├── bootstrap.min.css.map
│   │           │   │   ├── bootstrap.rtl.css
│   │           │   │   ├── bootstrap.rtl.css.map
│   │           │   │   ├── bootstrap.rtl.min.css
│   │           │   │   └── bootstrap.rtl.min.css.map
│   │           │   └── js
│   │           │       ├── bootstrap.bundle.js
│   │           │       ├── bootstrap.bundle.js.map
│   │           │       ├── bootstrap.bundle.min.js
│   │           │       ├── bootstrap.bundle.min.js.map
│   │           │       ├── bootstrap.esm.js
│   │           │       ├── bootstrap.esm.js.map
│   │           │       ├── bootstrap.esm.min.js
│   │           │       ├── bootstrap.esm.min.js.map
│   │           │       ├── bootstrap.js
│   │           │       ├── bootstrap.js.map
│   │           │       ├── bootstrap.min.js
│   │           │       └── bootstrap.min.js.map
│   │           ├── js
│   │           │   ├── dist
│   │           │   │   ├── alert.js
│   │           │   │   ├── alert.js.map
│   │           │   │   ├── base-component.js
│   │           │   │   ├── base-component.js.map
│   │           │   │   ├── button.js
│   │           │   │   ├── button.js.map
│   │           │   │   ├── carousel.js
│   │           │   │   ├── carousel.js.map
│   │           │   │   ├── collapse.js
│   │           │   │   ├── collapse.js.map
│   │           │   │   ├── dom
│   │           │   │   │   ├── data.js
│   │           │   │   │   ├── data.js.map
│   │           │   │   │   ├── event-handler.js
│   │           │   │   │   ├── event-handler.js.map
│   │           │   │   │   ├── manipulator.js
│   │           │   │   │   ├── manipulator.js.map
│   │           │   │   │   ├── selector-engine.js
│   │           │   │   │   └── selector-engine.js.map
│   │           │   │   ├── dropdown.js
│   │           │   │   ├── dropdown.js.map
│   │           │   │   ├── modal.js
│   │           │   │   ├── modal.js.map
│   │           │   │   ├── offcanvas.js
│   │           │   │   ├── offcanvas.js.map
│   │           │   │   ├── popover.js
│   │           │   │   ├── popover.js.map
│   │           │   │   ├── scrollspy.js
│   │           │   │   ├── scrollspy.js.map
│   │           │   │   ├── tab.js
│   │           │   │   ├── tab.js.map
│   │           │   │   ├── toast.js
│   │           │   │   ├── toast.js.map
│   │           │   │   ├── tooltip.js
│   │           │   │   └── tooltip.js.map
│   │           │   ├── index.esm.js
│   │           │   ├── index.umd.js
│   │           │   ├── src
│   │           │   │   ├── alert.js
│   │           │   │   ├── base-component.js
│   │           │   │   ├── button.js
│   │           │   │   ├── carousel.js
│   │           │   │   ├── collapse.js
│   │           │   │   ├── dom
│   │           │   │   │   ├── data.js
│   │           │   │   │   ├── event-handler.js
│   │           │   │   │   ├── manipulator.js
│   │           │   │   │   └── selector-engine.js
│   │           │   │   ├── dropdown.js
│   │           │   │   ├── modal.js
│   │           │   │   ├── offcanvas.js
│   │           │   │   ├── popover.js
│   │           │   │   ├── scrollspy.js
│   │           │   │   ├── tab.js
│   │           │   │   ├── toast.js
│   │           │   │   ├── tooltip.js
│   │           │   │   └── util
│   │           │   │       ├── backdrop.js
│   │           │   │       ├── index.js
│   │           │   │       ├── sanitizer.js
│   │           │   │       └── scrollbar.js
│   │           │   └── tests
│   │           │       ├── README.md
│   │           │       ├── browsers.js
│   │           │       ├── helpers
│   │           │       │   └── fixture.js
│   │           │       ├── integration
│   │           │       │   ├── bundle-modularity.js
│   │           │       │   ├── bundle.js
│   │           │       │   ├── index.html
│   │           │       │   ├── rollup.bundle-modularity.js
│   │           │       │   └── rollup.bundle.js
│   │           │       ├── karma.conf.js
│   │           │       ├── unit
│   │           │       │   ├── alert.spec.js
│   │           │       │   ├── base-component.spec.js
│   │           │       │   ├── button.spec.js
│   │           │       │   ├── carousel.spec.js
│   │           │       │   ├── collapse.spec.js
│   │           │       │   ├── dom
│   │           │       │   │   ├── data.spec.js
│   │           │       │   │   ├── event-handler.spec.js
│   │           │       │   │   ├── manipulator.spec.js
│   │           │       │   │   └── selector-engine.spec.js
│   │           │       │   ├── dropdown.spec.js
│   │           │       │   ├── jquery.spec.js
│   │           │       │   ├── modal.spec.js
│   │           │       │   ├── offcanvas.spec.js
│   │           │       │   ├── popover.spec.js
│   │           │       │   ├── scrollspy.spec.js
│   │           │       │   ├── tab.spec.js
│   │           │       │   ├── toast.spec.js
│   │           │       │   ├── tooltip.spec.js
│   │           │       │   └── util
│   │           │       │       ├── backdrop.spec.js
│   │           │       │       ├── index.spec.js
│   │           │       │       ├── sanitizer.spec.js
│   │           │       │       └── scrollbar.spec.js
│   │           │       └── visual
│   │           │           ├── alert.html
│   │           │           ├── button.html
│   │           │           ├── carousel.html
│   │           │           ├── collapse.html
│   │           │           ├── dropdown.html
│   │           │           ├── modal.html
│   │           │           ├── popover.html
│   │           │           ├── scrollspy.html
│   │           │           ├── tab.html
│   │           │           ├── toast.html
│   │           │           └── tooltip.html
│   │           ├── nuget
│   │           │   ├── MyGet.ps1
│   │           │   ├── bootstrap.nuspec
│   │           │   ├── bootstrap.png
│   │           │   └── bootstrap.sass.nuspec
│   │           ├── package.js
│   │           ├── package.json
│   │           ├── scss
│   │           │   ├── _accordion.scss
│   │           │   ├── _alert.scss
│   │           │   ├── _badge.scss
│   │           │   ├── _breadcrumb.scss
│   │           │   ├── _button-group.scss
│   │           │   ├── _buttons.scss
│   │           │   ├── _card.scss
│   │           │   ├── _carousel.scss
│   │           │   ├── _close.scss
│   │           │   ├── _containers.scss
│   │           │   ├── _dropdown.scss
│   │           │   ├── _forms.scss
│   │           │   ├── _functions.scss
│   │           │   ├── _grid.scss
│   │           │   ├── _helpers.scss
│   │           │   ├── _images.scss
│   │           │   ├── _list-group.scss
│   │           │   ├── _mixins.scss
│   │           │   ├── _modal.scss
│   │           │   ├── _nav.scss
│   │           │   ├── _navbar.scss
│   │           │   ├── _offcanvas.scss
│   │           │   ├── _pagination.scss
│   │           │   ├── _popover.scss
│   │           │   ├── _progress.scss
│   │           │   ├── _reboot.scss
│   │           │   ├── _root.scss
│   │           │   ├── _spinners.scss
│   │           │   ├── _tables.scss
│   │           │   ├── _toasts.scss
│   │           │   ├── _tooltip.scss
│   │           │   ├── _transitions.scss
│   │           │   ├── _type.scss
│   │           │   ├── _utilities.scss
│   │           │   ├── _variables.scss
│   │           │   ├── bootstrap-grid.scss
│   │           │   ├── bootstrap-reboot.scss
│   │           │   ├── bootstrap-utilities.scss
│   │           │   ├── bootstrap.scss
│   │           │   ├── forms
│   │           │   │   ├── _floating-labels.scss
│   │           │   │   ├── _form-check.scss
│   │           │   │   ├── _form-control.scss
│   │           │   │   ├── _form-range.scss
│   │           │   │   ├── _form-select.scss
│   │           │   │   ├── _form-text.scss
│   │           │   │   ├── _input-group.scss
│   │           │   │   ├── _labels.scss
│   │           │   │   └── _validation.scss
│   │           │   ├── helpers
│   │           │   │   ├── _clearfix.scss
│   │           │   │   ├── _colored-links.scss
│   │           │   │   ├── _position.scss
│   │           │   │   ├── _ratio.scss
│   │           │   │   ├── _stretched-link.scss
│   │           │   │   ├── _text-truncation.scss
│   │           │   │   └── _visually-hidden.scss
│   │           │   ├── mixins
│   │           │   │   ├── _alert.scss
│   │           │   │   ├── _border-radius.scss
│   │           │   │   ├── _box-shadow.scss
│   │           │   │   ├── _breakpoints.scss
│   │           │   │   ├── _buttons.scss
│   │           │   │   ├── _caret.scss
│   │           │   │   ├── _clearfix.scss
│   │           │   │   ├── _color-scheme.scss
│   │           │   │   ├── _container.scss
│   │           │   │   ├── _deprecate.scss
│   │           │   │   ├── _forms.scss
│   │           │   │   ├── _gradients.scss
│   │           │   │   ├── _grid.scss
│   │           │   │   ├── _image.scss
│   │           │   │   ├── _list-group.scss
│   │           │   │   ├── _lists.scss
│   │           │   │   ├── _pagination.scss
│   │           │   │   ├── _reset-text.scss
│   │           │   │   ├── _resize.scss
│   │           │   │   ├── _table-variants.scss
│   │           │   │   ├── _text-truncate.scss
│   │           │   │   ├── _transition.scss
│   │           │   │   ├── _utilities.scss
│   │           │   │   └── _visually-hidden.scss
│   │           │   ├── utilities
│   │           │   │   └── _api.scss
│   │           │   └── vendor
│   │           │       └── _rfs.scss
│   │           └── site
│   │               ├── assets
│   │               │   ├── js
│   │               │   │   ├── application.js
│   │               │   │   ├── search.js
│   │               │   │   └── vendor
│   │               │   │       ├── anchor.min.js
│   │               │   │       └── clipboard.min.js
│   │               │   └── scss
│   │               │       ├── _ads.scss
│   │               │       ├── _algolia.scss
│   │               │       ├── _anchor.scss
│   │               │       ├── _brand.scss
│   │               │       ├── _buttons.scss
│   │               │       ├── _callouts.scss
│   │               │       ├── _clipboard-js.scss
│   │               │       ├── _colors.scss
│   │               │       ├── _component-examples.scss
│   │               │       ├── _content.scss
│   │               │       ├── _footer.scss
│   │               │       ├── _layout.scss
│   │               │       ├── _masthead.scss
│   │               │       ├── _navbar.scss
│   │               │       ├── _placeholder-img.scss
│   │               │       ├── _sidebar.scss
│   │               │       ├── _skippy.scss
│   │               │       ├── _subnav.scss
│   │               │       ├── _syntax.scss
│   │               │       ├── _toc.scss
│   │               │       ├── _variables.scss
│   │               │       └── docs.scss
│   │               ├── content
│   │               │   ├── 404.md
│   │               │   └── docs
│   │               │       ├── 5.0
│   │               │       │   ├── _index.html
│   │               │       │   ├── about
│   │               │       │   │   ├── brand.md
│   │               │       │   │   ├── license.md
│   │               │       │   │   ├── overview.md
│   │               │       │   │   ├── team.md
│   │               │       │   │   └── translations.md
│   │               │       │   ├── components
│   │               │       │   │   ├── accordion.md
│   │               │       │   │   ├── alerts.md
│   │               │       │   │   ├── badge.md
│   │               │       │   │   ├── breadcrumb.md
│   │               │       │   │   ├── button-group.md
│   │               │       │   │   ├── buttons.md
│   │               │       │   │   ├── card.md
│   │               │       │   │   ├── carousel.md
│   │               │       │   │   ├── close-button.md
│   │               │       │   │   ├── collapse.md
│   │               │       │   │   ├── dropdowns.md
│   │               │       │   │   ├── list-group.md
│   │               │       │   │   ├── modal.md
│   │               │       │   │   ├── navbar.md
│   │               │       │   │   ├── navs-tabs.md
│   │               │       │   │   ├── offcanvas.md
│   │               │       │   │   ├── pagination.md
│   │               │       │   │   ├── popovers.md
│   │               │       │   │   ├── progress.md
│   │               │       │   │   ├── scrollspy.md
│   │               │       │   │   ├── spinners.md
│   │               │       │   │   ├── toasts.md
│   │               │       │   │   └── tooltips.md
│   │               │       │   ├── content
│   │               │       │   │   ├── figures.md
│   │               │       │   │   ├── images.md
│   │               │       │   │   ├── reboot.md
│   │               │       │   │   ├── tables.md
│   │               │       │   │   └── typography.md
│   │               │       │   ├── customize
│   │               │       │   │   ├── color.md
│   │               │       │   │   ├── components.md
│   │               │       │   │   ├── css-variables.md
│   │               │       │   │   ├── optimize.md
│   │               │       │   │   ├── options.md
│   │               │       │   │   ├── overview.md
│   │               │       │   │   └── sass.md
│   │               │       │   ├── examples
│   │               │       │   │   ├── _index.md
│   │               │       │   │   ├── album
│   │               │       │   │   │   └── index.html
│   │               │       │   │   ├── album-rtl
│   │               │       │   │   │   └── index.html
│   │               │       │   │   ├── blog
│   │               │       │   │   │   ├── blog.css
│   │               │       │   │   │   ├── blog.rtl.css
│   │               │       │   │   │   └── index.html
│   │               │       │   │   ├── blog-rtl
│   │               │       │   │   │   └── index.html
│   │               │       │   │   ├── carousel
│   │               │       │   │   │   ├── carousel.css
│   │               │       │   │   │   ├── carousel.rtl.css
│   │               │       │   │   │   └── index.html
│   │               │       │   │   ├── carousel-rtl
│   │               │       │   │   │   └── index.html
│   │               │       │   │   ├── cheatsheet
│   │               │       │   │   │   ├── cheatsheet.css
│   │               │       │   │   │   ├── cheatsheet.js
│   │               │       │   │   │   ├── cheatsheet.rtl.css
│   │               │       │   │   │   └── index.html
│   │               │       │   │   ├── cheatsheet-rtl
│   │               │       │   │   │   └── index.html
│   │               │       │   │   ├── checkout
│   │               │       │   │   │   ├── form-validation.css
│   │               │       │   │   │   ├── form-validation.js
│   │               │       │   │   │   └── index.html
│   │               │       │   │   ├── checkout-rtl
│   │               │       │   │   │   └── index.html
│   │               │       │   │   ├── cover
│   │               │       │   │   │   ├── cover.css
│   │               │       │   │   │   └── index.html
│   │               │       │   │   ├── dashboard
│   │               │       │   │   │   ├── dashboard.css
│   │               │       │   │   │   ├── dashboard.js
│   │               │       │   │   │   ├── dashboard.rtl.css
│   │               │       │   │   │   └── index.html
│   │               │       │   │   ├── dashboard-rtl
│   │               │       │   │   │   ├── dashboard.js
│   │               │       │   │   │   └── index.html
│   │               │       │   │   ├── features
│   │               │       │   │   │   ├── features.css
│   │               │       │   │   │   ├── index.html
│   │               │       │   │   │   ├── unsplash-photo-1.jpg
│   │               │       │   │   │   ├── unsplash-photo-2.jpg
│   │               │       │   │   │   └── unsplash-photo-3.jpg
│   │               │       │   │   ├── grid
│   │               │       │   │   │   ├── grid.css
│   │               │       │   │   │   └── index.html
│   │               │       │   │   ├── headers
│   │               │       │   │   │   ├── headers.css
│   │               │       │   │   │   └── index.html
│   │               │       │   │   ├── heroes
│   │               │       │   │   │   ├── bootstrap-docs.png
│   │               │       │   │   │   ├── bootstrap-themes.png
│   │               │       │   │   │   ├── heroes.css
│   │               │       │   │   │   └── index.html
│   │               │       │   │   ├── jumbotron
│   │               │       │   │   │   └── index.html
│   │               │       │   │   ├── masonry
│   │               │       │   │   │   └── index.html
│   │               │       │   │   ├── navbar-bottom
│   │               │       │   │   │   └── index.html
│   │               │       │   │   ├── navbar-fixed
│   │               │       │   │   │   ├── index.html
│   │               │       │   │   │   └── navbar-top-fixed.css
│   │               │       │   │   ├── navbar-static
│   │               │       │   │   │   ├── index.html
│   │               │       │   │   │   └── navbar-top.css
│   │               │       │   │   ├── navbars
│   │               │       │   │   │   ├── index.html
│   │               │       │   │   │   └── navbar.css
│   │               │       │   │   ├── offcanvas-navbar
│   │               │       │   │   │   ├── index.html
│   │               │       │   │   │   ├── offcanvas.css
│   │               │       │   │   │   └── offcanvas.js
│   │               │       │   │   ├── pricing
│   │               │       │   │   │   ├── index.html
│   │               │       │   │   │   └── pricing.css
│   │               │       │   │   ├── product
│   │               │       │   │   │   ├── index.html
│   │               │       │   │   │   └── product.css
│   │               │       │   │   ├── sidebars
│   │               │       │   │   │   ├── index.html
│   │               │       │   │   │   ├── sidebars.css
│   │               │       │   │   │   └── sidebars.js
│   │               │       │   │   ├── sign-in
│   │               │       │   │   │   ├── index.html
│   │               │       │   │   │   └── signin.css
│   │               │       │   │   ├── starter-template
│   │               │       │   │   │   ├── index.html
│   │               │       │   │   │   └── starter-template.css
│   │               │       │   │   ├── sticky-footer
│   │               │       │   │   │   ├── index.html
│   │               │       │   │   │   └── sticky-footer.css
│   │               │       │   │   └── sticky-footer-navbar
│   │               │       │   │       ├── index.html
│   │               │       │   │       └── sticky-footer-navbar.css
│   │               │       │   ├── extend
│   │               │       │   │   ├── approach.md
│   │               │       │   │   └── icons.md
│   │               │       │   ├── forms
│   │               │       │   │   ├── checks-radios.md
│   │               │       │   │   ├── floating-labels.md
│   │               │       │   │   ├── form-control.md
│   │               │       │   │   ├── input-group.md
│   │               │       │   │   ├── layout.md
│   │               │       │   │   ├── overview.md
│   │               │       │   │   ├── range.md
│   │               │       │   │   ├── select.md
│   │               │       │   │   └── validation.md
│   │               │       │   ├── getting-started
│   │               │       │   │   ├── accessibility.md
│   │               │       │   │   ├── best-practices.md
│   │               │       │   │   ├── browsers-devices.md
│   │               │       │   │   ├── build-tools.md
│   │               │       │   │   ├── contents.md
│   │               │       │   │   ├── download.md
│   │               │       │   │   ├── introduction.md
│   │               │       │   │   ├── javascript.md
│   │               │       │   │   ├── parcel.md
│   │               │       │   │   ├── rfs.md
│   │               │       │   │   ├── rtl.md
│   │               │       │   │   └── webpack.md
│   │               │       │   ├── helpers
│   │               │       │   │   ├── clearfix.md
│   │               │       │   │   ├── colored-links.md
│   │               │       │   │   ├── position.md
│   │               │       │   │   ├── ratio.md
│   │               │       │   │   ├── stretched-link.md
│   │               │       │   │   ├── text-truncation.md
│   │               │       │   │   └── visually-hidden.md
│   │               │       │   ├── layout
│   │               │       │   │   ├── breakpoints.md
│   │               │       │   │   ├── columns.md
│   │               │       │   │   ├── containers.md
│   │               │       │   │   ├── grid.md
│   │               │       │   │   ├── gutters.md
│   │               │       │   │   ├── utilities.md
│   │               │       │   │   └── z-index.md
│   │               │       │   ├── migration.md
│   │               │       │   └── utilities
│   │               │       │       ├── api.md
│   │               │       │       ├── background.md
│   │               │       │       ├── borders.md
│   │               │       │       ├── colors.md
│   │               │       │       ├── display.md
│   │               │       │       ├── flex.md
│   │               │       │       ├── float.md
│   │               │       │       ├── interactions.md
│   │               │       │       ├── overflow.md
│   │               │       │       ├── position.md
│   │               │       │       ├── shadows.md
│   │               │       │       ├── sizing.md
│   │               │       │       ├── spacing.md
│   │               │       │       ├── text.md
│   │               │       │       ├── vertical-align.md
│   │               │       │       └── visibility.md
│   │               │       ├── _index.html
│   │               │       └── versions.md
│   │               ├── data
│   │               │   ├── breakpoints.yml
│   │               │   ├── colors.yml
│   │               │   ├── core-team.yml
│   │               │   ├── docs-versions.yml
│   │               │   ├── examples.yml
│   │               │   ├── grays.yml
│   │               │   ├── icons.yml
│   │               │   ├── sidebar.yml
│   │               │   ├── theme-colors.yml
│   │               │   └── translations.yml
│   │               ├── layouts
│   │               │   ├── _default
│   │               │   │   ├── 404.html
│   │               │   │   ├── baseof.html
│   │               │   │   ├── docs.html
│   │               │   │   ├── examples.html
│   │               │   │   ├── home.html
│   │               │   │   ├── redirect.html
│   │               │   │   └── single.html
│   │               │   ├── alias.html
│   │               │   ├── partials
│   │               │   │   ├── ads.html
│   │               │   │   ├── analytics.html
│   │               │   │   ├── callout-danger-async-methods.md
│   │               │   │   ├── callout-info-mediaqueries-breakpoints.md
│   │               │   │   ├── callout-info-npm-starter.md
│   │               │   │   ├── callout-info-prefersreducedmotion.md
│   │               │   │   ├── callout-info-sanitizer.md
│   │               │   │   ├── callout-warning-color-assistive-technologies.md
│   │               │   │   ├── callout-warning-input-support.md
│   │               │   │   ├── docs-navbar.html
│   │               │   │   ├── docs-sidebar.html
│   │               │   │   ├── docs-subnav.html
│   │               │   │   ├── docs-versions.html
│   │               │   │   ├── favicons.html
│   │               │   │   ├── footer.html
│   │               │   │   ├── header.html
│   │               │   │   ├── home
│   │               │   │   │   ├── masthead-followup.html
│   │               │   │   │   └── masthead.html
│   │               │   │   ├── icons
│   │               │   │   │   ├── bootstrap-logo-solid.svg
│   │               │   │   │   ├── bootstrap-white-fill.svg
│   │               │   │   │   ├── bootstrap.svg
│   │               │   │   │   ├── circle-square.svg
│   │               │   │   │   ├── cloud-fill.svg
│   │               │   │   │   ├── code.svg
│   │               │   │   │   ├── collapse.svg
│   │               │   │   │   ├── droplet-fill.svg
│   │               │   │   │   ├── expand.svg
│   │               │   │   │   ├── github.svg
│   │               │   │   │   ├── hamburger.svg
│   │               │   │   │   ├── homepage-hero.svg
│   │               │   │   │   ├── list.svg
│   │               │   │   │   ├── menu.svg
│   │               │   │   │   ├── opencollective.svg
│   │               │   │   │   ├── slack.svg
│   │               │   │   │   └── twitter.svg
│   │               │   │   ├── redirect.html
│   │               │   │   ├── scripts.html
│   │               │   │   ├── skippy.html
│   │               │   │   ├── social.html
│   │               │   │   ├── stylesheet.html
│   │               │   │   └── table-content.html
│   │               │   ├── robots.txt
│   │               │   ├── shortcodes
│   │               │   │   ├── bs-table.html
│   │               │   │   ├── callout.html
│   │               │   │   ├── docsref.html
│   │               │   │   ├── example.html
│   │               │   │   ├── markdown.html
│   │               │   │   ├── param.html
│   │               │   │   ├── partial.html
│   │               │   │   ├── placeholder.html
│   │               │   │   ├── scss-docs.html
│   │               │   │   ├── table.html
│   │               │   │   └── year.html
│   │               │   └── sitemap.xml
│   │               └── static
│   │                   ├── CNAME
│   │                   ├── docs
│   │                   │   └── 5.0
│   │                   │       └── assets
│   │                   │           ├── brand
│   │                   │           │   ├── bootstrap-logo-black.svg
│   │                   │           │   ├── bootstrap-logo-shadow.png
│   │                   │           │   ├── bootstrap-logo-white.svg
│   │                   │           │   ├── bootstrap-logo.svg
│   │                   │           │   ├── bootstrap-social-logo.png
│   │                   │           │   └── bootstrap-social.png
│   │                   │           ├── img
│   │                   │           │   ├── bootstrap-icons.png
│   │                   │           │   ├── bootstrap-icons@2x.png
│   │                   │           │   ├── bootstrap-themes-collage.png
│   │                   │           │   ├── bootstrap-themes-collage@2x.png
│   │                   │           │   ├── bootstrap-themes.png
│   │                   │           │   ├── bootstrap-themes@2x.png
│   │                   │           │   ├── examples
│   │                   │           │   │   ├── album-rtl.png
│   │                   │           │   │   ├── album-rtl@2x.png
│   │                   │           │   │   ├── album.png
│   │                   │           │   │   ├── album@2x.png
│   │                   │           │   │   ├── blog-rtl.png
│   │                   │           │   │   ├── blog-rtl@2x.png
│   │                   │           │   │   ├── blog.png
│   │                   │           │   │   ├── blog@2x.png
│   │                   │           │   │   ├── carousel-rtl.png
│   │                   │           │   │   ├── carousel-rtl@2x.png
│   │                   │           │   │   ├── carousel.png
│   │                   │           │   │   ├── carousel@2x.png
│   │                   │           │   │   ├── cheatsheet-rtl.png
│   │                   │           │   │   ├── cheatsheet-rtl@2x.png
│   │                   │           │   │   ├── cheatsheet.png
│   │                   │           │   │   ├── cheatsheet@2x.png
│   │                   │           │   │   ├── checkout-rtl.png
│   │                   │           │   │   ├── checkout-rtl@2x.png
│   │                   │           │   │   ├── checkout.png
│   │                   │           │   │   ├── checkout@2x.png
│   │                   │           │   │   ├── cover.png
│   │                   │           │   │   ├── cover@2x.png
│   │                   │           │   │   ├── dashboard-rtl.png
│   │                   │           │   │   ├── dashboard-rtl@2x.png
│   │                   │           │   │   ├── dashboard.png
│   │                   │           │   │   ├── dashboard@2x.png
│   │                   │           │   │   ├── features.png
│   │                   │           │   │   ├── features@2x.png
│   │                   │           │   │   ├── grid.png
│   │                   │           │   │   ├── grid@2x.png
│   │                   │           │   │   ├── headers.png
│   │                   │           │   │   ├── headers@2x.png
│   │                   │           │   │   ├── heroes.png
│   │                   │           │   │   ├── heroes@2x.png
│   │                   │           │   │   ├── jumbotron.png
│   │                   │           │   │   ├── jumbotron@2x.png
│   │                   │           │   │   ├── masonry.png
│   │                   │           │   │   ├── masonry@2x.png
│   │                   │           │   │   ├── navbar-bottom.png
│   │                   │           │   │   ├── navbar-bottom@2x.png
│   │                   │           │   │   ├── navbar-fixed.png
│   │                   │           │   │   ├── navbar-fixed@2x.png
│   │                   │           │   │   ├── navbar-static.png
│   │                   │           │   │   ├── navbar-static@2x.png
│   │                   │           │   │   ├── navbars.png
│   │                   │           │   │   ├── navbars@2x.png
│   │                   │           │   │   ├── offcanvas-navbar.png
│   │                   │           │   │   ├── offcanvas-navbar@2x.png
│   │                   │           │   │   ├── pricing.png
│   │                   │           │   │   ├── pricing@2x.png
│   │                   │           │   │   ├── product.png
│   │                   │           │   │   ├── product@2x.png
│   │                   │           │   │   ├── sidebars.png
│   │                   │           │   │   ├── sidebars@2x.png
│   │                   │           │   │   ├── sign-in.png
│   │                   │           │   │   ├── sign-in@2x.png
│   │                   │           │   │   ├── starter-template.png
│   │                   │           │   │   ├── starter-template@2x.png
│   │                   │           │   │   ├── sticky-footer-navbar.png
│   │                   │           │   │   ├── sticky-footer-navbar@2x.png
│   │                   │           │   │   ├── sticky-footer.png
│   │                   │           │   │   └── sticky-footer@2x.png
│   │                   │           │   └── favicons
│   │                   │           │       ├── android-chrome-192x192.png
│   │                   │           │       ├── android-chrome-512x512.png
│   │                   │           │       ├── apple-touch-icon.png
│   │                   │           │       ├── favicon-16x16.png
│   │                   │           │       ├── favicon-32x32.png
│   │                   │           │       ├── favicon.ico
│   │                   │           │       ├── manifest.json
│   │                   │           │       └── safari-pinned-tab.svg
│   │                   │           └── js
│   │                   │               └── validate-forms.js
│   │                   └── sw.js
│   ├── tailwind.config.js
│   ├── theme
│   │   ├── chakra.js
│   │   ├── color.ts
│   │   └── constant.ts
│   ├── tsconfig.json
│   ├── types
│   │   └── images.d.ts
│   ├── yarn-error.log
│   └── yarn.lock
└── server
    ├── Dockerfile
    ├── controllers
    │   ├── adminController.ts
    │   ├── capitalInjectController.ts
    │   ├── customerListController.ts
    │   ├── dashboardController.ts
    │   ├── expensesController.ts
    │   ├── fixedAssetsController.ts
    │   ├── incomesController.ts
    │   ├── inventoryController.ts
    │   ├── renderServicesController.ts
    │   ├── reportController.ts
    │   ├── signUpController.ts
    │   ├── transactionDetailListController.ts
    │   ├── userController.ts
    │   └── vendorListController.ts
    ├── data
    │   ├── capstone_project.xlsx
    │   ├── capstone_project_columns.xlsx
    │   └── capstone_project_source_data.xlsx
    ├── docker-compose.yml
    ├── index.js
    ├── knexfile.ts
    ├── migrations
    │   ├── 20220505070240_create-db.ts
    │   ├── 20220514094930_delete_inventory_column.ts
    │   ├── 20220515054526_add_fixed_Assets_column.ts
    │   └── 20220516041636_add_fixed_Assets_column.ts
    ├── package.json
    ├── public
    │   └── upload
    │       ├── adams.png
    │       ├── alex.jpg
    │       ├── bruce.jpg
    │       ├── daniel.jpg
    │       ├── default.png
    │       ├── gordon.jpg
    │       ├── jacky.jpg
    │       ├── jason.jpg
    │       ├── jasonfong.jpg
    │       ├── joe.jpg
    │       └── leo.jpg
    ├── routers
    │   ├── adminRoute.ts
    │   ├── customerListRoute.ts
    │   ├── dashboardRoute.ts
    │   ├── reportRoute.ts
    │   ├── signUpRoute.ts
    │   ├── transactionRoute.ts
    │   ├── userRoute.ts
    │   └── vendorRoute.ts
    ├── routes.ts
    ├── seeds
    │   └── init-data.ts
    ├── server.ts
    ├── services
    │   ├── adminServices.ts
    │   ├── capitalInjectService.ts
    │   ├── customerListServices.ts
    │   ├── dashboardServices.ts
    │   ├── expensesService.ts
    │   ├── fixedAssetsService.ts
    │   ├── getInfoServices.ts
    │   ├── incomesService.ts
    │   ├── inventoryService.ts
    │   ├── renderServicesService.ts
    │   ├── reportServices.ts
    │   ├── signUpServices.ts
    │   ├── transactionDetailListService.ts
    │   ├── userService.ts
    │   └── vendorListServices.ts
    ├── tsconfig.json
    ├── users
    ├── utils
    │   ├── database.ts
    │   ├── dayCalculation.ts
    │   ├── errors.ts
    │   ├── guards.ts
    │   ├── hash.ts
    │   ├── jwt.ts
    │   ├── logger.ts
    │   ├── models.ts
    │   └── tablesColumns.ts
    └── yarn.lock