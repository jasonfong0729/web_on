import dotenv from "dotenv";
import { hashPassword } from "./hash"
dotenv.config();

import pg from "pg";
import { users, channels, messages, channelRelations, settings,images } from "../database/dummy_data_present";
// messages, channels, channel_relations
const client = new pg.Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
});

const userMapping = new Map<string, number>();
const messageMapping = new Map<string, number>();
const channelMapping = new Map<string, number>();
const channelRelationMapping = new Map<number, number>();
const settingMapping = new Map<number, number>();
const imageMapping = new Map<string, number>();

async function main() {
    await client.connect();
    // Delete Table from
    await client.query(/*sql*/ `DELETE FROM channel_relations`);
    await client.query(/*sql*/ `DELETE FROM messages`);
    await client.query(/*sql*/ `DELETE FROM images`);
    await client.query(/*sql*/ `DELETE FROM setting`);
    await client.query(/*sql*/ `DELETE FROM channels`);
    await client.query(/*sql*/ `DELETE FROM users`);


    // Comment the below when if you dont have any data at the below table
    await client.query(`ALTER SEQUENCE users_id_seq RESTART WITH 1`);
    await client.query(`ALTER SEQUENCE channels_id_seq RESTART WITH 1`);
    await client.query(`ALTER SEQUENCE messages_id_seq RESTART WITH 1`);
    await client.query(`ALTER SEQUENCE channel_relations_id_seq RESTART WITH 1`);
    await client.query(`ALTER SEQUENCE setting_id_seq RESTART WITH 1`);
    await client.query(`ALTER SEQUENCE images_id_seq RESTART WITH 1`);
    //Users
    for (const user of users) {

        const hashedPassword = await hashPassword(user.password)

        const insertedResult = (

            await client.query(/*sql*/ `INSERT INTO users (name,username,email,password,profile_image) VALUES ($1,$2,$3,$4,$5) RETURNING id`,
                [user.name, user.username, user.email, hashedPassword,user.profileImage])
        ).rows[0]
        userMapping.set(user.username, insertedResult.id);
    }


    //Channels
    for (const channel of channels) {
        const insertedResult = (
            await client.query(`INSERT INTO channels (name,channel_types,profile_image) VALUES ($1,$2,$3) RETURNING id`,
                [channel.name, channel.type, channel.profileImage])
        ).rows[0]
        channelMapping.set(channel.name, insertedResult.id)
    }
    // console.log(channelMapping)

    // console.log(messageMapping)

    //Channel Relations
    for (const channelRelation of channelRelations) {

        const insertedResult = (
            await client.query(`INSERT INTO channel_relations (user_id,channel_id) VALUES ($1,$2) RETURNING id`,
                [channelRelation.user_id, channelRelation.channel_id]
            )
        ).rows[0]
        channelRelationMapping.set(channelRelation.user_id, insertedResult.id)
    }
    // console.log(channelRelationMapping)

    //Settings
    for (const setting of settings) {

        const insertedResult = (
            await client.query(`INSERT INTO setting (user_id) VALUES ($1) RETURNING id`,
                [setting.user_id]
            )
        ).rows[0]
        // console.log(1)
        settingMapping.set(setting.user_id, insertedResult.id)
        // console.log(settingMapping)
    }

    for (const image of images) {

        const insertedResult = (
            await client.query(`INSERT INTO images (image_name) VALUES ($1) RETURNING id`,
                [image.image_name]
            )
        ).rows[0]
        // console.log(1)
        imageMapping.set(image.image_name, insertedResult.id)
        // console.log(settingMapping)
    }

        //Messages
        for (const message of messages) {

            const insertedResult = (
                await client.query(`INSERT INTO messages (content,user_id,channel_id,image_id) VALUES ($1,$2,$3,$4) RETURNING id`,
                    [message.content, message.user_id, message.channel_id,message.image_id]
                )
            ).rows[0]
            messageMapping.set(message.content, insertedResult.id)
        }
    client.end()
    console.log('[INFO] Reset done')


    return
}
main()

