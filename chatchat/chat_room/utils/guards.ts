import { Request, Response, NextFunction } from "express";
const chatroomPath = "/chatroom_dummy.html"


export const isLoggedInStatic = (req: Request, res: Response, next: NextFunction) => {
  console.log(`[INFO]     FROM guard(isLoggedInStatic)--------------------------------`)
  if (req.session["user"]) {
    console.log('[INFO]     Found user\'s session')
    next();
    return;
  }
  if (req.path != "/" && req.path != "/chatroom" && req.path != chatroomPath) {
    console.log('[INFO]     user is not logged in')
    next()
    return
  }
  console.log(`[INFO]     user is not logged in`);
  res.redirect("/");
};

export const isLoggedInApi = (req: Request, res: Response, next: NextFunction) => {
  if (req.session["user"]) {
    res.redirect("chatroom_dummy.html")
    return;
  }
  next();;
};
