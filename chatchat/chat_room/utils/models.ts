export type User ={
    id: number;
    name: string;
    username: string;
    email: string;
    password: string;
    profile_image: string;
    description: string;
    is_online: boolean;
    permission_level:number;
    created_at: Date;
    updated_at: Date;
  }
export type Username ={
  id:number;
  username: string;
}

export type name ={
  id:number;
  name: string;
}

export type Email = {
  id: number;
  email: string;
}

export type LoginData ={
  userInfo:ChannelUser
  channelUsersList:ChannelUser[];
  userChannelList:UserChannel[];
  channelDetail:ChannelDetail;
  channelMessages:Message[];
}

export type ChannelUser ={
  id:number;
  name:string;
  profile_image:string;
  description:string;
  keep_login?:boolean;
  is_online:boolean;
}

type ChannelType = "room"|"group"|"private"
//enum ??

export type UserChannel ={
  id:number;
  name:string;
  channel_types:ChannelType
  profile_image: string;
}

export type ChannelDetail ={
  id:number;
  name :string;
  profile_image:string;
}

export type Message ={
  content:string;
  id:number;
  name:string;
  profile_image:string;
  is_online:boolean;
  created_at: Date;
}