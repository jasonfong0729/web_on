import dotenv from "dotenv";
dotenv.config();
import express from 'express';
import { Request, Response } from 'express';
import expressSession from "express-session";
import multer from "multer";
import http from 'http';
import { Server as SocketIO } from 'socket.io';
import { LoginData, ChannelUser, UserChannel, ChannelDetail, Message } from "./utils/models"
import { isLoggedInStatic, isLoggedInApi } from "./utils/guards";
import path from 'path'
import pg from 'pg';


//SQL for User Detail
const sqlUser = `SELECT id,name, profile_image,description,keep_login,is_online FROM users WHERE id = $1`

//SQL for  User Personal Channel List 
const sqlChannelUsersList = `SELECT 
channel_relations.user_id, users.name, users.profile_image, users.description, users.is_online
FROM channel_relations 
INNER JOIN users ON channel_relations.user_id = users.id
WHERE channel_id = $1 ORDER BY users.is_online DESC,upper(users.name) ASC;`; //[]

// SQL for Channel's Users List   
const sqlUserChannelList = `SELECT
channels.id, channels.name, channels.channel_types, channels.profile_image
FROM channel_relations 
INNER JOIN channels ON channel_relations.channel_id = channels.id
WHERE channel_relations.user_id = $1 ORDER BY channels.id DESC`; // []

//SQL for Channel Info
const sqlChannelDetail = `SELECT id,name,profile_image from channels where id = $1`

//SQL for Channel's Messages
const sqlChannelMessage = `SELECT 
messages.content, images.image_name, users.id, users.name, users.profile_image,users.is_online,messages.created_at
FROM messages 
INNER JOIN users ON messages.user_id = users.id
LEFT OUTER JOIN images ON messages.image_id = images.id
WHERE messages.channel_id = $1
ORDER BY messages.created_at`;
////////////////////////////////////////////////////////////////environment setup

//connect to database
export const client = new pg.Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
});
client.connect();
//reset online status after sever error
resetDataAfterError()
async function resetDataAfterError (){
    const sqlResetOnlineStatus = `UPDATE users SET is_online = $1`;
    await client.query(sqlResetOnlineStatus,[false])
}
// image upload set up
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.resolve("./private/uploads/messages_attachment"));
    },
    filename: function (req, file, cb) {
        const{userId,channelId} = req.body
      cb(null, `Channel_ID_${channelId}_User_ID_${userId}-${Date.now()}.${file.mimetype.split("/")[1]}`);
    },
  });
//   file.fieldname

const messageUpload = multer({ storage });

//server and session setup
const app = express();
const server = new http.Server(app);
const io = new SocketIO(server);
const sessionMiddleware = expressSession({
    secret: "We are group 12! Please enjoy!!",
    resave: true,
    saveUninitialized: true,
})

app.use(sessionMiddleware);
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
////////////////////////////////////////////////////////////////route setup

//login Guard
app.get('/', isLoggedInApi, function (req: Request, res: Response) {
    res.redirect("index.html");
})
import { registerRoutes } from "./routers/signUpRoutes"
import { userRoutes } from "./routers/userRoutes"
import { leaveWindowRoutes } from "./routers/leaveWindowRoutes"
//login related
app.use("/signup", registerRoutes);
app.use("/chatroom", userRoutes);
app.use("/logout", userRoutes);
app.use("/leaveWindow", leaveWindowRoutes);

app.post("/sendMessage", messageUpload.single("image"),async(req: Request, res: Response)=>{
    console.log(`[INFO]     FROM Post /sendMessage--------------------------------`)
    const image = req.file?.filename
    const {userId,channelId,message} = req.body
    console.log(userId,channelId,message)
    let imageId
    if(image){
        const sqlImageUpload = `INSERT INTO images (image_name) VALUES ($1) RETURNING id`
        imageId = (await client.query(sqlImageUpload, [image])).rows[0].id
    }else {imageId = null}
    const sqlMessage = `INSERT INTO messages (content,user_id,channel_id,image_id) VALUES ($1,$2,$3,$4)`
    await client.query(sqlMessage, [message, userId, channelId,imageId])
    res.json({"message":"upload successful"})
    console.log(`[INFO]     Success -- Upload the message to Database`)
})
////////////////////////////////////////////////////////////////socket.io related

//Create io.session link with express session for identify the user
io.use((socket, next) => {
    let req = socket.request as express.Request
    let res = req.res as express.Response
    sessionMiddleware(req, res, next as express.NextFunction)
});
// let sockets = [];
// let people = {};

//user connected to server 
io.on('connection', async function (socket) {
    // sockets.push(socket)

    const req = socket.request as express.Request;
 //[]

    /*         message image problem        */

    if (req.session["user"]) {
        console.log(`[INFO]     FROM Login--------------------------------`)
        //Identify the user 
        // console.log(socket.id)
        const id: number = req.session["user"].id
        console.log('[INFO]     Start: The incoming user id: ', id)
        const channel: number = req.session["user"].channel
        console.log('[INFO]     User entered room: ', channel)
        // io.on('join', function (id) {
        //     people['on00'] = { name: id };
        // })
        const sqlFirstChannelDetail = `SELECT id,name,profile_image from channels where id = $1`

        const user: ChannelUser = (await client.query(sqlUser, [id])).rows[0]
        const channelUsersList: ChannelUser[] = (await client.query(sqlChannelUsersList, [channel])).rows
        const userChannelList: UserChannel[] = (await client.query(sqlUserChannelList, [id])).rows
        const FirstChannelDetail: ChannelDetail = (await client.query(sqlFirstChannelDetail, [channel])).rows[0]
        const channelMessages: Message[] = (await client.query(sqlChannelMessage, [channel])).rows

        //Checking the data
        if (0) {
            console.log('[INFO] user', user)
            console.log('[INFO] channelUsersList', channelUsersList)
            console.log('[INFO] userChannelList', userChannelList)
            console.log('[INFO] channelDetail', FirstChannelDetail[0])
            console.log('[INFO] ChannelMessage', channelMessages)
        }

        /* Upgrade the database to server method
        // const sqlCombinedData = `SELECT 
        // channels.id, channels.name, channels.channel_types, channels.profile_image, channel_relations.user_id,
        // users.name, users.profile_image, 
        // messages.content, messages.created_at 
        // FROM channels 
        // LEFT OUTER  JOIN channel_relations ON channels.id = channel_relations.channel_id
        // LEFT OUTER  JOIN users on  channel_relations.user_id = users.id
        // LEFT OUTER JOIN messages on users.id = messages.user_id
        // where channels.id = $1`
        // const combinedData = (await client.query(sqlCombinedData, [channel])).rows
        // console.log(combinedData[0].name) */

        //Data Set transfer to users
        const data: LoginData = {
            userInfo: user,
            channelUsersList: channelUsersList,
            userChannelList: userChannelList,
            channelDetail: FirstChannelDetail,
            channelMessages: channelMessages,
        }
        console.log(`[INFO]     Success -- Loading the login data of user successfully.`)
        // console.log(data.userChannelList)
        // console.log(userChannelList)
        for (const userChannel of userChannelList) {
            socket.join(`channel-${userChannel.id}`)
            console.log(`[INFO]     User ID ${user.id} entered channel ID :` + userChannel.id)
            const dataForOtherUser = {
                user: user,
                channelId: userChannel.id
            }
            socket.broadcast.to(`channel-${userChannel.id}`).emit("new-online", dataForOtherUser)
            console.log(`[INFO]     Success -- Told the other users in channel ID ${userChannel.id} (User ID ${id} entered channel)`)
        }

        socket.emit('loadingTheInformation', { data })
        console.log(`[INFO]     Success -- Loading the login data to client `)
        // Tell all users other than user who are entering the room
    };

    socket.on("new register",async (name) => {
        const sqlNewRegister = `SELECT id,name, profile_image,description,keep_login,is_online FROM users WHERE name = $1` 
        const user: ChannelUser = (await client.query(sqlNewRegister, [name])).rows[0]
        const dataForOtherUser = {
            user: user,
            channelId: 1
        }
        socket.broadcast.to(`channel-1`).emit("new-online", dataForOtherUser)
        console.log(`[INFO]     Success -- Told the other users in channel ID 1 (User ID ${user.id} entered channel)`)
    })

    socket.on('new channel list', async id => {
        console.log(`[INFO]     FROM new channel list--------------------------------`)
        const userChannelList: UserChannel[] = (await client.query(sqlUserChannelList, [id])).rows
        socket.emit('new channel list given', userChannelList)
        console.log(`[INFO]     Success -- Send the new channel list to user id: ${id} `)
    })
    /*      How can I replace the name of property??        */
    //Send the messages to the channel
    socket.on("send-message", async ({ userId, channelId}) => {
        console.log(`[INFO]     FROM send-message--------------------------------`)
        // console.log(userId)
        const sqlUser = `SELECT id,name, profile_image,is_online FROM users WHERE id = $1`
        const user = (await client.query(sqlUser, [userId])).rows[0]
        const sqlMessage = `SELECT messages.content,messages.created_at,images.image_name FROM messages 
        LEFT OUTER  JOIN images ON messages.image_id = images.id
        WHERE user_id = $1 AND channel_id = $2 ORDER BY created_at DESC`
        const messageInfo = (await client.query(sqlMessage, [userId, channelId])).rows[0]
        const newMessage = {
            user: user,
            message: messageInfo,
            channelId: channelId
        }
        // console.log(newMessage)
        io.to(`channel-${channelId}`).emit("new-message", newMessage)
        console.log(`[INFO]     Success -- Send the new message to channel ${channelId} `)
    })
    //Send the latest user's online of the channel
    socket.on('onlineUsers', async (channel) => {
        console.log(`[INFO]     FROM onlineUsers--------------------------------`)
        // console.log(channel)
        const channelUsersList: ChannelUser[] = (await client.query(sqlChannelUsersList, [channel])).rows
        // console.log(channelUsersList)
        socket.emit('onlineUsersList', { channelUsersList })
        console.log(`[INFO]     Success -- Send Online Users List to client in channel id ${channel} `)
    })

    //Send the latest user's list of the channel
    socket.on('allUsers', async (channel) => {
        console.log(`[INFO]     FROM allUser--------------------------------`)
        console.log("[INFO]     channel id: " + channel)
        const channelUsersList: ChannelUser[] = (await client.query(sqlChannelUsersList, [channel])).rows
        // console.log(channelUsersList)
        socket.emit('allUserList', { channelUsersList })
        console.log(`[INFO]     Success to send all Users Status to client in channel id ${channel}`)
    })

    //Send the information of the channel after user clicked the desired channel 
    socket.on('changeChannel', async (channelId) => {
        console.log(`[INFO]     FROM changeChannel--------------------------------`)
        const channelUsersList: ChannelUser[] = (await client.query(sqlChannelUsersList, [channelId])).rows
        const channelDetail: ChannelDetail = (await client.query(sqlChannelDetail, [channelId])).rows[0]
        const channelMessages: Message[] = (await client.query(sqlChannelMessage, [channelId])).rows
        const changedChannelData = {
            channelUsersList: channelUsersList,
            channelDetail: channelDetail,
            channelMessages: channelMessages,
        }
        console.log(`[INFO]     Success -- Send the Changed data to client `)

        socket.join(`channel-${channelId}`)
        socket.emit('changedChannelData', { changedChannelData })
    })

    // cant create the same name
    socket.on('create_channel', async ({ channelName, userId, type }) => {
        console.log(`[INFO]     FROM create_channel--------------------------------`)
        const sqlCreateChannel = `INSERT INTO channels (name,channel_types) VALUES ($1,$2) RETURNING id,name,profile_image`
        const createdChannel = (await client.query(sqlCreateChannel, [channelName, type])).rows[0]
        const channelId = createdChannel.id
        // const name = createdChannel.name
        // const profileImage = createdChannel.profile_image
        // console.log(`createdChannel.id: ` + createdChannel.id)
        const sqlCreateChannelRelation = `INSERT INTO channel_relations (user_id,channel_id,is_admin) VALUES ($1,$2,$3)`
        await client.query(sqlCreateChannelRelation, [userId, channelId, true])
        const userChannelList: UserChannel[] = (await client.query(sqlUserChannelList, [userId])).rows
        // console.log('createdChannelRelation: ' +  createdChannelRelation)
        // const data = {
        //     id: channelId,
        //     name: name,
        //     profile_image: profileImage
        // }
        socket.join(`channel-${channelId}`)
        console.log(`[INFO]     ID ${userId} entered channel ID :` + channelId)
        socket.emit("new-channel", userChannelList)
        // console.log('data: ' +  data.name)
        console.log(`[INFO]     Success -- Create channel ID ${channelId} `)
    })

    /* should block the same groupname and name make guard */
    /* wanner search the user from database but fail
    https://stackoverflow.com/questions/29257735/socket-io-add-user-to-room-manually-node-js?answertab=scoredesc#tab-top
    */
    socket.on("add-friend", async ({ channelId, name }) => {
        console.log(`[INFO]     FROM add-friend--------------------------------`)
        console.log('[INFO]     channel Id: ' + channelId)
        console.log("[INFO]     friend name: " + name)
        const sqlUser = `SELECT id,name, profile_image,description,keep_login,is_online FROM users WHERE name = $1`
        const foundUser: ChannelUser = (await client.query(sqlUser, [name])).rows[0];
        if(foundUser == undefined){
            socket.emit('no such user',0)
            console.log(`[INFO]     Fail to find the user ${name} No such user`)
            return
        }
        console.log(`[INFO]     Success -- pass the first rule`)
        const sqlRelation = `SELECT id from channel_relations where channel_id = $1 AND user_id = $2`
        const foundRelation = (await client.query(sqlRelation, [channelId,foundUser.id])).rows[0]
        // console.log("foundRelation: " + foundRelation)
        if(foundRelation != undefined){
            socket.emit('Already in the group',0)
            console.log(`[INFO]     Fail to find the user ${name} Already in the channel`)
            return
        }
        console.log(`[INFO]     Success -- Start to add the user in channel......`)
        
        const sqlCreateChannelRelation = `INSERT INTO channel_relations (user_id,channel_id,is_admin) VALUES ($1,$2,$3)`
        await client.query(sqlCreateChannelRelation, [foundUser.id, channelId, false])
        const dataForOtherUser = {
            user: foundUser,
            channelId: channelId
        }
        // socket.join(`channel-${userChannel.id}`)
        // console.log(`[INFO]     User ID ${id} entered channel ID :` + channel)
        console.log(`[INFO]     Success add friend ${name} to channel id ${channelId}`)
        io.to(`channel-${channelId}`).emit("friend-added", dataForOtherUser)
        console.log(`[INFO]     Success update the user's user list in channel id ${channelId}`)
        io.emit('being-added', foundUser.id)
        console.log(`[INFO]     Success update the user's channel list who are being added in channel id ${channelId}`)
    })

    //Update the status of user after the user leave the window of clicked logout  
    socket.on('leaveRoom', async (id) => {
        (`[INFO]     FROM leaveRoom--------------------------------`)
        const sqlUser = `SELECT id,name, profile_image,description,keep_login,is_online FROM users WHERE id = $1`
        const sqlUserChannelList = `SELECT
        channels.id, channels.name, channels.channel_types, channels.profile_image
        FROM channel_relations 
        INNER JOIN channels ON channel_relations.channel_id = channels.id
        WHERE channel_relations.user_id = $1`;
        const userChannelList: UserChannel[] = (await client.query(sqlUserChannelList, [id])).rows
        const user: ChannelUser = (await client.query(sqlUser, [id])).rows[0]
        for (const userChannel of userChannelList) {
            const dataToOtherUser = {
                user: user,
                channelId: `${userChannel.id}`
            }
            socket.broadcast.to(`channel-${userChannel.id}`).emit("new-offline", dataToOtherUser)
        }
        console.log(`[INFO]     Success Send the logout infos to other user --logout ID: ` + id)
    })

});
////////////////////////////////////////////////////////////////static direct

app.use(express.static(path.join(__dirname, "public")))
app.use(isLoggedInStatic, express.static(path.join(__dirname, "private")))
app.use((req, res) => {
    res.sendFile(path.resolve("./public/404.html"));
});
////////////////////////////////////////////////////////////////start the server

const PORT = 8080;

server.listen(PORT, () => {
    console.log(`[INFO]          Listening at http://localhost:${PORT}/`);
});