import express from 'express';
import { Request, Response } from 'express';
import { User } from '../utils/models'
import { client } from "../app";
import { checkPassword } from "../utils/hash"

export const userRoutes = express.Router();
userRoutes.post("/", login)
userRoutes.get("/",logout)
async function login(req: Request, res: Response) {
    const { username, password, keepLogin } = req.body
    const sqlQuery = `Select * from users where username = $1`
    const foundUser: User| undefined= (await client.query(sqlQuery, [username])).rows[0];
    

    if (!foundUser || !(await checkPassword(password, foundUser.password))) {
        res.status(401).json({ message: "Incorrect Username or Password" });
        return;
    }

    const sqlQueryUpdateStatus = 
    `UPDATE users SET keep_login = $1, is_online = $2
    WHERE id = $3`
    await client.query(sqlQueryUpdateStatus, [keepLogin,true,foundUser.id])

    req.session["user"] = { 
        id: foundUser.id, 
        channel:1,
        username: foundUser.username, 
        keepLogin:keepLogin,
        isOnline:true
    };
    console.log(`[INFO]     FROM userRoutes--------------------------------`)
    console.log(`[INFO]     req.session:` + req.session)
    // console.log(`[INFO] Sesson ID: ${req.session["user"].id}`)
    res.json({ message: "success" });
    return;
}

///when user leave for a while error occur
async function logout(req:express.Request,res:express.Response){
    if(req.session['user']){
        //server down the user is still there catch error
        const id = req.session['user']["id"]
        console.log(`[INFO]     FROM userRoutes--------------------------------`)
        console.log('[INFO]     logout id: ' + id)
        const sqlQueryUpdateStatus = 
        `UPDATE users SET keep_login = $1, is_online = $2
        WHERE id = $3`

        await client.query(sqlQueryUpdateStatus, [false,false,id])
        delete req.session['user'];
    }
    res.json({ message: "user logged out successfully"});
}

