import express from 'express';
import { Request, Response } from 'express';
import { name,Username,Email } from '../utils/models'
import { client } from "../app";
import { hashPassword } from "../utils/hash"
// import bodyParser from 'body-parser';

// const jsonParser = bodyParser.json()
export const registerRoutes = express.Router();

registerRoutes.post("/",createNewUser)

async function createNewUser(req: Request, res: Response) {
    console.log(`[INFO]     FROM Sign Up Routes--------------------------------`)
    console.log(req.body)
    const { name,username, password, email } = req.body
    const trimmedName = name.trim()
    const trimmedUsername = username.trim()
    const trimmedPassword = password.trim()
    const trimmedEmail = email.trim()
    const hashedPassword = await hashPassword(trimmedPassword)
    console.log(name, username, hashedPassword,email)


    const foundName: name| undefined = (
        await client.query(`select id,name from users where username = $1`,[trimmedName])).rows[0];
    if(foundName){
        console.log(`[INFO]     Fail -- Name: ${trimmedName} has been used already`)
        res.status(400).json({"message":"Sorry, name has been used already."})
        return
    }
    const foundUsername: Username| undefined = (
        await client.query(`select id,username from users where username = $1`,[trimmedUsername])).rows[0];
    if(foundUsername){
        console.log(`[INFO]     Fail -- Username: ${trimmedUsername} has been used already`)
        res.status(401).json({"message":"Sorry, username has been used already."})
        return
    }

    const foundEmail: Email | undefined= (
        await client.query(`select id,email from users where email = $1`,[trimmedEmail])).rows[0];
    if(foundEmail){
        console.log(`[INFO]     Fail -- Email: ${trimmedEmail} has been used already`)
        res.status(400).json({"message":"Sorry, email has been used already."})
        return
    }

    const sqlQuery = `INSERT INTO users 
    (name,username,email,password)
    VALUES ($1,$2,$3,$4) RETURNING id`

    const newUser = (await client.query(sqlQuery,[trimmedName,trimmedUsername,trimmedEmail,hashedPassword])).rows[0]
    await client.query(`INSERT INTO channel_relations (user_id,channel_id) VALUES ($1,$2)`,[newUser.id,1])
    await client.query(`INSERT INTO setting (user_id) VALUES ($1)`,[newUser.id])
    res.status(200).json({"message":"Account created"})   
    console.log(`[INFO]     Success -- user ${trimmedUsername}  created`);
}

// User|undefined