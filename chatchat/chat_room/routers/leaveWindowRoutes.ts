import express from 'express';
import { client } from "../app";

export const leaveWindowRoutes = express.Router();
leaveWindowRoutes.get("/",leaveWindow)


async function leaveWindow(req:express.Request,res:express.Response){
    if(req.session['user']){
        //server down the user is still there catch error
        const id = req.session['user']["id"]
        console.log(`[INFO]     FROM leaveWindowRoutes--------------------------------`)
        console.log('[INFO]     logout id: ' + id)
        const sqlQueryUpdateStatus = 
        `UPDATE users SET keep_login = $1, is_online = $2
        WHERE id = $3`

        await client.query(sqlQueryUpdateStatus, [false,false,id])
        delete req.session['user'];
    }
    res.json({ message: "user logged out successfully"});
}