// const loginForm = document.getElementById("login-form");
// const loginButton = document.getElementById("login-button");

// loginButton.addEventListener("click", (e) => {
//     e.preventDefault();
//     const username = loginForm.username.value;
//     const password = loginForm.password.value;

//     if (username === "user" && password === "web_dev") {
//         alert("You have successfully logged in.");
//         location.reload();
//     } else {
//         alert("Incorrect Username or Password.")
//     }
// })
window.onload = async function () {
    jumpToSignupPage()
    loginToChatroom()
  };
  


function jumpToSignupPage() {
    const signup = document.getElementById("signup");
    signup.addEventListener("click", (event) => {
        event.preventDefault();
        location.replace("\signup.html")
        // console.log('hihi')
    })
}

 function loginToChatroom(){
    const login = document.getElementById("login-form");
    login.addEventListener("submit", async (event) => {
        event.preventDefault();
        const {username,password,remember} = event.target;

        const formObject = {
            username:username.value,
            password:password.value,
            keepLogin:remember.checked
        }
        const resp = await fetch ("/chatroom",{
            method: "POST",
            headers: { "content-type": "application/json" },
            body: JSON.stringify(formObject)
        })

        if(resp.status === 200) {
            location.replace("/chatroom_dummy.html")
        }
        if(resp.status ===401){
            location.replace("/fail_to_login.html")
        }
    })

}