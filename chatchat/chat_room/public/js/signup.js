// const newSignup = document.getElementById("submit");
// const loginRedirect = document.getElementById("loginRedirect");

// newSignup.addEventListener("click", (e) => {
//     e.preventDefault();
//     location.replace("\index.html")
// })

// loginRedirect.addEventListener("click", (e) => {
//     e.preventDefault();
//     location.replace("\index.html")
// })

window.onload = async () => {
    registerAccount()
}
const socket = io.connect();
let adjInfo
let adjEmail;

const registerAccount = () => {
    const formData = document.querySelector("#register_form")
    formData.addEventListener("submit", async (event) => {
        event.preventDefault()
        const password = validatePassword("password", formData["password"].value, 8, 20)
        if (password == "not valid") {
            alert(`Please re-enter your password (Must be 8-20 characters long including at least one uppercase.)`)
            return
        };
        if(!verifyThePassword()){return};
        const name = validateLength("name", formData["fullname"].value, 1, 30)
        const username = validateLength("username", formData["username"].value, 8, 12)
        // let username = formData["username"].value

        const email = validateEmail(formData["email"].value)

        const formObject = {
            name: name,
            username: username,
            password: password,
            email: email,
        }
        const resp = await fetch('/signup', {
            method: "POST",
            headers: { "content-type": "application/json" },
            body: JSON.stringify(formObject)
        })
        if(resp.status === 200) {
            socket.emit("new register",name)
            location.replace("\index.html")
        }
        if(resp.status === 402) {
            location.replace("\email_has_been_used_already.html")
        }
    })

}
function verifyThePassword() {
    const password = document.getElementById("password").value
    const verification = document.getElementById("verify_password").value
    if (password != verification) {
        alert(`Two entered passwords are not the same.`)
        return false
    }
    return true
}

function validateLength(type, info, min, max) {
    if (info.length <= max && info.length >= min) {
        return info
    }else {
        adjInfo = prompt(`Please re-enter your ${type} (Must be ${min}-${max} characters long.) `, info);
        console.log('Orignal: '+ info)
        console.log('Adj: ' + adjInfo)
        validateLength(type, adjInfo, min, max)
    }
    return adjInfo
}

function validatePassword(type, password, min, max) {
    if (password.length <= max && password.length >= min && validateUppercase(password)) {
        return password
    } else return "not valid"
}

function validateUppercase(password) {
    for (let character of password) {
        if (!isNaN(character * 1)) { }
        else {
            if (character == character.toUpperCase()) {
                return true;
            }
        }
    }
    return false
}

function validateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return mail
    }
    adjEmail = prompt("Please enter a valid email address!", mail)
    return validateEmail(adjEmail)
}

function backToLoginPage(){
    location.replace("\index.html")
}