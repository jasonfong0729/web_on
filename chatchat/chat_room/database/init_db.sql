DROP TABLE "setting";
DROP TABLE "channel_relations";
DROP TABLE "messages";
DROP TABLE "images";
DROP TABLE "channels";
DROP TABLE "users";
DROP TYPE "channel_type";

CREATE TABLE "users"(
    "id" SERIAL PRIMARY KEY,
    "name" varchar(255) NOT NULL,
    "username" VARCHAR(255) NOT NULL,
    "email" VARCHAR(255) NOT NULL,
    "password" VARCHAR(255) NOT NULL,
    "profile_image" varchar(255) NOT NULL DEFAULT 'default.png',
    "description" VARCHAR(255) DEFAULT 'I am using Chat Chat',
    "keep_login" boolean NOT NULL DEFAULT false,
    "is_online" BOOLEAN NOT NULL DEFAULT false,
    "permission_level" INTEGER NOT NULL DEFAULT 1,
    "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TYPE channel_type AS ENUM ('room','group','private');

CREATE TABLE "channels"(
    "id" SERIAL PRIMARY KEY,
    "name" VARCHAR(255) NOT NULL,
    "channel_types" channel_type NOT NULL, 
    "profile_image" VARCHAR(255) NOT NULL DEFAULT 'default.jpg',
    "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
CREATE TABLE "channel_relations"(
    "id" SERIAL PRIMARY KEY,
    "user_id" INTEGER NOT NULL,
    "channel_id" INTEGER NOT NULL,
    "is_admin" BOOLEAN NOT NULL DEFAULT FALSE,
    "favourite" BOOLEAN NOT NULL DEFAULT FALSE,
    "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY("user_id") REFERENCES "users"("id"),
    FOREIGN KEY("channel_id") REFERENCES "channels"("id")
);
CREATE TABLE "images"(
    "id" SERIAL PRIMARY KEY,
    "image_name" VARCHAR(255) NOT NULL,
    "read_once" BOOLEAN NOT NULL DEFAULT FALSE,
    "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
CREATE TABLE "messages"(
    "id" SERIAL PRIMARY KEY,
    "content" TEXT,
    "user_id" INTEGER,
    "channel_id" INTEGER NOT NULL,
    "image_id" INTEGER,
    "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY("user_id") REFERENCES "users"("id"),
    FOREIGN KEY("channel_id") REFERENCES "channels"("id"),
    FOREIGN KEY("image_id") REFERENCES "images"("id")
);
CREATE TABLE "setting"(
    "id" SERIAL PRIMARY KEY,
    "mode" BOOLEAN NOT NULL DEFAULT FALSE,
    "time_24hr" BOOLEAN NOT NULL DEFAULT False,
    "user_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY("user_id") REFERENCES "users"("id")
);
 
 SELECT channels.id, channels.name, channels.channel_types, channels.profile_image, channel_relations.user_id,
  users.name, users.profile_image, messages.content, messages.created_at 
FROM channels 
LEFT OUTER  JOIN channel_relations ON channels.id = channel_relations.channel_id
LEFT OUTER  JOIN users on  channel_relations.user_id = users.id
LEFT OUTER JOIN messages on users.id = messages.user_id
where channels.id = 1;

SELECT 
        channel_relations.user_id, users.name, users.profile_image, users.description,users.is_online
        FROM channel_relations 
        INNER JOIN users ON channel_relations.user_id = users.id
        WHERE channel_id = 1;

SELECT 
    channels.name, channels.channel_types, channels.profile_image
    FROM channel_relations 
    INNER JOIN channels ON channel_relations.channel_id = channels.id
    WHERE channel_relations.user_id = 1;

SELECT name from channels where id = 1;

SELECT 
        messages.content, users.name, messages.created_at
        FROM messages 
        INNER JOIN users ON messages.user_id = users.id
        WHERE messages.channel_id = 1
         ORDER BY messages.created_at;

SELECT id from channel_relations where channel_id = 5 AND user_id = 1;

SELECT 
    channel_relations.user_id, users.name, users.profile_image, users.description, users.is_online
    FROM channel_relations 
    INNER JOIN users ON channel_relations.user_id = users.id
    WHERE channel_id = 1 ORDER BY users.is_online DESC,upper(users.name) ASC;

SELECT is_online from users;

SELECT 
messages.content, images.image_name, users.id, users.name, users.profile_image,users.is_online,messages.created_at
FROM messages 
INNER JOIN users ON messages.user_id = users.id
LEFT OUTER JOIN images ON messages.image_id = images.id
WHERE messages.channel_id = 1
ORDER BY messages.created_at;

SELECT messages.content,messages.created_at,images.image_name FROM messages 
LEFT OUTER  JOIN images ON messages.image_id = images.id
WHERE user_id = 3 AND channel_id = 1 ORDER BY created_at DESC;