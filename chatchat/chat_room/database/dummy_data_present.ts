export const users = [
    {
        name: "KY",
        username: "jasonfong01",
        email: "1",
        password: "Ktmc0729",
        profileImage:"user_logo_1.png"
    },
    {
        name: "Daniel",
        username: "jasonfong02",
        email: "2",
        password: "Ktmc0729",
        profileImage:"user_logo_3.png"
    },
    {
        name: "Jason",
        username: "jasonfong03",
        email: "3",
        password: "Ktmc0729",
        profileImage:"user_logo_2.png"
    },
    {
        name: 'Alex',
        username: '4',
        email: 'm.a.riadel.r.osar.io.t.m.p@gmail.com',
        password: '4',
        profileImage:"default.png"
    },
    {
        name: 'Joe',
        username: '5',
        email: 'made.n.r.ay.mo.nd.t.m.p@gmail.com',
        password: '5',
        profileImage:"default.png"
    },
    {
        name: 'Agnes',
        username: '6',
        email: '    j.erij.oseig.na.ciot.m.p@gmail.com',
        password: '6',
        profileImage:"default.png"
    },
    {
        name: 'Ryan',
        username: '7',
        email: '    lyn.d.ia.pa.r.ke.rt.mp@gmail.com',
        password: '7',
        profileImage:"default.png"
    },
    {
        name: 'Jacky',
        username: '8',
        email: 'otatmp+svdph@gmail.com',
        password: '8',
        profileImage:"default.png"
    },
    {
        name: 'Winnie',
        username: '9 ',
        email: '    jaso.n.g.onz.al.e.s.tmp@gmail.com',
        password: '9',
        profileImage:"default.png"
    },
    {
        name: 'hahahhahahahahahhahahaha',
        username: '10',
        email: '    giiftmp+kogsj@gmail.com',
        password: '10',
        profileImage:"default.png"
    },
    {
        name: 'plow',
        username: '11',
        email: '    sizetmp+k7dxh@gmail.com',
        password: '11',
        profileImage:"default.png"
    },
    {
        name: 'firm',
        username: '12',
        email: '    ch.adf.ra.n.c.i.scojrt.mp@gmail.com',
        password: '12',
        profileImage:"default.png"
    },
    {
        name: 'buzz',
        username: '13',
        email: '    feartmp+nidor@gmail.com',
        password: '13',
        profileImage:"default.png"
    },
    {
        name: 'endurable',
        username: '14',
        email: '    h.a.n.n.a.helizabet.ht.mp@gmail.com',
        password: '14',
        profileImage:"default.png"
    },
    {
        name: 'bagel',
        username: '15',
        email: '    be.n.t.l.eep.ricetm.p@gmail.com',
        password: '15',
        profileImage:"default.png"
    }
]

export const messages = [
    {
        content: ` We're so glad to have you here!`,
        user_id: 2,
        channel_id: 2,
        image_id:null
    },
    {
        content: `一般人以為 Programming 是很複雜，遙不可及，即使有很多網上教學，大多數都以英文為主。
        Tecky Academy 製作全新廣東話【一個字Programming】淺白教學片，為任何職業人士、
        待業人士、甚至學生和全職媽媽等的繁忙都市人，可以只需花5分鐘「一個字」的時間去認識
        Programming。我們每星期會上傳一集！讓你輕鬆了解 Programming！`,
        user_id: 3,
        channel_id: 2,
        image_id:null
    },
    {
        content: "Cool!",
        user_id: 1,
        channel_id: 2,
        image_id:null
    },
    {   
        content: `Tecky’s MicroMaster in A.I. & Programming consists of a 16 weeks of full-time
        classes going from 9-6 on every weekday. It is intended for persons of any background
        interested in entering in the IT industry to be a Full Stack Developer`,
        user_id: 2,
        channel_id: 2,
        image_id:1
    },
    {
        content: `Our Micromaster in A.I. & Programming is career-centric in nature, compared with the
        other online or in-person short-term course offerings available in the market.
        Our main aim is to provide the trainees with the technical knowledge of modern
        Full-stack web technology for our trainees to thrive in today’s rapidly evolving
        technology world. During the course, students will be equipped with the technical and
        communication skills required to be a productive member in a real-world software
        development environment.`,
        user_id: 2,
        channel_id: 2,
        image_id:null
    },
    {
        content: `I highly recommend Tecky Academy to anyone who want to start their career
        in software development. The syllabus of Tecky is regularly updated to match with
        the ever-changing industry requirement, and the instructors are both knowledgeable
        and patient to guide you through all the fundamental subjects for a junior developer.`,
        user_id: 3,
        channel_id: 2,
        image_id:null
    }
]

export const channels = [
    {
        name: "Tecky",
        type: "group",
        profileImage: "group_logo_1.png"
    },
    {
        name: "Designer",
        type: "group",
        profileImage: "group_logo_2.png"
    },
    {
        name: "Hobby",
        type: "group",
        profileImage: "group_logo_3.png"
    },
    {
        name: "Game",
        type: "group",
        profileImage: "group_logo_4.png"
    },
    {
        name: "My Cute Cat",
        type: "room",
        profileImage: "default.jpg"
    },
]

export const channelRelations = [
    {
        user_id: 1,
        channel_id: 1,
    },
    {
        user_id: 1,
        channel_id: 2,
    },
        {
        user_id: 1,
        channel_id: 3,
    },
    {
        user_id: 2,
        channel_id: 1,
    },
    {
        user_id: 2,
        channel_id: 2,
    },
    {
        user_id: 2,
        channel_id: 4,
    },
    {
        user_id: 3,
        channel_id: 1,
    },
    {
        user_id: 3,
        channel_id: 2,
    },
    {
        user_id: 3,
        channel_id: 5,
    },
    {
        user_id: 4,
        channel_id: 1,
    },
    {
        user_id: 5,
        channel_id: 1,
    },
    {
        user_id: 6,
        channel_id: 1,
    },
    {
        user_id: 7,
        channel_id: 1,
    },
    {
        user_id: 8,
        channel_id: 1,
    },
    {
        user_id: 9,
        channel_id: 1,
    },
    {
        user_id: 10,
        channel_id: 1,
    },
    {
        user_id: 11,
        channel_id: 1,
    },
    {
        user_id: 12,
        channel_id: 1,
    },
    {
        user_id: 13,
        channel_id: 1,
    },
    {
        user_id: 14,
        channel_id: 1,
    },
    {
        user_id: 15,
        channel_id: 1,
    },
]
export const settings = [
    {
        user_id: 1
    },
    {
        user_id: 2
    },
    {
        user_id: 3
    },
    {
        user_id: 4
    },
    {
        user_id: 5
    },
    {
        user_id: 6
    },
    {
        user_id: 7
    },
    {
        user_id: 8
    },
    {
        user_id: 9
    },
    {
        user_id: 10
    },
    {
        user_id: 11
    },
    {
        user_id: 12
    },
    {
        user_id: 13
    },
    {
        user_id: 14
    },
    {
        user_id: 15
    },
]
export const images = [
    {
        image_name:'message_demo01.jpg'
    }
]