// Renamed below querySelectors to match class names from Daniel's chatroom html
// Rename roomName and room to groupName and group
////////////////////////////////////////////////////////////////environment setup
//Global Variable
const chatForm = document.querySelector("#input-box");
const submitMsgButton = document.querySelector("#submit_btn")
const createGroupButton = document.querySelector("#create_group")
const createGroupButtonMobile = document.querySelector("#create_group_Mobile")
const chatMessages = document.querySelector(".row .message-lists");
const groupNameList = document.querySelector("#groupNameList-in-window");
const mobileGroupNameList = document.querySelector("#groupNameList-in-mobile");
const currentGroup = document.querySelector("#room-title");
const userDetail = document.querySelector("#user-status")
const windowUserList = document.querySelector("#userlist-in-window");
const mobileUserList = document.querySelector("#userlist-in-mobile");
const onlineButton = document.querySelector("#online")
const allButton = document.querySelector("#all")
const mobileOnlineButton = document.querySelector("#online-mobile")
const mobileAllButton = document.querySelector("#all-mobile")
const addFriendButton = document.querySelector("#add-friend-button")
const mobileAddFriendButton = document.querySelector("#add-friend-button-mobile")
// Get username and group from URL
// In progress. Should be extracting from SQL Database here instead of QS (QueryString), but I'm stuck
// Comment by Jason I am not going to use this method 
// const { username, group } = Qs.parse(location.search, {
//   ignoreQueryPrefix: true,
// });

////////////////////////////////////////////////////////////////socket related

const socket = io.connect();

// First loading when the user logs in successfully
socket.on('loadingTheInformation', ({ data }) => {
  //User Data
  // console.log(data);
  loadingUserInfo(data["userInfo"])

  //Add users to DOM
  loadingUsers(data["channelUsersList"])

  loadingUserPersonalChannelList(data["userChannelList"])
  // Add room name to DOM
  loadingCurrentGroupDetail(data["channelDetail"])

  // Message from server
  loadingMessage(data["channelMessages"])



  // Scroll down
  /* It depends on the situation */
  chatMessages.scrollTop = chatMessages.scrollHeight;
})

function loadingUserInfo(userInfo) {

  // console.log(userInfo)
  const name = userInfo.name
  const image = userInfo.profile_image
  userDetail.setAttribute("user-id", `${userInfo.id}`)
  userDetail.innerHTML = /*html*/`
  <img src="./images/users/${image}" class="user-status-logo" id="user-status-logo" alt="G" width="30px">
      <div class="user-status-name" id="user-status-name">
        <h5>${name}</h5>
      </div>
      <div class="btn-group dropup">
        <button type="button" class="btn dropdown-toggle-split" data-bs-toggle="dropdown">
          <i class="bi bi-gear user-status-button" style="font-size: 1.5em;" id="user-status-button"></i>
        </button>
        <ul class="dropdown-menu dropdown-menu-md-end">
          <li>
            <a href="#" class="dropdown-item">User Settings</a>
          </li>
          <li>
            <a href="#" class="dropdown-item">Online</a>
            <a href="#" class="dropdown-item">Offline</a>
            <a class="dropdown-item" style="cursor: pointer" onclick="logout()">Logout</a>
          </li>
        </ul>
      </div>
  `
}

// KY Note: Tried both "channels" and "channelRelations" from dummy.data.ts, group icons showing, but group names not showing properly
function loadingUserPersonalChannelList(channels) {
  groupNameList.innerHTML = ""
  for (const channel of channels) {
    const { id, name } = channel
    const profileImage = channel.profile_image
    const div = document.createElement("div");
    div.classList.add("group-item");
    (id == 1) ? div.classList.add('group-active') : div.classList.add('group-inactive')
    div.setAttribute("onclick", `changeChannel(${id})`)
    // if(id = 1){div.classList.add('group-active')}else{div.classList.add('group-inactive')};
    div.id = `channel-id-${id}`
    div.innerHTML = /*html*/ `

    <img src="./images/groups/${profileImage}" class="group-logo" id="group-logo-${id}" alt="G">
    <div class="group-name" id="channel-id-${id}">
      <h6>${name}</h6>
    </div>
      `
    groupNameList.appendChild(div);
  }
  mobileGroupNameList.innerHTML = ""
  for (const channel of channels) {
    const { id, name } = channel
    const profileImage = channel.profile_image
    const div = document.createElement("div");
    div.classList.add("group-item");
    (id == 1) ? div.classList.add('group-active') : div.classList.add('group-inactive')
    div.setAttribute("onclick", `changeChannel(${id})`)
    // if(id = 1){div.classList.add('group-active')}else{div.classList.add('group-inactive')};
    div.id = `channel_id_${id}`
    div.innerHTML = /*html*/ `

    <img src="./images/groups/${profileImage}" class="group-logo" id="group-logo-${id}" alt="G">
    <div class="group-name" id="channel-id-${id}">
      <h6>${name}</h6>
    </div>
      `
    mobileGroupNameList.appendChild(div);
  }
}

function loadingCurrentGroupDetail(groupInfo) {
  const id = groupInfo.id;
  const name = groupInfo.name
  const profileImage = groupInfo.profile_image
  const username = document.getElementById('user-status-name').innerText
  currentGroup.setAttribute('channel-id', id)
  onlineButton.setAttribute("onclick", `showOnlineList(${id})`)
  mobileOnlineButton.setAttribute("onclick", `showOnlineList(${id})`)
  allButton.setAttribute("onclick", `showAll(${id})`)
  mobileAllButton.setAttribute("onclick", `showAll(${id})`)
  currentGroup.innerHTML = /*html*/`
  <img src="./images/groups/${profileImage}" class="group-logo" id="group-logo" alt="G">
  <h3 class=""id="room-title">Hello,${username}! Welcome to ${name}</h3>  `


}
//old version of loading image
// function loadingMessage(messages) {
//   // console.log(messages)
//   chatMessages.innerHTML = ""
//   for (const message of messages) {
//     let onlineStatus = message.is_online ? "online-circle" : "offline-circle";
//     const div = document.createElement("div");
//     div.classList.add("message-item");
//     div.classList.add("row")
//     const userId = message.id
//     const username = message.name
//     const content = message.content
//     const profileImage = message.profile_image
//     const time = moment(message.created_at).format('HH:mm');
//     const date = moment(message.created_at).format('DD/MM/YYYY');
//     // Ignore below, for reference only
//     //   const p = document.createElement("p");
//     //   p.classList.add("meta");
//     //   p.innerText = message.username;
//     //   p.innerHTML += `<span>${message.time}</span>`;
//     //   div.appendChild(p);
//     //   const para = document.createElement("p");
//     //   para.classList.add("text");
//     //   para.innerText = message.text;
//     //   div.appendChild(para);

//     // Ignore below, for reference only
//     // div.innerHTML = `<p class="meta">${message.user} <span>${message.time}</span></p>
//     //<p class="text">
//     //    ${message.text}
//     //</p>`

//     // Append html to message area.
//     // Not sure how to append each User Logo PNG
//     div.innerHTML = /*html*/ `
//         <div class="row">
//     <hr class="message-lists-hr">
//   </div>
//   <div class="row">
//     <div class="col message-time ">
//       <div class="time">${time}</div>
//       <div class="date">${date}</div>
//     </div>
//     <div class="col message-user-item ">
//       <img src="./images/users/${profileImage}" alt="F" class="user-logo">
//       <div class="${onlineStatus} change-online" id="user-id-${userId}"></div>
//       <div class="user-name" id="user-name">
//         <h5>${username}</h5>
//       </div>
//     </div>
//   </div>
//   <div class="row">
//     <div class="col message-time"></div>
//     <div class="col message-body ">
//       <p>
//         ${content}
//       </p>
//     </div>
//   </div>`;

//     chatMessages.appendChild(div);
//   }
//   chatMessages.scrollTop = chatMessages.scrollHeight
// }

function loadingMessage(messages) {
  // console.log(messages)
  chatMessages.innerHTML = ""
  for (const message of messages) {
    let onlineStatus = message.is_online ? "online-circle" : "offline-circle";
    const div = document.createElement("div");
    div.classList.add("message-item");
    div.classList.add("row")
    const userId = message.id
    const username = message.name
    const content = message.content
    const profileImage = message.profile_image
    let image;
    if (message.image_name) {

      image =
        `<img src="./uploads/messages_attachment/${message.image_name}" class="message-image" alt="">`
    } else { image = "" }
    const time = moment(message.created_at).format('HH:mm');
    const date = moment(message.created_at).format('DD/MM/YYYY');
    // Ignore below, for reference only
    //   const p = document.createElement("p");
    //   p.classList.add("meta");
    //   p.innerText = message.username;
    //   p.innerHTML += `<span>${message.time}</span>`;
    //   div.appendChild(p);
    //   const para = document.createElement("p");
    //   para.classList.add("text");
    //   para.innerText = message.text;
    //   div.appendChild(para);

    // Ignore below, for reference only
    // div.innerHTML = `<p class="meta">${message.user} <span>${message.time}</span></p>
    //<p class="text">
    //    ${message.text}
    //</p>`

    // Append html to message area.
    // Not sure how to append each User Logo PNG
    div.innerHTML = /*html*/ `
        <div class="row">
    <hr class="message-lists-hr">
  </div>
  <div class="row">
    <div class="col message-time ">
      <div class="time">${time}</div>
      <div class="date">${date}</div>
    </div>
    <div class="col message-user-item ">
      <img src="./images/users/${profileImage}" alt="F" class="user-logo">
      <div class="${onlineStatus} change-online" id="user-id-${userId}"></div>
      <div class="user-name" id="user-name">
        <h5>${username}</h5>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col message-time"></div>
    <div class="col message-body ">
      ${image}
      <p>
        ${content}
      </p>
    </div>
  </div>`;
    chatMessages.appendChild(div);
  }
  chatMessages.scrollTop = chatMessages.clientHeight
}

function loadingUsers(users) {

  // Ignore below, for reference only
  //   windowUserList.innerHTML = "";
  //   users.forEach((user) => {
  //     const li = document.createElement("li");
  //     li.innerText = user.username;
  //     windowUserList.appendChild(li);
  //   });

  // Ignore below, for reference only
  //   windowUserList.innerHTML = `
  //    ${users.map((user) => `<li>${user.username}</li>`).join("")}
  //   `;

  // Append html to user status list
  // Not sure how to append User Logo PNG and Online/Offline circle class
  // Comment by Jason we should use for loop here
  windowUserList.innerHTML = ""


  for (const user of users) {
    const name = user.name
    const id = user.user_id
    const profileImage = user.profile_image
    const div = document.createElement("div");
    div.classList.add("user-item");
    div.classList.add("all-list")
    div.id = `user_id-${user.user_id}`
    let onlineStatus = user.is_online ? "online-circle" : "offline-circle";
    div.innerHTML = /*html*/ `

        <img src="./images/users/${profileImage}" alt="F" class="user-logo">
        <div class="${onlineStatus}"></div>
        <div class="user-name" id="user_id-${id}">
          <h5 class="m-1 ">${name}</h5>
        </div>`
    windowUserList.appendChild(div);
  }
  mobileUserList.innerHTML = ""
  for (const user of users) {
    const name = user.name
    const id = user.user_id
    const profileImage = user.profile_image
    const div = document.createElement("div");
    div.classList.add("user-item");
    div.classList.add("all-list")
    div.id = `user_id-${user.user_id}`
    let onlineStatus = user.is_online ? "online-circle" : "offline-circle";
    div.innerHTML = /*html*/ `

        <img src="./images/users/${profileImage}" alt="F" class="user-logo">
        <div class="${onlineStatus}"></div>
        <div class="user-name" id="user_id-${id}">
          <h5 class="m-1 ">${name}</h5>
        </div>`
    mobileUserList.appendChild(div);
  }

}

//change to channel/group data (name,messages,channel list) to show the all users after user click the "all user" button
socket.on('changedChannelData', ({ changedChannelData }) => {
  // console.log(changedChannelData)
  outputChangeChannel(changedChannelData)
})

function outputChangeChannel(changedChannelData) {
  // console.log(changedChannelData["channelUsersList"])
  loadingMessage(changedChannelData["channelMessages"])
  loadingCurrentGroupDetail(changedChannelData["channelDetail"])
  loadingUsers(changedChannelData["channelUsersList"])
}

//auto update the channel/group list when other users log in  
socket.on('new-online', ({ user, channelId }) => {
  // console.log(user)
  const currentId = currentGroup.getAttribute('channel-id')
  if (channelId == currentId) {

    const name = user.name
    const id = user.id
    const idCheckerForUserList = `user_id-${id}`
    const idCheckerForMessage = `user-id-${id}`
    const foundUser = document.getElementById(idCheckerForUserList)
    const profileImage = user.profile_image
    const allUserList = document.querySelector(".all-list")
    const shownUsersOnChatBox = document.querySelectorAll(".change-online")
    for (const shownUserOnChatBox of shownUsersOnChatBox) {
      // console.log('idChecker: ' + idChecker)
      // console.log('shownchecker: ' + shownUserOnChatBox.id)
      if (idCheckerForMessage == shownUserOnChatBox.id) {
        shownUserOnChatBox.classList.remove("offline-circle")
        shownUserOnChatBox.classList.add("online-circle")
      }
      const channel = currentGroup.getAttribute("channel-id")
      if (allUserList) {
        showAll(channel)
      } else {
        showOnlineList(channel)
        /*    Original Method   */
        // console.log(`${idCheckerForUserList}`)
        // document.getElementById(`${idCheckerForUserList}`).remove();
      }



    }
    //Can change the logic by search the class online-list or all-list
  }
  // for (const shownUser of shownUsers) {
  //   if (idChecker!== shownUser.id) {
  //     shownUser.classList.remove("offline-circle")
  //     shownUser.classList.add("online-circle")
  //   } else {

  //   }
  // }
  ///wanna try node list to array and find the corresponding user
})

//auto update the channel/group list when other users log out or close the window  
socket.on('new-offline', ({ user, channelId }) => {
  // console.log(user)
  const currentId = currentGroup.getAttribute('channel-id')
  if (channelId == currentId) {
    const name = user.name
    const id = user.id
    const profileImage = user.profile_image
    const allUserList = document.querySelector(".all-list")
    // console.log('from new-offline', allUserList)
    const idCheckerForUserList = `user_id-${id}`
    const idCheckerForMessage = `user-id-${id}`
    const foundUser = document.getElementById(idCheckerForUserList)
    // const shownUsersOnChatArea = 


    // function checkTheListType(){
    //   return shownUsers.forEach((shownUser) => {
    //       return shownUser.querySelector(".offline-circle")?true:false;
    //   })
    // }
    //check the page is online list or all list 
    // function checkTheListType() {
    //   for (const shownUser of shownUsers) {
    //     if (shownUser.querySelector(".offline-circle")) { return true }
    //   }
    //   return false
    // }
    const channel = currentGroup.getAttribute("channel-id")
    if (allUserList) {
      showAll(channel)
    } else {
      showOnlineList(channel)
      /*    Original Method   */
      // console.log(`${idCheckerForUserList}`)
      // document.getElementById(`${idCheckerForUserList}`).remove();
    }
    // for (const shownUser of shownUsers) {

    //   if (idCheckerForUserList == shownUser.id) {
    //     shownUser.classList.remove("online-circle")
    //     shownUser.classList.add("offline-circle")
    //   } else {
    //     const div = document.getElementById(`user-id-${id}`);
    //     windowUserList.removeChild(div);
    //   }
    // }
    const shownUsersOnChatBox = document.querySelectorAll(".change-online")
    for (const shownUserOnChatBox of shownUsersOnChatBox) {
      if (idCheckerForMessage == (shownUserOnChatBox.id)) {
        shownUserOnChatBox.classList.remove("online-circle")
        shownUserOnChatBox.classList.add("offline-circle")
      }
    }
  }
})

function showAll(channel) {
  socket.emit('allUsers', channel)
}

function showOnlineList(channel) {
  // console.log(channel)
  socket.emit('onlineUsers', channel)
}

//change channel/group list which shows the all users after user click the "all user" button
socket.on('allUserList', ({ channelUsersList }) => {
  outputAllMembers(channelUsersList)
})

//change channel/group list which shows the online users only after user click the "online" button
socket.on('onlineUsersList', ({ channelUsersList }) => {
  outputOnlineList(channelUsersList)
})

function outputAllMembers(list) {
  // console.log(list)
  windowUserList.innerHTML = ""
  for (const user of list) {
    const name = user.name
    const id = user.user_id
    const profileImage = user.profile_image
    const div = document.createElement("div");
    div.classList.add("user-item");
    div.classList.add("all-list")
    div.id = `user_id-${user.user_id}`
    let onlineStatus = user.is_online ? "online-circle" : "offline-circle";;
    div.innerHTML = /*html*/ `
      <img src="./images/users/${profileImage}" alt="F" class="user-logo">
      <div class="${onlineStatus}"></div>
      <div class="user-name">
        <h5 class="m-1 ">${name}</h5>
      </div>`
    windowUserList.appendChild(div);
  }
  mobileUserList.innerHTML = ""
  for (const user of list) {
    const name = user.name
    const id = user.user_id
    const profileImage = user.profile_image
    const div = document.createElement("div");
    div.classList.add("user-item");
    div.classList.add("all-list")
    div.id = `user_id-${user.user_id}`
    let onlineStatus = user.is_online ? "online-circle" : "offline-circle";;
    div.innerHTML = /*html*/ `
      <img src="./images/users/${profileImage}" alt="F" class="user-logo">
      <div class="${onlineStatus}"></div>
      <div class="user-name">
        <h5 class="m-1 ">${name}</h5>
      </div>`
    mobileUserList.appendChild(div);
  }



}

function outputOnlineList(list) {
  // console.log(list)

  windowUserList.innerHTML = ""

  for (const user of list) {
    const name = user.name
    const id = user.user_id
    const profileImage = user.profile_image
    if (user.is_online) {
      const div = document.createElement("div");
      div.classList.add("user-item");
      div.classList.add("online-list")
      div.id = `user_id-${user.user_id}`
      div.innerHTML = /*html*/ `
          <img src="./images/users/${profileImage}" alt="F" class="user-logo">
          <div class="online-circle"></div>
          <div class="user-name">
            <h5 class="m-1 ">${name}</h5>
          </div>`
      windowUserList.appendChild(div);
    }
  }
  mobileUserList.innerHTML = ""
  for (const user of list) {
    const name = user.name
    const id = user.user_id
    const profileImage = user.profile_image
    if (user.is_online) {
      const div = document.createElement("div");
      div.classList.add("user-item");
      div.classList.add("online-list")
      div.id = `user_id-${user.user_id}`
      div.innerHTML = /*html*/ `
          <img src="./images/users/${profileImage}" alt="F" class="user-logo">
          <div class="online-circle"></div>
          <div class="user-name">
            <h5 class="m-1 ">${name}</h5>
          </div>`

      mobileUserList.appendChild(div);
    }
  }
}

socket.on('being-added', (userId) => {
  const idOnPage = userDetail.getAttribute("user-id")
  console.log(userId)
  console.log(idOnPage)
  if (userId == idOnPage) {
    socket.emit('new channel list', userId)
  }
})

socket.on('new channel list given', (channels) => {

  const currentGroupId = currentGroup.getAttribute('channel-id')
  groupNameList.innerHTML = ""
  for (const channel of channels) {
    const { id, name } = channel
    const profileImage = channel.profile_image
    const div = document.createElement("div");
    div.classList.add("group-item");
    (id == currentGroupId) ? div.classList.add('group-active') : div.classList.add('group-inactive')
    div.setAttribute("onclick", `changeChannel(${id})`)
    // if(id = 1){div.classList.add('group-active')}else{div.classList.add('group-inactive')};
    div.id = `channel-id-${id}`
    div.innerHTML = /*html*/ `

    <img src="./images/groups/${profileImage}" class="group-logo" id="group-logo-${id}" alt="G">
    <div class="group-name" id="channel-id-${id}">
      <h6>${name}</h6>
    </div>
      `
    groupNameList.appendChild(div);
  }
  mobileGroupNameList.innerHTML = ""
  for (const channel of channels) {
    const { id, name } = channel
    const profileImage = channel.profile_image
    const div = document.createElement("div");
    div.classList.add("group-item");
    (id == currentGroupId) ? div.classList.add('group-active') : div.classList.add('group-inactive')
    div.setAttribute("onclick", `changeChannel(${id})`)
    // if(id = 1){div.classList.add('group-active')}else{div.classList.add('group-inactive')};
    div.id = `channel_id_${id}`
    div.innerHTML = /*html*/ `

    <img src="./images/groups/${profileImage}" class="group-logo" id="group-logo-${id}" alt="G">
    <div class="group-name" id="channel-id-${id}">
      <h6>${name}</h6>
    </div>
      `
    mobileGroupNameList.appendChild(div);
  }


})

//function for received the message from the page to server
sendMessage()

createChannel()

addFriend()

search()
////////////////////////////////////////////////////////////////

// socket.on("groupUsers", ({ group, users }) => {
//   outputGroupName(group);
//   outputUsers(users);
// });

// Message from server
// socket.on("message", (message) => {
//   console.log(message);
//   outputMessage(message);

//   // Scroll down
//   chatMessages.scrollTop = chatMessages.scrollHeight;
// });

// First loading related


// Old version Message submit
//  
// chatForm.addEventListener("submit", (event) => {
//     event.preventDefault();

//     // Get message text
//     let msg = event.target.elements.msg.value;
//     //The "msg" in e.target.elements.msg.value refers to "class:"input-box" in html file

//     msg = msg.trim();

//     if (!msg) {
//         return false;
//     }
//KY method
// chatForm.addEventListener("click", (event) => {
//   event.preventDefault();
//   const message = chatForm.value;

//   if (message === "") return;
//   displayMessage(message);
//   socket.emit("send-message", message);

//   chatForm.value = "";
// });
// function displayMessage(message) {
// 	const div = document.createElement("div")
// 	div.textContent = message
// 	document.getElementById("message-container").append(div)
// }

/*    If people know how to amend to code * server fail    */
function sendMessage() {
  // Press Enter will fireup Submit Button Click Event
  // chatForm.addEventListener("keyup",(event)=>{
  //   if(event.keyCode===13){
  //     event.preventDefault();
  //     submitMsgButton.click();
  //   }
  // });

  // const resp = await fetch (`/sendmessage`,{
  //   method:"POST",
  //   body:formData,
  // })
  submitMsgButton.addEventListener("click", async () => {
    const formData = new FormData();
    const file = document.getElementById("image-upload").files[0]
    if (file) { console.log(file) }
    const userId = userDetail.getAttribute("user-id")
    const channelId = currentGroup.getAttribute('channel-id')
    const message = chatForm.value;
    if (message === "" && file == undefined) { return }
    formData.append("userId", userId)
    formData.append("channelId", channelId)
    formData.append("message", message)
    if (file) { formData.append("image", file) }
    const resp = await fetch("/sendMessage", {
      method: "POST",
      body: formData,
    })
    // const filePlace = document.getElementById("image-upload").value
    // console.log(filePlace)
    chatForm.value = ""
    document.getElementById("image-upload").value = ""
    // const filePlaceUpdated = document.getElementById("image-upload").value
    // console.log(filePlaceUpdated)
    if (resp.status == 200) {
      console.log("success to use the restful api")
      const messageInfo = {
        userId: userId,
        channelId: channelId,
      }
      socket.emit("send-message", messageInfo);
      chatForm.value = ""
    }
    // const userId = userDetail.getAttribute("user-id")
    // const channelId = currentGroup.getAttribute('channel-id')
    // console.log(userId)
    // console.log(channelId)
    // const message = chatForm.value;
    // console.log(message)

    // chatForm.value = ""
  })
  chatForm.addEventListener("keydown", async (event) => {
    const keyChoice = event.keyCode // 
    if (keyChoice == 13) {
      const formData = new FormData();
      const file = document.getElementById("image-upload").files[0]
      // if (file) { console.log(file) }
      const userId = userDetail.getAttribute("user-id")
      const channelId = currentGroup.getAttribute('channel-id')
      const message = chatForm.value;
      if (message === "" && file == undefined) { return }
      formData.append("userId", userId)
      formData.append("channelId", channelId)
      formData.append("message", message)
      if (file) { formData.append("image", file) }
      const resp = await fetch("/sendMessage", {
        method: "POST",
        body: formData,
      })
      chatForm.value = ""
      document.getElementById("image-upload").value = ""
      if (resp.status == 200) {
        console.log("success to use the restful api")
        const messageInfo = {
          userId: userId,
          channelId: channelId,
        }
        socket.emit("send-message", messageInfo);
        chatForm.value = ""
      }
    }
    // const keyChoice = event.keyCode // 
    // if (keyChoice == 13) {
    //   const userId = userDetail.getAttribute("user-id")
    //   const channelId = currentGroup.getAttribute('channel-id')
    //   // console.log(userId)
    //   // console.log(channelId)
    //   const message = chatForm.value;
    //   // console.log(message)
    //   if (message === "") return;
    //   const messageInfo = {
    //     userId: userId,
    //     channelId: channelId,
    //     message: message,
    //   }
    //   socket.emit("send-message", messageInfo);
    //   chatForm.value = ""
    // }
  })
}

//auto update the chat when users send the message in the channel/group
socket.on('new-message', ({ user, message, channelId }) => {
  const currentId = currentGroup.getAttribute('channel-id')
  if (channelId == currentId) {
    let onlineStatus = user.is_online ? "online-circle" : "offline-circle";
    const div = document.createElement("div");
    div.classList.add("message-item");
    div.classList.add("row")
    const profileImage = user.profile_image
    let image;
    if (message.image_name) {
      image =
        `<img src="./uploads/messages_attachment/${message.image_name}" class="message-image" alt="">`
    } else { image = "" }
    const name = user.name
    const id = user.id
    const content = message.content
    const time = moment(message.created_at).format('HH:mm');
    const date = moment(message.created_at).format('DD/MM/YYYY');

    div.innerHTML = /*html*/ `
        <div class="row">
      <hr class="message-lists-hr">
      </div>
      <div class="row">
      <div class="col message-time ">
      <div class="time">${time}</div>
      <div class="date">${date}</div>
      </div>
      <div class="col message-user-item ">
      <img src="./images/users/${profileImage}" alt="F" class="user-logo">
      <div id="user-id-${id}"class="${onlineStatus} change-online"></div>
      <div class="user-name" id="user-name">
        <h5>${name}</h5>
      </div>
      </div>
      </div>
      <div class="row">
      <div class="col message-time"></div>
      <div class="col message-body ">
      ${image}
      <p>
        ${content}
      </p>
      </div>
      </div>`;

    chatMessages.appendChild(div);
  }
  chatMessages.scrollTop = chatMessages.scrollHeight
})

function createChannel() {
  createGroupButton.addEventListener("click", () => {
    const newGroupName = prompt(`Please enter your group name. `);
    console.log(newGroupName.length);
    if (newGroupName.length == 0) {
      alert(`Please enter the name at least one character`)
    }
    if (newGroupName == null || newGroupName.length == 0) { return }
    const userId = userDetail.getAttribute("user-id")
    const type = createGroupButton.getAttribute("channel_type");
    const data = {
      channelName: newGroupName,
      userId: userId,
      type: type
    }
    console.log(data)
    socket.emit("create_channel", data)


    //error if putting here
    //   socket.on('new-group',({id, name,profile_image})=>{
    // const profileImage = profile_image
    // const div = document.createElement("div");
    // div.classList.add("group-item");
    // (id == 1) ? div.classList.add('group-active') : div.classList.add('group-inactive')
    // div.setAttribute("onclick", `changeChannel(${id})`)
    // // if(id = 1){div.classList.add('group-active')}else{div.classList.add('group-inactive')};
    // div.id = `channel-id-${id}`
    // div.innerHTML = /*html*/ `

    // <img src="./images/groups/${profileImage}" class="group-logo" id="group-logo-${id}" alt="G">
    // <div class="group-name" id="channel-id-${id}">
    //   <h6>${name}</h6>
    // </div>
    //   `

    //   groupNameList.appendChild(div);
    // })
    // // console.log(userId)
    // // console.log(channelId)
    // const message = chatForm.value;
    // // console.log(message)
    // if (message === "") return;
    // const messageInfo = {
    //   userId: userId,
    //   channelId: channelId,
    //   message: message,
    // }
    // socket.emit("send-message", messageInfo);
    // chatForm.value = ""
  })

  createGroupButtonMobile.addEventListener("click", () => {
    const newGroupName = prompt(`Please enter your group name. `);
    console.log(newGroupName.length);
    if (newGroupName.length == 0) {
      alert(`Please enter the name at least one character`)
    }
    if (newGroupName == null || newGroupName.length == 0) { return }
    const userId = userDetail.getAttribute("user-id")
    const type = createGroupButtonMobile.getAttribute("channel_type");
    const data = {
      channelName: newGroupName,
      userId: userId,
      type: type
    }
    console.log(data)
    socket.emit("create_channel", data)
  })

}

socket.on('new-channel', (channels) => {
  // const profileImage = profile_image
  // const div = document.createElement("div");
  // div.classList.add("group-item");
  // (id == 1) ? div.classList.add('group-active') : div.classList.add('group-inactive')
  // div.setAttribute("onclick", `changeChannel(${id})`)
  // // if(id = 1){div.classList.add('group-active')}else{div.classList.add('group-inactive')};
  // div.id = `channel-id-${id}`
  // div.innerHTML = /*html*/ `

  // <img src="./images/groups/${profileImage}" class="group-logo" id="group-logo-${id}" alt="G">
  // <div class="group-name" id="channel-id-${id}">
  //   <h6>${name}</h6>
  // </div>
  //   `

  // groupNameList.appendChild(div);
  const currentGroupId = currentGroup.getAttribute('channel-id')
  groupNameList.innerHTML = ""
  for (const channel of channels) {
    const { id, name } = channel
    const profileImage = channel.profile_image
    const div = document.createElement("div");
    div.classList.add("group-item");
    (id == currentGroupId) ? div.classList.add('group-active') : div.classList.add('group-inactive')
    div.setAttribute("onclick", `changeChannel(${id})`)
    // if(id = 1){div.classList.add('group-active')}else{div.classList.add('group-inactive')};
    div.id = `channel-id-${id}`
    div.innerHTML = /*html*/ `

    <img src="./images/groups/${profileImage}" class="group-logo" id="group-logo-${id}" alt="G">
    <div class="group-name" id="channel-id-${id}">
      <h6>${name}</h6>
    </div>
      `
    groupNameList.appendChild(div);
  }
  mobileGroupNameList.innerHTML = ""
  for (const channel of channels) {
    const { id, name } = channel
    const profileImage = channel.profile_image
    const div = document.createElement("div");
    div.classList.add("group-item");
    (id == currentGroupId) ? div.classList.add('group-active') : div.classList.add('group-inactive')
    div.setAttribute("onclick", `changeChannel(${id})`)
    // if(id = 1){div.classList.add('group-active')}else{div.classList.add('group-inactive')};
    div.id = `channel_id_${id}`
    div.innerHTML = /*html*/ `

    <img src="./images/groups/${profileImage}" class="group-logo" id="group-logo-${id}" alt="G">
    <div class="group-name" id="channel-id-${id}">
      <h6>${name}</h6>
    </div>
      `
    mobileGroupNameList.appendChild(div);
  }
})
//Comment
function addFriend() {
  addFriendButton.addEventListener("click", () => {
    const channelId = currentGroup.getAttribute('channel-id')
    if (channelId == 1) {
      alert("Cant add friend at the main Group")
      return
    }
    const newFriend = prompt(`Please enter the name of your friend.`);
    if (newFriend.length == 0) {
      alert(`Please enter the name at least one character`)
    }
    if (newFriend == null || newFriend.length == 0) { return }
    const data = {
      channelId: channelId,
      name: newFriend
    }
    socket.emit("add-friend", data)
  })
  mobileAddFriendButton.addEventListener("click", () => {
    const channelId = currentGroup.getAttribute('channel-id')
    if (channelId == 1) {
      alert("Cant add friend at the main Group")
      return
    }
    const newFriend = prompt(`Please enter the name of your friend.`);
    if (newFriend.length == 0) {
      alert(`Please enter the name at least one character`)
    }
    if (newFriend == null || newFriend.length == 0) { return }
    const data = {
      channelId: channelId,
      name: newFriend
    }
    socket.emit("add-friend", data)
  })
}

function search() {
  let searchBar = document.getElementById('searchBar');
  searchBar.addEventListener("keyup",filterName)

    function filterName() {
      const filterWord = document.getElementById('searchBar').value.toUpperCase()
      console.log('hih')
      const users = document.querySelectorAll(".user-item")
      for (const user of users) {
        const name = user.querySelector(".user-name")
        if(name.innerText.toUpperCase().indexOf(filterWord) > -1){
          user.style.display ="";
        } else {
          user.style.display = "none"
          console.log('hihi')
        }
      }
    }
    
}

socket.on('friend-added', ({ channelId }) => {
  const currentId = currentGroup.getAttribute('channel-id')
  if (channelId == currentId) {
    const allUserList = document.querySelector(".all-list")
    if (allUserList) {
      showAll(channelId)
    } else {
      showOnlineList(channelId)
    }
  }
})

socket.on("no such user", (data) => {
  alert("You want to play 9 me. You try another method la")
})

socket.on('Already in the group', (data) => {
  alert("It seems you love your friend very much. There is another way to let my server down")
})

socket.on('being-added', (userId) => {
  const idOnPage = userDetail.getAttribute("user-id")
  console.log(userId)
  console.log(idOnPage)
  if (userId == idOnPage) {
    socket.emit('new channel list', userId)
  }
})

socket.on('new channel list given', (channels) => {

  const currentGroupId = currentGroup.getAttribute('channel-id')
  groupNameList.innerHTML = ""
  for (const channel of channels) {
    const { id, name } = channel
    const profileImage = channel.profile_image
    const div = document.createElement("div");
    div.classList.add("group-item");
    (id == currentGroupId) ? div.classList.add('group-active') : div.classList.add('group-inactive')
    div.setAttribute("onclick", `changeChannel(${id})`)
    // if(id = 1){div.classList.add('group-active')}else{div.classList.add('group-inactive')};
    div.id = `channel-id-${id}`
    div.innerHTML = /*html*/ `

    <img src="./images/groups/${profileImage}" class="group-logo" id="group-logo-${id}" alt="G">
    <div class="group-name" id="channel-id-${id}">
      <h6>${name}</h6>
    </div>
      `
    groupNameList.appendChild(div);
  }
  mobileGroupNameList.innerHTML = ""
  for (const channel of channels) {
    const { id, name } = channel
    const profileImage = channel.profile_image
    const div = document.createElement("div");
    div.classList.add("group-item");
    (id == currentGroupId) ? div.classList.add('group-active') : div.classList.add('group-inactive')
    div.setAttribute("onclick", `changeChannel(${id})`)
    // if(id = 1){div.classList.add('group-active')}else{div.classList.add('group-inactive')};
    div.id = `channel_id_${id}`
    div.innerHTML = /*html*/ `

    <img src="./images/groups/${profileImage}" class="group-logo" id="group-logo-${id}" alt="G">
    <div class="group-name" id="channel-id-${id}">
      <h6>${name}</h6>
    </div>
      `
    mobileGroupNameList.appendChild(div);
  }


})

function changeChannel(number) {
  /* Logic:Select current channel from the channel list by the id 
  change the class name which having css to change the display formal display
  if clicking the current channel, nothing will happen.
  Then send the chosen channel to server and 
  wait the server to provide the new channel info */

  const currentChannel = document.querySelector(`#channel-id-${number}`)
  const items = document.querySelectorAll(".group-item")
  const activeValue = "group-item group-active"
  const inactiveValue = "group-item group-inactive"

  // console.log(currentItem)
  // console.log(items)
  const className = currentChannel.className
  console.log(className)
  const id = `channel-id-${number}`
  if (className == activeValue) { return }
  if (className == inactiveValue) {
    for (const item of items) {
      if (item.id != id) {
        item.className = inactiveValue
      }
    }
    document.querySelector(`#channel-id-${number}`).className = activeValue
    document.querySelector(`#channel_id_${number}`).className = activeValue
    const channel = number
    socket.emit("changeChannel", channel)
    return
  }
}

// Ref: Prompt the user before leave chat room
// function logout(){
//   const leaveRoom = confirm("Leave chat?");
//   if (leaveRoom) {
//     window.location = "../index.html";
//   } else {
//   }
// };

// function fakeStatus(status){
//   const userId = userDetail.getAttribute("user-id")
//   if(status =='on'){

//   }
//   if(status =='off'){

//   }
// }

async function logout() {
  const leaveRoom = confirm("Leave chat?");
  if (leaveRoom) {
    // const id = userDetail.userid.value
    const id = userDetail.getAttribute("user-id")
    // const channel = currentGroup.getAttribute("channel-id")
    // const data = {
    //   id: id,
    //   channel: channel
    // }
    const resp = await fetch('/logout')
    socket.emit('leaveRoom', id)
    // ,{
    //   method: "POST",
    //   headers: { "content-type": "application/json" },
    //   body: JSON.stringify({id:id})
    // })
    if (resp.status == 200) {
      window.location = "../index.html";
    }
  } 
}
// leaveWindow()
// async function leaveWindow() {
//   const leaveRoom = confirm("Leave chat?");
//   if (leaveRoom) {
//     // const id = userDetail.userid.value
//     const id = userDetail.getAttribute("user-id")
//     // const channel = currentGroup.getAttribute("channel-id")
//     // const data = {
//     //   id: id,
//     //   channel: channel
//     // }
//     await fetch('/leaveWindow')
//     socket.emit('leaveRoom', id)
//     // ,{
//     //   method: "POST",
//     //   headers: { "content-type": "application/json" },
//     //   body: JSON.stringify({id:id})
//     // })
//   }
// }

// window.addEventListener("beforeunload", function (event) {
//   const leaveRoom = confirm("Leave chat?");
//   event.preventDefault()
//   leaveWindow()
//   console.log("hihi")
// })


