const stockIdList = []
const stockCodeList = []
const stockPriceList=[]
window.onload=async function(){

  const result = await getData()
  for (stock of result) {
    stockIdList.push(stock.stock_id);
    stockCodeList.push(stock.stock_code)
    stockPriceList.push(stock.close)
  }
  autocomplete(document.getElementById("myInput"), stockCodeList);
  await session();
}

async function getData() {
  const resp = await fetch("/trading/get")
  const result = await resp.json();
  return result
}

const dateStart = MCDatepicker.create({
	el: '#dateStart',
  bodyType:'inline',
  closeOnBlur:true,
});
const dateEnd = MCDatepicker.create({
	el: '#dateEnd',
  bodyType:'inline',
  closeOnBlur:true,
});

function ranPrice() {
  return Math.floor(Math.random() * (100000 - 10) + 10);
}

function genStockData(stock,nData) {
  let data = [];
  let labels = [];
  for (let i = 1; i <= nData; i++) {
    data.push(ranPrice());
    labels.push(i);
  }
  const stockData = {
    labels: labels,
    datasets: [
      {
        label: stock,
        data: data,
        fill: true,
        borderColor: "rgb(75,192,192)",
      },
    ],
  };

  return stockData;
}

const miniChart_config = {
  responsive: false,
  mintainAspectRatio: false,
  elements: {
    point: {
      radius: 0,
    },
  },
  plugins: {
    title: {
      display: false,
    },
    legend: {
      display: false,
    },
    tooltips: {
      enabled: false,
    },
  },
  scales: {
    y: {
      display: false,
      ticks: {
        display: false,
      },
    },
    x: {
      display: false,
      ticks: {
        display: false,
      },
    },
  },
};

function draw_miniStockChart() {
  const ctx_mini_stockCharts = document.querySelectorAll(".stock-list");
  for (const stock of ctx_mini_stockCharts) {
    const miniChartctx = stock.querySelector("#mini-stockchart");
    const miniChart = new Chart(miniChartctx, {
      type: "line",
      data: genStockData("TWTR",7),
      options: miniChart_config,
    });
  }
}
const ctx = document.getElementById("trading-chart");


const tradingChart = new Chart(ctx, {
  type: "line",
  data: genStockData("SPX",20),
});

draw_miniStockChart();
async function session() {
  document.querySelector("#stockadd_btn").addEventListener("click", async (event) => {
    event.preventDefault();
    const input = document.querySelector("#myInput").value
    const object={};
    for (let i in stockCodeList) {
      if (stockCodeList[i] === input) {
        object['stockId']=stockIdList[i],
        object['stockPrice']=stockPriceList[i]
      }}
      console.log(object.stockId,object.stockPrice)
      const resp=await fetch("/trading/search", {
        method: "PUT",
        headers: { "content-type": "application/json" },
        body: JSON.stringify(object)
      })
      console.log(resp.status)
      if(resp.status===300){
          location.replace(`/tradePage/trading${object.stockId}.html`)
      }
    }
    )}
// async function session() {
//   document.querySelector("#stockadd_btn").addEventListener("click", async (event) => {
//     event.preventDefault();
//     const input = document.querySelector("#myInput").value
//     for (let i in stockCodeList) {
//       if (stockCodeList[i] === input) {
//         const object={
//           stockId:stockIdList[i],
//           stockPrice:stockPriceList[i]
//         }
//         await fetch("/trading/search", {
//           method: "PUT",
//           headers: { "content-type": "application/json" },
//           body: JSON.stringify(object)
//         })
//       }
//     }
//     const stock = document.getElementById("myInput").value;
//     const watchlist = document.getElementById("watchlist");
//     console.log(stock)
//     watchlist.innerHTML = watchlist.innerHTML + /*html*/`
//     <div class="stock-list wrapper d-flex align-items-center justify-content-between py-2 border-bottom">
//       <div class="d-flex">
//        <div class="stock-name me-3">${stock}</div>
//        <canvas id="mini-stockchart" class="mini-stockchart"></canvas>
//       </div>
//       <div class="stock-price">$1,200</div>
//     </div>
//     `
  
//     draw_miniStockChart(); 
//   })
// }

