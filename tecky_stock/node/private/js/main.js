loadingIcon() 



async function loadingIcon() {
    const iconDiv = document.getElementById("user-dropdown_div")
    const resp = await fetch("/profile")
    const userInfo = await resp.json()
    const { profile_image } = userInfo
  
    iconDiv.innerHTML =/*html*/`<a href="#" class="nav-link dropdown-toggle user-dropdown" role="button" id="user-dropdown"
            data-bs-toggle="dropdown" aria-expanded="false">
            <img style="border-radius: 50%" src="images/users/${profile_image}" width="32" height="32" alt="">
            </a>
            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="user-dropdown">
            <li><a href="/profile.html" class="dropdown-item">Profile</a></li>
            <li><a href="/wallet.html" class="dropdown-item">Wallet</a></li>
            <li><a href="/user/logout" class="dropdown-item">Sign out</a></li>
            </ul>`
  }
