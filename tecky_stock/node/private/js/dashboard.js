let items = document.querySelectorAll('.carousel .carousel-item')

items.forEach((el) => {
  const minPerSlide = 4
  let next = el.nextElementSibling
  for (var i = 1; i < minPerSlide; i++) {
    if (!next) {
      // wrap carousel by using first child
      next = items[0]
    }
    let cloneChild = next.cloneNode(true)
    el.appendChild(cloneChild.children[0])
    next = next.nextElementSibling
  }
})

async function logout() {
  const leaveRoom = confirm("Leave stock?");
  if (leaveRoom) {

    const resp = await fetch('/user/logout')

    if (resp.status == 200) {
      window.location = "/";
    }
  }

}

const ctx_dashboard_chart = document.getElementById("dashboard-chart");


const stockData = {
  labels: [1, 2, 3, 4, 5, 6, 7],
  datasets: [
    {
      label: "S&P 500",
      data: [4500, 4530, 4423, 4624, 4899, 4123, 4412],
      fill: false,
      borderColor: "rgb(75,192,192)",
      tension: 0.1,
    },
  ],
};

const tradingChart = new Chart(ctx_dashboard_chart, {
  type: "line",
  data: stockData,
});

const stock_slider = document.querySelector("#stock-slider");
stock_slider.style.pointerEvents = "none"
function loadchart() {
  const ctx_pro_chart = document.querySelector("#stock-chart-pro");
  ctx_pro_chart.innerHTML = `
                        <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                        <script type="text/javascript">
                          new TradingView.widget(
                            {
                              "autosize": true,
                              "symbol": "OANDA:SPX500USD",
                              "timezone": "Etc/UTC",
                              "theme": "light",
                              "style": "1",
                              "locale": "en",
                              "toolbar_bg": "#f1f3f6",
                              "enable_publishing": false,
                              "hide_top_toolbar": true,
                              "range": "YTD",
                              "save_image": false,
                              "container_id": "tradingview_9922e"
                            }
                          );
                        </script>
  `
};


// async function destroy() {
//     const hyperlinks = document.querySelectorAll("a")
//     for (const hyperlink of hyperlinks) {
//         console.log(hyperlink)
//         hyperlink.addEventListener("click",(event) => {
//             return
//         })
//     }
//     const resp = await fetch("/checkKeepLogin");
//     if(resp.status === 200) {
//         location.replace("/")
//     }
// }
// window.onbeforeunload = async function () {
//     destroy()
//   };