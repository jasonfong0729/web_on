window.onload = async () => {
  await getUserDetail()
  await updateUserDetail()
}


const formContent = Object.freeze({
  firstName: "firstname",
  lastName: "lastname",
  title: "title",
  email: "email",
  password: "password",
  aboutMe: "aboutme"
})

function validateEmail(mail) {
  let adjEmail;
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail) || mail == null) {
    return mail
  } else {
    adjEmail = prompt("Please enter a valid email address!", mail)
    return validateEmail(adjEmail)
  }
}

function validateLength(type, info, min, max) {
  let adjInfo;
  if (info.length <= max && info.length >= min) {
    return info
  } else {
    adjInfo = prompt(`Please re-enter your ${type} (Must be ${min}-${max} characters long.) `, info);
    console.log('Orignal: ' + info)
    console.log('Adj: ' + adjInfo)
    return validateLength(type, adjInfo, min, max)
  }
}

async function updateUserDetail() {
  const form = document.querySelector("#info")
  const save = document.querySelector("#save")
  save.addEventListener("click", async (event) => {
    const firstName = validateLength("firstName", form[formContent.firstName].value.trim(), 1, 20)
    const lastName = validateLength("lastName", form[formContent.lastName].value.trim(), 1, 20)
    const title = validateLength("title", form[formContent.title].value.trim(), 1, 20)
    const email = validateEmail(form[formContent.email].value.trim())
    let aboutMe = form[formContent.aboutMe].value
    if (!aboutMe) { aboutMe = "I am using Tecky stock,Happy!!!!!!" }
    const file = document.getElementById("profile-upload").files[0]
    console.log(file)
    const formData = new FormData();

    formData.append("firstName", firstName)
    formData.append("lastName", lastName)
    formData.append("title", title)
    formData.append("email", email)
    formData.append("description", aboutMe)
    if (file) { formData.append("profile_image", file) }
    console.log(formData)

    const resp = await fetch("/profile/updateProfile", {
      method: "POST",
      body: formData,
    })

    if (resp.status == 200) {
      getUserDetail()
    }
  })
}

// async function getUserTransaction() {
//   const transactionResp = await fetch("/profile/transaction");
//   const transactionRecord = await transactionresp.json()
//   // const {shot} = transactionRecord
// }

async function getUserDetail() {
  const resp = await fetch("/profile")
  const userInfo = await resp.json()
  const { first_name, last_name, email, profile_image, description, title } = userInfo
  const transactionResp = await fetch("/profile/transaction");
  const transactionRecord = await transactionResp.json()
  console.log(transactionRecord)
  const {count} = transactionRecord
  console.log(count)
  const form = document.querySelector("#info")
  form[formContent.firstName].value = first_name
  form[formContent.lastName].value = last_name
  form[formContent.title].value = title
  form[formContent.email].value = email
  form[formContent.aboutMe].value = description

  const profile = document.querySelector("#profile")
  profile.innerHTML = /*html*/`
    <div class="avator text-center mt-lg-5 mt-2">
                      <img height="128px" width="128px" style="border-radius: 50%" src="images/users/${profile_image}" alt="">
                    </div>
                    <h3 class="card-title mt-3" id="profile-name"></h3>
                    <h5 class="card-subtitle mt-2" id="profile-title"></h5>
                    <div class="row row-cols-3 mt-5">
                      <div class="col-4">
                        <h6>${count}</h6>
                        <p>Shots</p>
                      </div>
                      <div class="col-4 border-end border-start">
                        <h6>-</h6>
                        <p>Followers</p>
                      </div>
                      <div class="col-4">
                        <h6>-</h6>
                        <p>Follow</p>
                      </div>
                    </div>
                    <div class="upload-avatar mt-3">
                      <div class="file btn btn-success">Upload new avatar
                        <input type="file" accept="image/*" name="upload-avatar" id="profile-upload">
                      </div>
                    </div>
                    <div class="aboutme pb-5">
                      <p class="blockquote-footer mt-4">
                        ${description}
                      </p>
                    </div>
    `
  document.getElementById("profile-name").innerText = first_name + " " + last_name
  document.getElementById("profile-title").innerText = title
  const iconDiv = document.getElementById("user-dropdown_div")
  iconDiv.innerHTML =/*html*/`<a href="#" class="nav-link dropdown-toggle user-dropdown" role="button" id="user-dropdown"
            data-bs-toggle="dropdown" aria-expanded="false">
            <img style="border-radius: 50%" src="images/users/${profile_image}" width="32" height="32" alt="">
            </a>
            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="user-dropdown">
            <li><a href="/profile.html" class="dropdown-item">Profile</a></li>
            <li><a href="/wallet.html" class="dropdown-item">Wallet</a></li>
            <li><a href="/user/logout" class="dropdown-item">Sign out</a></li>
            </ul>`

}

