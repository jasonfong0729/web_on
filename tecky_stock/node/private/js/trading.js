function ranPrice() {
  return Math.floor(Math.random() * (1000 - 10) + 10);
}

function genStockData(stock,nData) {
  let data = [];
  let labels = [];
  for (let i = 1; i <= nData; i++) {
    data.push(ranPrice());
    labels.push(i);
  }
  const stockData = {
    labels: labels,
    datasets: [
      {
        label: stock,
        data: data,
        fill: true,
        borderColor: "rgb(75,192,192)",
      },
    ],
  };

  return stockData;
}

const miniChart_config = {
  responsive: false,
  mintainAspectRatio: false,
  elements: {
    point: {
      radius: 0,
    },
  },
  plugins: {
    title: {
      display: false,
    },
    legend: {
      display: false,
    },
    tooltips: {
      enabled: false,
    },
  },
  scales: {
    y: {
      display: false,
      ticks: {
        display: false,
      },
    },
    x: {
      display: false,
      ticks: {
        display: false,
      },
    },
  },
};

function draw_miniStockChart() {
  const ctx_mini_stockCharts = document.querySelectorAll(".stock-list");
  for (const stock of ctx_mini_stockCharts) {
    const miniChartctx = stock.querySelector("#mini-stockchart");
    const miniChart = new Chart(miniChartctx, {
      type: "line",
      data: genStockData("TWTR",7),
      options: miniChart_config,
    });
  }
}
const ctx = document.getElementById("trading-chart");


const tradingChart = new Chart(ctx, {
  type: "line",
  data: genStockData("SPX",10),
});

draw_miniStockChart();


