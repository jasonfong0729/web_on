const stockIdList = []
const stockCodeList = []
const stockPriceList = []
window.onload = async function () {
  addValueToNumber()

  const result = await getData()
  for (stock of result) {
    stockIdList.push(stock.stock_id);
    stockCodeList.push(stock.stock_code)
    stockPriceList.push(stock.close)
  }
  autocomplete(document.getElementById("myInput"), stockCodeList);
  
  await buy()
  await sell()
  await session()
}
async function getData() {
  const resp = await fetch("/trading/get")
  const result = await resp.json();
  return result
}

function addValueToNumber() {
  const e = document.querySelectorAll(".value")
  for (let i = 0; i < e.length; i++) {
    e[i].addEventListener("click", (event) => {
      event.preventDefault()
      number = parseInt(event.target.innerText)
      const input = document.querySelector("#numberOfStockInput")
      if (!input.value) { input.value = 0 }
      let numberOfStock = parseInt(input.value) + number
      input.value = numberOfStock
    });
  }
}
async function buy() {
  const tradings = document.querySelector("#buy");
  tradings.addEventListener("click", async (event) => {
    event.preventDefault();
    const numberOfStock = parseInt(document.querySelector("#numberOfStockInput").value)
    const object = {
      numberOfStock: numberOfStock
    }
    const resp = await fetch("/trading/buying", {
      method: "POST",
      headers: { "content-type": "application/json" },
      body: JSON.stringify(object)
    })
    let message;
    switch (resp.status) {
      case 200: message = "BUYING SUCCESS"; break;
      case 400: message = "INPUT ERROR"; break;
      case 401: message = "[TRADING FALSE]PRICE UPDATE & HIGHER THAN YOU SAW"; break;
      case 402: message = "[TRADING FALSE]USER CASH NOT ENOUGH"; break;
      case 500: message = 'INTERNAL SERVER ERROR'; break;
    }
    document.querySelector("#numberOfStockInput").value = 0;
    alert(message)
  })

}

async function sell() {
  const tradings = document.querySelector("#sell");
  tradings.addEventListener("click", async (event) => {
    event.preventDefault();
    const numberOfStock = parseInt(document.querySelector("#numberOfStockInput").value)
    const object = {
      numberOfStock: numberOfStock
    }
    const resp = await fetch("/trading/selling", {
      method: "POST",
      headers: { "content-type": "application/json" },
      body: JSON.stringify(object)
    })
    let message;
    switch (resp.status) {
      case 200: message = "SELLING SUCCESS"; break;
      case 400: message = "INPUT ERROR"; break;
      case 402: message = "[TRADING FALSE]PRICE UPDATE & LOWER THAN YOU SAW"; break;
      case 401: message = "[TRADING FALSE]NOT ENOUGH STOCK TO SELL"; break;
      case 500: message = 'INTERNAL SERVER ERROR'; break;
    }
    document.querySelector("#numberOfStockInput").value = 0;
    alert(message)
  })

}



async function session() {
  document.querySelector("#stockadd_btn").addEventListener("click", async (event) => {
    event.preventDefault();
    const input = document.querySelector("#myInput").value
    const object={};
    for (let i in stockCodeList) {
      if (stockCodeList[i] === input) {
        object['stockId']=stockIdList[i],
        object['stockPrice']=stockPriceList[i]
      }}
      console.log(object.stockId,object.stockPrice)
      const resp=await fetch("/trading/search", {
        method: "PUT",
        headers: { "content-type": "application/json" },
        body: JSON.stringify(object)
      })
      console.log(resp.status)
      if(resp.status===300){
          location.replace(`/tradePage/trading${object.stockId}.html`)
      }
      
    // const stock = document.getElementById("myInput").value;
    // const watchlist = document.getElementById("watchlist");
    // console.log(stock)
    // watchlist.innerHTML = watchlist.innerHTML + /*html*/`
    // <div class="stock-list wrapper d-flex align-items-center justify-content-between py-2 border-bottom">
    //   <div class="d-flex">
    //    <div class="stock-name me-3">${stock}</div>
    //    <canvas id="mini-stockchart" class="mini-stockchart"></canvas>
    //   </div>
    //   <div class="stock-price">$1,200</div>
    // </div>
    // `
  
    // draw_miniStockChart(); 
  })
}
