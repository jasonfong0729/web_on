window.onload = async () => {
  await loadChart()
  await getUserProfile()
  deposit()
  withdraw()
}
async function loadChart() {

  const cashProfile = await getCash()
  const stockProfile = await getStock()
  const cash = parseFloat(cashProfile.cash_amount)

  const labelList = ["cash"]
  for (const stock of stockProfile) {
    labelList.push(stock.Code)
  }

  const labelValue = [cash]
  for (const stock of stockProfile) {
    labelValue.push(stock.amount * stock.price)
  }

  const colorList = [
    chartColors.orange,
    chartColors.yellow,
    chartColors.green,
    chartColors.blue,
    chartColors.cyan,
    chartColors.black
  ]
  const labelColor = [chartColors.red]
  for (let i = 0; i < stockProfile.length; i++) {
    labelColor.push(colorList[i])
  }

  const ctx = document.getElementById("wallet-portfolio-chart");

  const portfolioData = {
    labels: labelList,
    datasets: [
      {
        label: "Your Portfolio",
        data: labelValue,
        backgroundColor: labelColor,
        hoverOffset: 4,
      },
    ],
  };

  function getaspectRatio() {
    const screen_size = window.screen.width;
    switch (true) {
      case screen_size > 1400:
        return 1.8;
        break;
      case screen_size > 1200:
        return 1.7;
        break;
      case screen_size > 992:
        return 1.5;
        break;
      case screen_size > 768:
        return 1.3;
        break;
      case screen_size > 576:
        return 1.7;
        break;

      default:
        return 1.0;
        break;
    }
  }

  const portfolio_chart = new Chart(ctx, {
    type: "doughnut",
    options: {
      aspectRatio: getaspectRatio(),
      layout: {
        padding: {
          left: 30,
          right: 40,
          top: 0,
          bottom: 0,
        },
      },
      responsive: false,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          position: "right",
          labels: {
            boxWidth: 50,
            boxHeight: 20,
            padding: 20,
            font: {
              size: 14,
            },
          },
        },
      },
    },
    data: portfolioData,
  });
  
  const ctx_asset = document.getElementById("wallet-asset-chart");

  const assetData = {
    labels: ["Jun", "Jul", "Aug", "Oct", "Nov", "Dec"],
    datasets: [
      {
        label: "Your Assets Grow",
        data: [65, 88, 91, 125, 256, 453],
        fill: false,
        tensions: 0.1,
      },
    ],
  };

  const asset_chart = new Chart(ctx_asset, {
    type: "line",
    data: assetData,
    options: {
      maintainAspectRatio: false,
    },
  });

  const ctx_performance = document.getElementById("wallet-performance-chart");

  const performanceData = {
    labels: ["Jun", "Jul", "Aug", "Oct", "Nov", "Dec"],
    datasets: [
      {
        label: "Your Performance",
        data: [65, 88, 191, 125, 356, 353],
        fill: true,
        tensions: 0.1,
      },
    ],
  };

  const performance_chart = new Chart(ctx_performance, {
    type: "line",
    data: performanceData,
    options: {
      maintainAspectRatio: false,
    },
  });

}


async function deposit() {
  const button = document.querySelector("#deposit");
  button.addEventListener("click", async (event) => {
    let amount = prompt(`Please enter your desired amount.)`, 10000);
    amount = combined("amount", amount)
    // console.log(amount)
    if (!amount) { return };
    const choice = confirm(`Are you sure you want to deposit USD$${amount}. `)
    if (!choice) { return };
    console.log("success")
    console.log(amount)
    const resp = await fetch('/wallet/deposit', {
      method: "POST",
      headers: { "content-type": "application/json" },
      body: JSON.stringify({ amount: amount })
    })

    if (resp.status === 200) {
      // socket.emit("new register",username)
      alert(`Deposit successful`)
      location.reload()
    }
  })


}

async function withdraw() {
  const button = document.querySelector("#withdraw");
  button.addEventListener("click", async (event) => {
    let amount = prompt(`Please enter your desired amount.)`, 10000);
    amount = combined("amount", amount)
    
    if (!amount) { return };
    const choice = confirm(`Are you sure you want to withdraw USD$${amount}. `)
    if (!choice) { return };
    console.log("success")

    const resp = await fetch('/wallet/withdraw', {
      method: "POST",
      headers: { "content-type": "application/json" },
      body: JSON.stringify({ amount: amount })
    })

    if (resp.status === 200) {
      // socket.emit("new register",username)
      alert(`Withdraw successful`)
      location.reload()
    }
    if (resp.status === 401) {
      // socket.emit("new register",username)
      alert(`Withdraw unsuccessful. Sorry you don't have enough money`)
    }
    if (resp.status === 400) {
      // socket.emit("new register",username)
      alert(`Input error. Please try again`)
    }
  })


}

function combined(type, info) {
  let adjValue;
  const value = parseFloat(info)
  if ((typeof value === "number" && !Object.is(value, NaN))
    && (Math.floor(value) === value || value.toString().split(".")[1] == undefined)
    || info === null) {
    return value
  } else if ((typeof value === "number" && !Object.is(value, NaN))
    && value.toString().split(".")[1].length <= 2) {
    return value
  } else if (
    (typeof value === "number" && !Object.is(value, NaN))
    && value <= 1000000000 && value > 0) {
    return value
  } else {
    adjValue = prompt(`Please re-enter your ${type} (Must be number in 2 decimal place below 1 billion. `, info);
    return combined(type, adjValue)
  }
}


async function getCash() {
  const cashResp = await fetch("/wallet/cash")
  return await cashResp.json()
}

async function getStock() {
  const stockResp = await fetch("/wallet/stock")
  return await stockResp.json()
}

async function getUserProfile() {
  const cashProfile = await getCash()
  
  const stockProfile = await getStock()
  
  const balanceDiv = document.getElementById("account_balance")
  

  const cashBalance = parseFloat(cashProfile.cash_amount)
  let stockBalance = 0
  for (const stock of stockProfile) {
    stockBalance += stock.amount * stock.price
  }
  //learn about it
  const total = (cashBalance + stockBalance).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")

  balanceDiv.innerText = ` $ ${total}`
}



