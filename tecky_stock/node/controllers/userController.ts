import { UserService } from "../services/userService";
import { Request, Response } from 'express';
import fetch from 'node-fetch'

export class UserController {

    constructor(private userService: UserService,) { }

    login = async (req: Request, res: Response) => {
        try {
            console.log(`[INFO]     FROM User Controller(login)--------------------------------`)
            const { username, password, keepLogin } = req.body
            console.log(keepLogin)
            const result = await this.userService.login(username, password, keepLogin)

            console.log(`[INFO]     FROM User Controller(login)--------------------------------`)
            if (!result) {
                res.status(401).json({ message: "Incorrect Username or Password" });
                return;
            }
            if (req.session) {
                req.session["user"] = {
                    id: result.id,
                    firstName: result.first_name,
                    lastName: result.last_name,
                    username: result.username
                };
            }
            console.log(`[INFO]     req.session:`)
            console.log(req.session['user'])
            res.json({ message: "success" });
        } catch (error) {
            console.error(error.message);
            res.status(500).json({ message: "internal server error" });
        }
    }

    loginGoogle = async (req: Request, res: Response) => {
        try {
            console.log(`[INFO]     FROM User Controller(loginGoogle)--------------------------------`)
            const accessToken = req.session?.['grant'].response.access_token;
            const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
                method: "get",
                headers: {
                    "Authorization": `Bearer ${accessToken}`
                }
            });

            const googleUser = await fetchRes.json();

            const result = await this.userService.loginGoogle(googleUser)
            if (!result) {
                return res.status(401).redirect('/login.html?error=Incorrect+Username')
            }
            console.log(`[INFO]     FROM User Controller(loginGoogle)--------------------------------`)
            console.log(`[INFO]     Found User Detail`)

            if (req.session) {
                req.session["user"] = {
                    id: result.id,
                    firstName: result.first_name,
                    lastName: result.last_name,
                    username: result.username
                };
            }
            console.log(`[INFO]     req.session[user]: ${req.session['user']}`)
            res.redirect("/")
        } catch (error) {
            console.error(error.message);
            res.status(500).json({ message: "internal server error" });
        }
    }

    logout = (req: Request, res: Response) => {
        try {
            if (req.session['user']) {
                const id = req.session['user']["id"]
                console.log(`[INFO]     FROM User Controller(logout)--------------------------------`)
                console.log('[INFO]     logout id: ' + id)
                delete req.session['user'];
            }
            res.redirect("/");
        } catch (error) {
            console.error(error.message);
            res.status(500).json({ message: "internal server error" });
        }

    }
        // keepLogin = async (req: Request, res: Response) => {
    //     // try {
    //         console.log(`[INFO]     FROM User Controller(keepLogin)--------------------------------`)
    //         if(!req.session["user"]){
    //             res.status(200).send('ok');
    //             return
    //         }
    //         const result = await this.userService.keepLogin(req.session["user"].id)
    //         if (!result) {
    //             delete req.session["user"]
    //             console.log(`[INFO]     Session DELETED`)
    //             res.status(200).send('ok');
    //             return
    //         }
    //         console.log(`[INFO]     Session Kept`)
    //         res.status(200).send('ok');
    //     // } catch (error) {
    //     //     console.error(error.message);
    //     //     res.status(500).json({ message: "internal server error" });
    //     // }
    // }
}