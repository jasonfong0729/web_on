import { SignupService } from "../services/signUpService";
import { Request, Response } from "express";

export class SignupController {
    // @ts-ignore
    constructor(private signupService: SignupService) { }

    createNewUser = async (req: Request, res: Response) => {
        try {
            console.log(`[INFO]     FROM Sign Up Controller--------------------------------`)
            const { firstName, lastName, username, password, email } = req.body
            
            const result = await this.signupService.createNewUser(firstName, lastName, username, password, email)

            if(result == "username"){
                res.status(401).json({message:"Sorry, name has been used already."})
                return
            }

            if (result == "email") {
                res.status(402).json({ message:"Sorry, email has been used already." });
                return;
            }
            res.json({ message: "success" });

        } catch (error) {
            console.error(error.message);
            res.status(500).json({ message: "internal server error" });
        }
    }

}