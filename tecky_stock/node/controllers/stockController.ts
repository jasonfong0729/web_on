import { StockService } from "../services/stockService";
import { Request, Response } from "express";
// import path from "path";
export class StockController {
    // @ts-ignore
    constructor(private sS: StockService) {
        // default
    }

    getAllStock = async (req: Request, res: Response) => {
        try {
            if (!req.session['stock']) {
                const stockPrice = await this.sS.getSingleStocksLatestPrice(2);
                req.session['stock'] = { id: 2, price: stockPrice.close }}
            console.log("start get stock data")
            const stock_list = await this.sS.getAllStocksWithLatestPrice();
            res.json(stock_list);
        } catch (err) {
            console.error("[ERROR]IN GETTING STOCK LIST");
            console.error(err);
            res.status(500).json({ message: 'INTERNAL SERVER ERROR' });
        }
    }

    getMarket = async (req: Request, res: Response) => {
        try {
            const marketTodayList = await this.sS.getStockMinsPrice(101);
            const marketYearList = await this.sS.getStockDailyPrice(101);
            const market = {
                today: marketTodayList,
                year: marketYearList
            }
            res.json(market);
        } catch (err) {
            console.error("[ERROR] IN GETTING MARKET DATA")
            console.error(err);
            res.status(500).json({ message: 'INTERNAL SERVER ERROR' })
        }
    }


    getSingleStockData = async (req: Request, res: Response) => {
        try {
            let stockId=req.session['stock'].id
            const marketTodayList = await this.sS.getStockMinsPrice(stockId);
            const marketYearList = await this.sS.getStockDailyPrice(stockId);
            //@ts-ignore 
            const market = {
                today: marketTodayList,
                year: marketYearList
            }
            // res.json(market);
            res.status(200).redirect("./trading_testing.html")
        } catch (err) {
            console.error("[ERROR] IN GETTING STOCK DATA")
            console.error(err);
            res.status(500).json({ message: 'INTERNAL SERVER ERROR' })


        }
    }

    stockBuy = async (req: Request, res: Response) => {
        try {
            const {numberOfStock} = req.body
            const numberOfStock1=parseInt(numberOfStock)
            const stockId = req.session['stock'].id
            const price = req.session['stock'].price
            const username = req.session["user"].username
            const userId = req.session['user'].id
            const action = "buy"

            if (numberOfStock == NaN || numberOfStock <= 0 || !Number.isInteger(numberOfStock1)) {
                res.status(400).json({ message: "Input error" })
                console.log(`[TRADING FALSE]INPUT ERROR`)
                return
            }
            console.log(`[INFO] ${username} start buying`)
            const nowPrice = (await this.sS.getSingleStocksLatestPrice(stockId)).close
            const cashAmount = (await this.sS.getCashAmount(userId))['cash_amount']
            let totalTradeAmount = numberOfStock * price;
            let tradingPrice = price;
            if (price !== nowPrice && nowPrice > price) {
                res.status(401).json({ message: "[TRADING FALSE]PRICE UPDATE & HIGHER THAN YOU SAW" })
                console.log(`[TRADING FALSE]PRICE NOT MATCH`)
                return
            } else if (price > nowPrice) {
                totalTradeAmount = numberOfStock * nowPrice;
                tradingPrice = nowPrice;
            }
            if (totalTradeAmount > cashAmount) {
                res.status(402).json({ message: "[TRADING FALSE]USER CASH NOT ENOUGH" })
                console.log(`[TRADING FALSE]USER CASH ENOUGH"`)
                return
            }
            await this.sS.createTransactions(stockId, userId, action, numberOfStock, tradingPrice)
            console.log(`[INFO] ${username} BUYING STOCK SUCCESS`)
            res.status(200).json({ message:"buying success"})
        } catch (err) {
            console.error("[ERROR] SYSTEM ERROR ON TRADING")
            console.error(err);
            res.status(500).json({ message: 'INTERNAL SERVER ERROR' })
        }
    }
    stockSell = async (req: Request, res: Response) => {
        try {
            const {numberOfStock} = req.body
            const numberOfStock1=parseInt(numberOfStock)
            const stockId = req.session['stock'].id
            const price = req.session['stock'].price
            const username = req.session["user"].username
            const userId = req.session['user'].id
            const action = "sell"
            if (numberOfStock1 == NaN || numberOfStock1 <= 0 || !Number.isInteger(numberOfStock1)) {
                res.status(400).json({ message: "INPUT ERROR" })
                console.log(`[TRADING FALSE]INPUT ERROR`)
                return
            }
            console.log(`[INFO] ${username} start selling`)
            const nowPrice = (await this.sS.getSingleStocksLatestPrice(stockId)).close
            const userStockAmount = (await this.sS.getUserStockAmount(userId, stockId))['amount']
            if(userStockAmount<numberOfStock){
                res.status(401).json({ message: "NOT ENOUGH STOCK ON USER ACCOUNT" })
                console.log(`[TRADING FALSE]NOT ENOUGH STOCK ON USER ACCOUNT`)
                return
            }
            let tradingPrice = price;
            if (price !== nowPrice && nowPrice < price) {
                res.status(402).json({ message: "[TRADING FALSE]PRICE UPDATE & LOWER THAN YOU SAW" })
                console.log(`[TRADING FALSE]PRICE UPDATE & LOWER THAN YOU SAW`)
                return
            } else if (nowPrice > price) {
                tradingPrice = nowPrice;
            }
            await this.sS.createTransactions(stockId, userId, action, numberOfStock, tradingPrice)
            console.log(`[INFO] ${username} SELLING STOCK SUCCESS`)
            res.status(200).json({ message:"selling success"})
        } catch (err) {
            console.error("[ERROR] SYSTEM ERROR ON TRADING")
            console.error(err);
            res.status(500).json({ message: 'INTERNAL SERVER ERROR' })
        }

    }

    changeSession=(req: Request, res: Response)=>{
        try{
        const {stockId,stockPrice}=req.body
        req.session['stock']={
            id :stockId,
            price:stockPrice
        }
        console.log("[INFO] change info success")
        console.log(req.session['stock'].id,req.session['stock'].price)
    //    const  url=path.join(__dirname,`../private/trading${stockId}.html`)
    //    console.log(url)
        res.status(300).json({message:"success"});
    } catch(err){
        console.log("[ERROR] session change error")
        console.log(err)
        res.status(500).json({message:"INTERNAL SERVER ERROR"})
    }
}
}

