import { ProfileService } from "../services/profileService";
import { Request, Response } from "express";


export class ProfileController {
    // @ts-ignore
    constructor(private profileService: ProfileService) { }

    getProfile = async (req: Request, res: Response) => {
        try {
            console.log(`[INFO]     FROM Profile Controller(getProfile)------------------------------------`)
            const { id } = req.session["user"]

            const result = await this.profileService.getProfile(id)

            if (!result) {
                res.status(401).json({ message: "Sorry, no such user" })
                return
            }

            res.status(200).json(result);

        } catch (error) {
            console.error(error.message);
            res.status(500).json({ message: "internal server error" });
        }
    }
    getTransaction =async (req: Request, res: Response) => {
        try{
            const { id } = req.session["user"]
            const result = await this.profileService.getTransaction(id)
            if (!result) {
                res.status(401).json({ message: "Sorry, no such user" })
                return
            }

            res.status(200).json(result);


        }catch (error) {
            console.error(error.message);
            res.status(500).json({ message: "internal server error" });
        }
    }
    updateProfile = async (req: Request, res: Response) => {
        try {
            console.log(`[INFO]     FROM Profile Controller (updateUser)--------------------------------`)
            const { firstName, lastName, email, title, description } = req.body
            const { id } = req.session["user"]
            const profile_image = req.file?.filename
            const result = await this.profileService.updateProfile(id, firstName, lastName, description, email,profile_image, title,)

            if (!result) {
                res.status(401).json({ message: "Sorry, please input correct information" })
                return
            }
            res.json({ message: "success" });

        } catch (error) {
            console.error(error.message);
            res.status(500).json({ message: "internal server error" });
        }
    }

}