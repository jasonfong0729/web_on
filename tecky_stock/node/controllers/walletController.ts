import { WalletService } from "../services/walletService";
import { Request, Response } from "express";

export class WalletController {
    // @ts-ignore
    constructor(private walletService: WalletService) {
        // default
    }

    userStockWithLatestPrice = async (req: Request, res: Response) => {
        try {
            console.log(`[INFO]     FROM Wallet Controller(userStockWithLatestPrice)------------------------------------`)
            if (req.session['user']) {
                const { id } = req.session['user']
                
                const userStocksWithLastPrice = await this.walletService.userStockWithLatestPrice(id);
                res.status(200).json(userStocksWithLastPrice)
            }
        } catch (err) {
            console.error("[ERROR] IN GETTING STOCK DATA")
            console.error(err);
            res.status(500).json({ message: 'INTERNAL SERVER ERROR' })
        }
    }

    userCashBalance = async (req: Request, res: Response) => {
        try {
            console.log(`[INFO]     FROM Wallet Controller(userCashBalance)------------------------------------`)
            if (req.session['user']) {
                const { id } = req.session['user']
                
                const userCash = await this.walletService.getCashAmount(id);
                res.status(200).json(userCash)
            }
        } catch (err) {
            console.error("[ERROR] IN GETTING STOCK DATA")
            console.error(err);
            res.status(500).json({ message: 'INTERNAL SERVER ERROR' })
        }
    }

    deposit = async (req: Request, res: Response) => {
        try {
            console.log(`[INFO]     FROM Wallet Controller(deposit)------------------------------------`)
            let { amount } = req.body
            console.log(typeof amount)
            amount = parseFloat(amount)

            const userId = req.session['user'].id
            // console.log(userId, typeof (userId))
            const action = "deposit"
            // console.log(action, typeof (action))

            if (Object.is(amount,NaN) || amount < 0 || !(typeof amount === "number")) {
                res.status(400).json({ message: "Input error" })
                console.log(`Input error"`)
                return
            }
            console.log(`[INFO]     User ${userId} Start Deposit`)
            const result = await this.walletService.createTransactions(userId, action, amount)

            console.log(`[INFO]     FROM Wallet Controller(deposit)------------------------------------`)
            if (result) {
                res.status(200).json({ message: "success" })
                console.log(`[INFO]     Deposit SUCCESS`)
            } else {
                console.log(`[INFO]     Deposit FAIL`)
                res.status(400).json({ message: "Input error" })
            };
        } catch (err) {
            console.error("[ERROR] SYSTEM ERROR ON Deposit")
            console.error(err);
            res.status(500).json({ message: 'INTERNAL SERVER ERROR' })
        }
    }
    withdraw = async (req: Request, res: Response) => {
        try {
            console.log(`[INFO]     FROM Wallet Controller(withdraw)------------------------------------`)
            let { amount } = req.body
            amount = parseFloat(amount)

            const userId = req.session['user'].id
            const action = "withdraw"
            if (Object.is(amount,NaN) || amount < 0 || !(typeof amount === "number")) {
                res.status(400).json({ message: "Input error" })
                return
            }
            console.log(`[INFO]     userID ${userId} start withdraw`)
            const {cash_amount}:any = await this.walletService.getCashAmount(userId)
            console.log('[INFO]     userID'+ userId +cash_amount)
            console.log(`[INFO]     FROM Wallet Controller(withdraw)------------------------------------`)
            if (cash_amount < amount) {
                res.status(401).json({ message: "NOT ENOUGH CASH ON USER ACCOUNT" })
                return
            }
            const result = await this.walletService.createTransactions(userId, action, amount)

            console.log(`[INFO]     FROM Wallet Controller(withdraw)------------------------------------`)
            if (result) {
                res.status(200).json({ message: "success" })
                console.log(`[INFO]     Withdraw SUCCESS`)
            } else {
                console.log(`[INFO]     Withdraw FAIL`)
                res.status(400).json({ message: "Input error" })
            };
        } catch (err) {
            console.error("[ERROR] SYSTEM ERROR ON TRADING")
            console.error(err);
            res.status(500).json({ message: 'INTERNAL SERVER ERROR' })
        }

    }
}

