// @ts-ignore
import { hashPassword } from "./hash";
// @ts-ignore
import { Knex } from "knex";
// @ts-ignore
import { tables,USERData,WALLETData,SEARCHData,WATCH_LISTData,STOCKinNYSEData } from "./models"

import xlsx from "xlsx";
// @ts-ignore
import { readJsonFilePromise } from "./jsonfile"
// @ts-ignore
import { readCSVPromise } from "./csvfile"
// @ts-ignore
import path from "path"

const workbook = xlsx.readFile(path.join(__dirname, "..", "database","users.xlsx"));
// const users = xlsx.utils.sheet_to_json<USERData>(workbook.Sheets["user"]);
const wallets = xlsx.utils.sheet_to_json<WALLETData>(workbook.Sheets["transaction"]);
// const searchHistory = xlsx.utils.sheet_to_json<SEARCHData>(workbook.Sheets["file"]);
// const watchLists = xlsx.utils.sheet_to_json<WATCH_LISTData>(workbook.Sheets["category"]);

console.log(wallets)