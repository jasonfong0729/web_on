import fs, { Stats,readdirSync, statSync } from 'fs'

import path from 'path'

function FSStatPromise(address: string): Promise<Stats> {
    return new Promise<Stats>(function (resolve, reject) {
        fs.stat(address, (err, stat: Stats) => {
            if (err) {
                reject(err)
                return
            }
            resolve(stat)
        })
    })
}

function FSReaddir /*< T extends string[]|Buffer[]|fs.Dirent[]>*/(address: string): Promise<string[]> {
    return new Promise<string[]>(function (resolve, reject) {
        fs.readdir(address, (err, t: string[]) => {
            if (err) {
                reject(err)
                return
            }
            resolve(t)
        })
    })
}

// function FSReaddir < T extends string[]|Buffer[]|fs.Dirent[]> (address:string): Promise <T> {
//     return new Promise<T>(function (resolve,reject){
//         fs.readdir(address,(err,t:T) => {
//            if (err){
//                reject(err)
//                return
//            } 
//            resolve(t)
//         })
//     }) 
// }


/*Only string version*/

// async function listAllJsRecursive(address: string) {
//     try {
//         const files = await FSReaddir(address)
//         for (let file of files) {
//             const fileDetail = await FSStatPromise(path.join(address, file))
//             if (fileDetail.isFile() && path.extname(file) == '.js') {
//                 console.log(path.join(address, file))
//             }
//             if (fileDetail.isDirectory() && file != 'node_modules') {
//                 listAllJsRecursive(path.join(address, file))
//             }
//         }
//     } catch (err) {
//         console.log('error!!!')
//     }
// }
// listAllJsRecursive('/Users/jason/tecky')

export function listAllHTMLRecursive(address: string,lists:string[]) {
        const files = readdirSync(address)
        for (let file of files) {
            const fileDetail =  statSync(path.join(address, file))
            // console.log(file)
            if (fileDetail.isFile() && path.extname(file) == '.html') {
                const filename = file.replace(".html","")
                lists.push(filename)
                
            }
            else if (fileDetail.isDirectory() && file != 'node_modules') {
                listAllHTMLRecursive(path.join(address, file),lists)
            }
        }
        return lists
}


export async function ListAllHTMLRecursivePromise(address: string) {
    try {
        const files = await FSReaddir(address)
        const list:string[] =[]
        for (let file of files) {
            const fileDetail = await FSStatPromise(path.join(address, file))   
            if (fileDetail.isFile() && path.extname(file) == '.html') {
                // console.log(file)
                list.push(file)
                
            }
            else if (fileDetail.isDirectory() && file != 'node_modules') {
                await ListAllHTMLRecursivePromise(path.join(address, file))
            }
        }
        return files
    } catch (error) {
        console.error(error.message)
        return
    }
}
// const list:string[] =[]
// const files = listAllHTMLRecursive(path.join(__dirname, "../","private"),list)
// console.log(files)