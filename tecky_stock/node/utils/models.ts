export const tables = Object.freeze({
    USER: "users",
    WALLET: "wallets",
    STOCK: "stocks",
    SEARCH: "search_history",
    WATCH_LIST: "watch_list",
    TRANSACTION: "transactions",
    DAILY: "stock_daily_price",
    mins15: "stock_15mins_price",
    USER_STOCK:"users_stock"
})

export interface USERData {
    firstName: string;
    lastName: string;
    username: string;
    email: string;
    password: string;
}

export interface WALLETData {
    user_id: number
    action: string;
    amount: number;
}

export interface USER_STOCKData {
    user_id: number;
    stock_id: number;
    amount: number;
}

export interface SEARCHData {
    user_id: number;
    stock_id: number;
}

export interface WATCH_LISTData {
    user_id: number;
    stock_id: number;
}

// export interface STOCKinNYSEData {
//     stock_code: string;
//     stock_name:string;
// }

export interface MOSTActiveStock {
    stock_code: string;
    stock_name: string;
    ranking: number;
}

export interface TRANSACTIONData {
    user_id: number;
    stock_id: number;
    action: string;
    num_of_stock: number;
    price_of_stock: number;
    amount: number;
}

export type User = {
    id: number;
    first_name: string;
    last_name: string;
    username: string;
    email: string;
    password: string;
    profile_image: string;
    description: string;
}

export type GoogleUserData = {
    id: number;
    given_name: string;
    family_name: string;
    name:string;
    verified_email: boolean;
    email: string;
    password: string;
    picture: string;
    locale:string;
}

export type GoogleUser = {
    first_name: string;
    last_name: string;
    username: string;
    email: string;
    password: string;
}

export interface UserStockList{ 
    id:number;
    stock_code:string;
    amount: number;
}