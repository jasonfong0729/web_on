import jsonfile from "jsonfile";

export const readJsonFilePromise = <T>(filepath: string) =>
  new Promise<T>((resolve, reject) => {
    jsonfile.readFile(filepath, (err, data) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(data);
    });
  });

// export const writeJsonFilePromise = (filepath: string, data: any) =>
//   new Promise<void>((resolve, reject) => {
//     jsonfile.writeFile(filepath, data, (err) => {
//       if (err) {
//         reject(err);
//         return;
//       }
//       resolve();
//     });
//   });
// await jsonfile.writeFile(...)

export const writeJsonFilePromise = (filepath: string, data: any) =>
  jsonfile.writeFile(filepath, data, { spaces: 4 });

