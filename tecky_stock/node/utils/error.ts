import fs from "fs";
import papa from "papaparse"
import { MOSTActiveStock } from "./models";

const readCSVPromise = (filepath: string) =>
  new Promise<string[][]>((resolve, reject) => {
    const file = fs.createReadStream(filepath);

    papa.parse<string[]>(file, {
      worker: true, // Don't bog down the main thread if its a big file
      complete: function (results, file) {
        // console.log(results.errors);
        resolve(results.data);
      },
      error: function (err) {
        reject(err);
      },
    });
  });



export const readMostActiveStock = async (filepath: string) => {
    const files: string[][] = await readCSVPromise(filepath)

    let i = 1
    let stockWithRanking:MOSTActiveStock[]=[]

    for (const file of files) {
      const stock = {
        stock_code:file[0].trim(),
        stock_name:file[1],
        ranking:i
      }
      stockWithRanking.push(stock);
      i++
    }

    stockWithRanking.sort(function (a, b) {
      if (a.stock_code > b.stock_code) {
        return 1;
      }
      if (b.stock_code > a.stock_code) {
        return -1;
      }
      return 0
    });
    return stockWithRanking
}
