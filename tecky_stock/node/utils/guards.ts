import { Request, Response, NextFunction } from "express";
import path from "path";
import { listAllHTMLRecursive } from "./listAllHtml";
// const privatePaths = "/dashboard_dummy.html"
const paths:string[] = []
const privatePaths= listAllHTMLRecursive(path.join(__dirname,"../","private"),paths)
// const privatePaths = ["dashboard", "wallet", "trading", "dashboard_dummy"]
// const privatePaths = ["dashboard_dummy","trading"]

export const isLoggedInStatic = (req: Request, res: Response, next: NextFunction) => {
  try{
    console.log(`[INFO]     FROM guard(isLoggedInStatic)--------------------------------`)

    // const checkingPathRequired = (requestedPath:string)=>{
    //   for(const privatePath of privatePaths){
  
    //   }
    // }
    // console.log(req.session["user"])
    if (req.session["user"]) {
      console.log('[INFO]     Found user\'s session')
      next();
      return;
    }
  
  
    for (const privatePath of privatePaths) {
      // console.log(("/"+privatePath+".html"))
      if (req.path == privatePath || req.path == ("/"+privatePath+".html")||req.path == ("/tradePage"+"/"+privatePath+".html")) {
        console.log('[INFO]     user is not logged in')
        res.redirect("/");
        return
      }
    }
  
    console.log(`[INFO]     user is not logged in`);
    next();
  }catch(error){
    console.error(error.message)
    res.redirect("/");
  }
};

export const isLoggedInApi = (req: Request, res: Response, next: NextFunction) => {
  if (req.session["user"]) {
    res.redirect("dashboard.html")
    return;
  }
  next();;
};
