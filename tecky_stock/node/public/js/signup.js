window.onload = async () => {
    registerAccount()
}
// const socket = io.connect();

const registerAccount = () => {

    const formData = document.querySelector("#register_form")
    formData.addEventListener("submit", async (event) => {
        event.preventDefault()
        const password = validatePassword("password", formData["password"].value.trim(), 8, 20)
        if (password == "not valid") {
            alert(`Please re-enter your password (Must be 8-20 characters long including at least one uppercase.)`)
            return
        };
        if (!verifyThePassword()) { return };
        const firstName = validateLength("firstName", formData["first-name"].value.trim(), 1, 20)
        const lastName = validateLength("lastName", formData["last-name"].value.trim(), 1, 10)
        const username = validateLength("username", formData["username"].value.trim(), 8, 12)
        

        const email = validateEmail(formData["email"].value.trim())
        if(!email){return}
        
        const formObject = {
            firstName: firstName,
            lastName: lastName,
            username: username,
            password: password,
            email: email,
        }
        const resp = await fetch('/signup', {
            method: "POST",
            headers: { "content-type": "application/json" },
            body: JSON.stringify(formObject)
        })
        if (resp.status === 200) {
            // socket.emit("new register",username)
            alert(`Sign Up successful`)
            location.replace("\index.html")
        }
        if (resp.status === 401) {
            alert(`Sorry, username has been used already.`)
        }
        if (resp.status === 402) {
            alert(`Sorry, email has been used already.`)
        }
        if (resp.status === 500) {
            alert(`Please try again`)
            location.replace("\signup.html")
        }
    })

}
function verifyThePassword() {
    const password = document.getElementById("password").value
    const verification = document.getElementById("password-conform").value
    if (password != verification) {
        alert(`Two entered passwords are not the same.`)
        return false
    }
    return true
}

function validateLength(type, info, min, max) {
    let adjInfo;
    if (info.length <= max && info.length >= min) {
        return info
    } else {
        adjInfo = prompt(`Please re-enter your ${type} (Must be ${min}-${max} characters long.) `, info);
        console.log('Orignal: ' + info)
        console.log('Adj: ' + adjInfo)
        return validateLength(type, adjInfo, min, max)
    }
}

function validateEmail(mail) {
    let adjEmail;
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)||mail == null) {
        return mail
    }else{
    adjEmail = prompt("Please enter a valid email address!", mail)
    return validateEmail(adjEmail)
    }
}

function validatePassword(type, password, min, max) {
    if (password.length <= max && password.length >= min && validateUppercase(password)) {
        return password
    } else return "not valid"
}

function validateUppercase(password) {
    for (let character of password) {
        if (!isNaN(character * 1)) { }
        else {
            if (character == character.toUpperCase()) {
                return true;
            }
        }
    }
    return false
}


