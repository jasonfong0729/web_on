
window.onload = async function () {
    
    loginToChatroom()
  };
  

 function loginToChatroom(){
    const login = document.getElementById("login-form");
    
    login.addEventListener("submit", async (event) => {
        event.preventDefault();
        const {username,password,remember} = event.target;

        const formObject = {
            username:username.value,
            password:password.value,
            keepLogin:remember.checked
        }
        console.log(formObject);
        const resp = await fetch ("/user/login",{
            method: "POST",
            headers: { "content-type": "application/json" },
            body: JSON.stringify(formObject)
        })

        if(resp.status === 200) {
            location.replace("/dashboard.html")
        }
        if(resp.status ===401){
            location.replace("/fail_to_login.html")
        }
        if (resp.status === 500) {
            alert(`Please try again`)
            location.replace("/")
        }
    })

}