Create database stock_game;

Drop table transactions;

Drop table watchlists;

Drop table search_historys;

Drop table stock_15mins_price;

Drop table stock_daily_price;

Drop table stocks;

Drop table wallets;

Drop table users;

Create table "users"(
    "id" serial primary key,
    "first_name" varchar(255) not null,
    "last_name" varchar(255) not null,
    "username" VARCHAR(255) NOT NULL,
    "email" VARCHAR(255) NOT NULL,
    "password" VARCHAR(255) NOT NULL,
    "profile_image" varchar(255) NOT NULL DEFAULT 'default.png',
    "description" VARCHAR(255) DEFAULT 'I am using tecky stcok',
    "keep_login" boolean NOT NULL DEFAULT false,
    "is_online" BOOLEAN NOT NULL DEFAULT false,
    "notification" BOOLEAN NOT NULL DEFAULT true,
    "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

Create table "wallets"(
    "id" serial primary key,
    "user_id" integer not null,
    FOREIGN KEY("user_id") REFERENCES "users"("id"),
    "action" varchar(255) NOT NULL,
    "amount" decimal(19, 2) not null,
    "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

Create table "stocks"(
    "id" serial primary key,
    "stock_code" varchar(255) NOT NULL,
    "stock_name" varchar(255) NOT NULL,
    "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

Create table "stock_daily_price"(
    "id" serial primary key,
    "stock_id" integer NOT NULL,
    FOREIGN KEY("stock_id") REFERENCES "stocks"("id"),
    "open" decimal(9, 2) NOT NULL,
    "close" decimal(9, 2) NOT NULL,
    "high" decimal(9, 2) NOT NULL,
    "low" decimal(9, 2) NOT NULL,
    "date" date NOT NULL,
    "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

Create table "stock_15mins_price"(
    "id" serial primary key,
    "stock_id" integer NOT NULL,
    FOREIGN KEY("stock_id") REFERENCES "stocks"("id"),
    "open" decimal(9, 2) NOT NULL,
    "close" decimal(9, 2) NOT NULL,
    "high" decimal(9, 2) NOT NULL,
    "low" decimal(9, 2) NOT NULL,
    "datetime" timestamp NOT NULL,
    "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

Create table "search_historys"(
    "id" serial primary key,
    "user_id" integer not null,
    FOREIGN KEY("user_id") REFERENCES "users"("id"),
    "stock_id" integer NOT NULL,
    FOREIGN KEY("stock_id") REFERENCES "stocks"("id"),
    "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

Create table "watchlists"(
    "id" serial primary key,
    "user_id" integer not null,
    FOREIGN KEY("user_id") REFERENCES "users"("id"),
    "stock_id" integer NOT NULL,
    FOREIGN KEY("stock_id") REFERENCES "stocks"("id"),
    "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

Create table "transactions"(
    "user_id" integer not null,
    FOREIGN KEY("user_id") REFERENCES "users"("id"),
    "stock_id" integer NOT NULL,
    FOREIGN KEY("stock_id") REFERENCES "stocks"("id"),
    "action" varchar(255) NOT NULL,
    "num_of_stock" integer not null,
    "price_of_stcok" decimal(9, 2) NOT NULL,
    "amount" decimal(19, 2) not null,
    "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

