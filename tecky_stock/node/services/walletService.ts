import { Knex } from "knex";
import { tables, UserStockList } from "../utils/models"




export class WalletService {
    
    constructor(private knex: Knex) { }

    userStockWithLatestPrice = async (userID: number) => {
        console.log(`[INFO]     FROM Wallet Service(userStockWithLatestPrice)------------------------------------`)
        
        const userStockList = await this.getUserStockDetail(userID)

        // console.log(userStockList)
        const stockListWithLastPrice = []
        for (const stock of userStockList) {
            const latestPrice: any = await this.knex(tables.mins15)
                .first("close")
                .whereNot("close", NaN)
                .where("stock_id", stock.id)
                .orderBy("date_time", "desc")
            const object = {
                Code: stock.stock_code,
                amount: stock.amount,
                price: parseFloat(latestPrice.close)
            }
            stockListWithLastPrice.push(object)
        }
        console.log(`[INFO]      UserStockList with Price`)
        console.log(stockListWithLastPrice)
        return stockListWithLastPrice
    }

    createTransactions = async (userId: number, action: string, amount: number) => {
        console.log(`[INFO]     FROM Wallet Service(createTransactions)------------------------------------`)
        
        const { cash_amount }: any = (await this.getCashAmount(userId))
        
        if (action === "deposit") {
            await this.knex(tables.USER).where("id", userId).increment("cash_amount", amount);
        } else if (action === "withdraw" && cash_amount >= amount) {
            await this.knex(tables.USER).decrement("cash_amount", amount).where("id", userId)
        } else if (action === "withdraw" && cash_amount < amount) {
            console.log("[Warning]      Not enough money to withdraw")
            return false
        } else {
            console.log("[Warning]      THERE ARE WRONG ACTION CREATED TRANSACTION PLEASE LOOK IT")
            return false
        }

        await this.knex(tables.WALLET).insert({
            user_id: userId,
            action: action,
            amount: amount
        })
        return true
    }

    getUserTransaction = async (user_id: number) => {
        console.log(`[INFO]     FROM Wallet Service(getUserTransaction)------------------------------------`)
        const transaction = await this.knex(tables.TRANSACTION).select()
            .where("user_id", user_id)
            .whereRaw("created_at >= CURRENT_TIMESTAMP - interval '?days'", [30])
        return transaction
    }

    getCashAmount = async (user_id: number) => {
        console.log(`[INFO]     FROM Wallet Service(getCashAmount)------------------------------------`)
        const cash: object = await this.knex(tables.USER).first("cash_amount").where("id", user_id)
        return cash
    }

    getUserStockDetail = async (userID: number) => {
        console.log(`[INFO]     FROM Wallet Service(getUserStockDetail)------------------------------------`)
        // const sql = /*sql*/`
        // select stocks.id,stocks.stock_code,users_stock.amount
        // from users_stock 
        // left outer join stocks on users_stock.stock_id = stocks.id 
        // where users_stock.user_id = ?`
        // console.log(userID)

        //example of rename
        // const columns = ["stocks.id", {"hihi":"stocks.stock_code"}, "users_stock.amount"]
        const columns = ["stocks.id", "stocks.stock_code", "users_stock.amount"] 
        // const userStockList:UserStockList[] = await this.knex(tables.USER_STOCK).column(columns).select().where("user_id", userID )
        const userStockList:UserStockList[] = await this.knex.column(columns).select().from(tables.USER_STOCK).leftOuterJoin(tables.STOCK,"users_stock.stock_id","stocks.id").where("users_stock.user_id", userID )
        // const userStockList: UserStockList[] = (await this.knex.raw(sql,userID)).rows

        return userStockList
    }
}

