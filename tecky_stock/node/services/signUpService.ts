import { Knex } from "knex";
import { hashPassword } from "../utils/hash"

export class SignupService {
    
    constructor(private knex: Knex) { }
   
    createNewUser = async (firstName: string, lastName: string, username: string, password: string, email: string) => {
        console.log(`[INFO]     From Sign Up Service--------------------------------`)
        const foundUsername = await this.knex("users").select("id").where("username", username).first()
        if(foundUsername){
            console.log(`[INFO]     Fail -- Name: ${username} has been used already by user ${foundUsername.id}`)
            return "username"
        }

        const foundEmail = await this.knex("users").select("id").where("email", email).first()
        if(foundEmail){
            console.log(`[INFO]     Fail -- Name: ${email} has been used already  by user ${foundEmail.id}`)
            return "email"
        }

        const data = {
            first_name: firstName,
            last_name: lastName,
            username: username,
            email: email,
            password: await hashPassword(password)
        }
        const userID:any = (await this.knex("users").returning('id').insert(data))[0]
        console.log(userID)
        console.log(`[INFO]     Success -- User ${userID.id} is created`)
        return userID

    }

}