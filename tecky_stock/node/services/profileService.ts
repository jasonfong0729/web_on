import { Knex } from "knex";

export class ProfileService {

    constructor(private knex: Knex) { }

    getProfile = async (id: number) => {
        console.log(`[INFO]     From Profile Service(getProfile)--------------------------------`)
        const information = ["id", "first_name", "last_name", "username", "email", "profile_image", "description","title"]
        const foundUser = await this.knex("users").column(information).select().where("id", id).first()
        return foundUser
    }
    getTransaction =async (id: number) => {
        console.log(`[INFO]     From Profile Service(getTransaction)--------------------------------`)
        const foundRecord = await this.knex("transactions").count("user_id").where("user_id",id).first()
        return foundRecord

    }

    updateProfile = async (id: number, firstName: string, lastName: string, description: string, email: string, profile_image: string | undefined,title:string,) => {
        console.log(`[INFO]     From Profile Service(updateUser)--------------------------------`)
        try {
            if (profile_image) {
                const data = { first_name: firstName, last_name: lastName, description: description, email: email, profile_image: profile_image,title:title }
                await this.knex("users").where("id", id).update(data)
                return true
            } else {
                const data = { first_name: firstName, last_name: lastName, description: description, email: email,title:title }
                await this.knex("users").where("id", id).update(data)
                return true
            }
        } catch (error) {
            console.error(error.message)
            return false
        }
    }

}