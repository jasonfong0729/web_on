import { Knex } from "knex";
import { tables } from "../utils/models"

export class StockService {
    constructor(private knex: Knex) { }

    getAllStocksWithLatestPrice = async () => {
        const result= await this.knex(tables.STOCK).count();
        let stocks=parseInt(result[0].count.toString())
        let allStockInfoList = []
        for (let i = 1; i < stocks+1; i++) {
            const latestPrice = await this.knex(tables.mins15)
                .first("stock_id", "stock_code", "stock_name", "close", "date_time")
                .innerJoin(tables.STOCK, `${tables.mins15}.stock_id`, `${tables.STOCK}.id`)
                .whereNot("close", NaN)
                .where("stock_id", i)
                .orderBy("date_time", "desc")

            allStockInfoList.push(latestPrice)

        }
        return allStockInfoList

    }



    getSingleStocksLatestPrice = async (stockId: number) => {
        const latestPrice = await this.knex(tables.mins15)
            .first("close")
            .whereNot("close", NaN)
            .where("stock_id", stockId)
            .orderBy("date_time", "desc")
        return latestPrice
    }


    getStockDailyPrice = async (stock_id: number) => {
        const dailyPrice = await this.knex(tables.DAILY)
            .select().where("stock_id", stock_id)
            .orderBy("date");
        return dailyPrice
    }

    getStockMinsPrice = async (stock_id: number) => {
        const minsPrice = await this.knex(tables.mins15)
            .select().where("stock_id", stock_id)
            .orderBy("date_time");
        return minsPrice
    }

    createTransactions = async (stock_id: number, user_id: number, action: string, number_of_stock: number, price_of_stock: number) => {
        const tradingAmount = number_of_stock * price_of_stock
        const amountOfUser = await this.getUserStockAmount(user_id,stock_id)
        if (action === "buy") {
            await this.knex(tables.USER).where("id", user_id).decrement("cash_amount", tradingAmount);
            if (!amountOfUser) {
                await this.knex(tables.USER_STOCK).insert({
                    stock_id: stock_id,
                    user_id: user_id,
                    amount: number_of_stock
                })
            } else {
                await this.knex(tables.USER_STOCK).increment("amount", number_of_stock).where({
                    stock_id: stock_id,
                    user_id: user_id,
                })
            }
        } else if (action === "sell") {
            await this.knex(tables.USER).where("id", user_id).increment("cash_amount", tradingAmount);
            if (amountOfUser.amount > number_of_stock) {
                await this.knex(tables.USER_STOCK).decrement("amount", number_of_stock).where({
                    stock_id: stock_id,
                    user_id: user_id,
                })
            } else {
                await this.knex(tables.USER_STOCK).del().where({
                    stock_id: stock_id,
                    user_id: user_id,
                })
            }
            console.log(`[INFO] create transactions success`)
        } else {
            console.log("[Warning]THERE ARE WRONG ACTION CREATED TRANSACTION PLEASE LOOK IT")
        }
        await this.knex(tables.TRANSACTION).insert({
            stock_id: stock_id,
            user_id: user_id,
            action: action,
            number_of_stock: number_of_stock,
            price_of_stock: price_of_stock,
            amount: tradingAmount
        })
    }

    getUserTransaction = async (user_id: number) => {
        const transaction = await this.knex(tables.TRANSACTION).select()
            .where("user_id", user_id)
            .whereRaw("created_at >= CURRENT_TIMESTAMP - interval '?days'", [30])
        return transaction
    }


    createWatchStocks = async (user_id: number, stock_id: number) => {
        const count = await this.knex(tables.WATCH_LIST).count("user_id").where("user_id", user_id).first()
        if (count && typeof (count) === "number" && count >= 7) {
            const old_id = await this.knex(tables.WATCH_LIST).min('id').where("user_id", user_id)
            await this.knex(tables.WATCH_LIST).where("id", old_id).del()
        }
        await this.knex(tables.WATCH_LIST).insert({
            user_id: user_id,
            stock_id: stock_id
        })

    }


    getWatchedStocks = async (user_id: number) => {
        const stock_list = await this.knex(tables.WATCH_LIST).select().where("user_id", user_id).orderBy("id")
        return stock_list
    }

    createSearchHistory = async (user_id: number, stock_id: number) => {
        await this.knex(tables.SEARCH).insert({
            user_id: user_id,
            stock_id: stock_id
        })

    }


    getSearchHistory = async () => {
        const stock_list = await this.knex(tables.SEARCH).select("stock_code")
            .count("stock_id")
            .innerJoin(tables.STOCK, `${tables.SEARCH}.stock_id`, `${tables.STOCK}.id`)
            .groupBy("stock_id")
        return stock_list
    }


    getCashAmount = async (user_id: number) => {
        const cash = await this.knex(tables.USER).first("cash_amount").where("id", user_id)
        return cash
    }

    getUserStockAmount = async (user_id: number, stock_id: number) => {
        const stockAmount = await this.knex(tables.USER_STOCK).first("amount").where({user_id:user_id,stock_id:stock_id})
        return stockAmount
    }
}
