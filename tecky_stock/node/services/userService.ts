import { Knex } from "knex";
import { Server as SocketIO } from "socket.io";
import { checkPassword, hashPassword } from "../utils/hash"
import { User, GoogleUserData,GoogleUser } from '../utils/models'
import crypto from "crypto";

export class UserService {
    // for reference @ts-ignore
    constructor(private knex: Knex, io: SocketIO) { }

    login = async (username: string, password: string, keepLogin: boolean) => {
        //create session timeout next time
        console.log(`[INFO]     FROM User Service (login)--------------------------------`)
        const columns = ['id', 'first_name', "last_name", "username", "email", "password", "profile_image", "description"]
        const foundUser: User | undefined |any = await this.knex("users").select(columns).where("username", username).first()

        console.log(`[INFO]    Return result to controller--------------------------------`)
        if (!foundUser || !(await checkPassword(password, foundUser.password))) {
            return false;
        }
        await this.knex("users").where("id", foundUser.id).update({"keep_login":keepLogin})
        return foundUser
    }

    loginGoogle = async (googleUserData: GoogleUserData) => {
        console.log(`[INFO]     FROM User Service (loginGoogle)--------------------------------`)
        const columns = ['id', 'first_name', "last_name", "username", "email", "password", "profile_image", "description"]
        let foundUser: User = await this.knex("users").select(columns).where("email", googleUserData.email).first()
        
        console.log(`[INFO]    Return result to controller--------------------------------`)
        if (!foundUser) {
            console.log('[INFO]     No such user, creating new user')
            let tempPass = crypto.randomBytes(20).toString('hex');
            const  hashedPassword = await hashPassword(tempPass)
            let googleUser:GoogleUser = {
                first_name: googleUserData.given_name,
                last_name: googleUserData.family_name,
                username: googleUserData.email,
                email: googleUserData.email,
                password: hashedPassword,
            }
            // references
            // await client.query(`insert into "users" (username,password,is_admin) values ($1,$2,$3)`, [result.email, hashedPassword, false])
            foundUser = (await this.knex('users').insert(googleUser).returning(columns))[0]
            console.log('[INFO]     User created')
            console.log(foundUser)
            return foundUser
        }
        console.log('[INFO]     Found user')
        return foundUser
    }
    // keepLogin = async (id:number) => {
    //     console.log(`[INFO]     FROM User Service (keepLogin)--------------------------------`)
    //     const foundUserStatus:boolean = (await this.knex("users").select("keep_login").where("id", id).first()).keep_login
    //     console.log(`[INFO]     KeepLogin : ${foundUserStatus}`)
    //     return foundUserStatus
    // }
}