import express from 'express';
import {Request,Response} from 'express';
import expressSession from "express-session";

import http from 'http';
import { Server as SocketIO } from 'socket.io';
import { isLoggedInStatic, isLoggedInApi } from "./utils/guards";

import path from 'path';
import dotenv from 'dotenv';
dotenv.config();
import grant from 'grant';

import Knex from "knex";
const knexConfigs = require("./knexfile");
const configMode = process.env.NODE_ENV || "development";
const knexConfig = knexConfigs[configMode];

const knex = Knex(knexConfig);

const grantExpress = grant.express({
    "defaults":{
        "origin": process.env.GRANT_URL,
        "transport": "session",
        "state": true,
    },
    "google":{
        "key": process.env.GOOGLE_CLIENT_ID || "",
        "secret": process.env.GOOGLE_CLIENT_SECRET || "",
        "scope": ["profile","email"],
        "callback": "/user/login/google"
    }
});


const app = express();

export const server = new http.Server(app);
const io = new SocketIO(server);
const sessionMiddleware = expressSession({
    secret: "We are group 9! Please enjoy!!",
    resave: true,
    saveUninitialized: true,
})
app.use(sessionMiddleware);
app.use(grantExpress as express.RequestHandler);
app.use(express.json());
app.get("/test", (req, res) => {console.log("hihidfjklasjflkwajfokwaejiofej")})
//login Guard
app.get('/', isLoggedInApi, function (req: Request, res: Response) {
    res.sendFile(path.join(__dirname,"public","index.html"));
})

//another optional 
// import type { Knex as KnexType } from "knex";
// const knex = Knex(knexConfig) as KnexType;

import { SignupService } from './services/signUpService';
import { SignupController } from './controllers/signUpController';
const signUpService = new SignupService(knex as any);
export const signUpController = new SignupController(signUpService);
import { signUpRoute } from "./routers/signUpRoute"

app.use("/signup", signUpRoute);

//user
import { UserService } from './services/userService';
import { UserController } from './controllers/userController';
const userService = new UserService(knex as any , io);
export const userController = new UserController(userService);
import { userRoute } from "./routers/userRoute"
app.use("/user",userRoute);

//stock
import { StockService } from './services/stockService';
import { StockController } from './controllers/stockController';

const stockService = new StockService(knex as any );
export const stockController = new StockController(stockService);

import { stockRoute } from "./routers/stockRoute"
app.use("/trading",stockRoute);

//wallet
import { WalletService } from './services/walletService';
import { WalletController } from './controllers/walletController';

const walletService = new WalletService(knex as any );
export const walletController = new WalletController(walletService);

import { walletRoute } from "./routers/walletRoute"
app.use("/wallet",walletRoute);

//profile
import { ProfileService } from './services/profileService';
import { ProfileController } from './controllers/profileController';
const profileService = new ProfileService(knex as any);
export const profileController = new ProfileController(profileService);
import { profileRoute } from './routers/profileRoute';

app.use("/profile",profileRoute);

//folder permission
app.use(express.static(path.join(__dirname, "public")))
app.use(isLoggedInStatic, express.static(path.join(__dirname, "private")))

//404html
app.use((req, res) => {
    res.sendFile(path.resolve("./public/404.html"));
});

const PORT = 8080;
server.listen(PORT, () => {
    console.log(`[INFO]     Listening at http://localhost:${PORT}/`);
});