import { hashPassword } from "../utils/hash";
import { Knex } from "knex";
import { tables, USERData, WALLETData, SEARCHData, WATCH_LISTData, TRANSACTIONData } from "../utils/models"

import xlsx from "xlsx";
// import { readJsonFilePromise } from "../utils/jsonfile"
import { readMostActiveStock } from "../utils/csvfile"
import path from "path"

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries

    //All stock approach
    // const stockInNasdaq = await readCSVPromise(path.join(__dirname, "..", "database", "stock_in_NASDAQ.csv"))
    // const stockInNyse:STOCKinNYSEData[] = await readJsonFilePromise(path.join(__dirname, "..", "database", "stock_in_nyse.json"))

    const mostActiveStocks = await readMostActiveStock(path.join(__dirname, "..", "database", "top100activeStock04042022.csv"))

    const workbook = xlsx.readFile(path.join(__dirname, "..", "database", "users.xlsx"));
    const users = xlsx.utils.sheet_to_json<USERData>(workbook.Sheets["user"]);
    const wallets = xlsx.utils.sheet_to_json<WALLETData>(workbook.Sheets["wallet"]);
    const watchLists = xlsx.utils.sheet_to_json<WATCH_LISTData>(workbook.Sheets["watchlist"]);
    const searchHistory = xlsx.utils.sheet_to_json<SEARCHData>(workbook.Sheets["search history"]);
    const transactions = xlsx.utils.sheet_to_json<TRANSACTIONData>(workbook.Sheets["transaction"]);
    const userStock = xlsx.utils.sheet_to_json<TRANSACTIONData>(workbook.Sheets["users_stock"]);

    const trx = await knex.transaction()

    try {
        // Delete Tables and Reset sequence
        await trx(tables.TRANSACTION).del();
        await trx.raw(/*sql*/`ALTER SEQUENCE ${tables.TRANSACTION}_id_seq RESTART WITH 1`);

        await trx(tables.USER_STOCK).del();
        await trx.raw(/*sql*/`ALTER SEQUENCE ${tables.USER_STOCK}_id_seq RESTART WITH 1`);

        await trx(tables.WATCH_LIST).del();
        await trx.raw(/*sql*/`ALTER SEQUENCE ${tables.WATCH_LIST}_id_seq RESTART WITH 1`);

        await trx(tables.SEARCH).del();
        await trx.raw(/*sql*/`ALTER SEQUENCE ${tables.SEARCH}_id_seq RESTART WITH 1`);

        await trx(tables.mins15).del();
        await trx.raw(/*sql*/`ALTER SEQUENCE ${tables.mins15}_id_seq RESTART WITH 1`);

        await trx(tables.DAILY).del();
        await trx.raw(/*sql*/`ALTER SEQUENCE ${tables.DAILY}_id_seq RESTART WITH 1`);

        await trx(tables.STOCK).del();
        await trx.raw(/*sql*/`ALTER SEQUENCE ${tables.STOCK}_id_seq RESTART WITH 1`);

        await trx(tables.WALLET).del();
        await trx.raw(/*sql*/`ALTER SEQUENCE ${tables.WALLET}_id_seq RESTART WITH 1`);

        await trx(tables.USER).del();
        await trx.raw(/*sql*/`ALTER SEQUENCE ${tables.USER}_id_seq RESTART WITH 1`);

        // Inserts seed entries

        // Insert User Data
        for (const user of users) {
            user.password = await hashPassword(String(user.password));
        }
        await trx(tables.USER).insert(users);

        // Insert Wallet Data
        await trx(tables.WALLET).insert(wallets);

        // Insert Stock Data
        await trx(tables.STOCK).insert(mostActiveStocks);

        // Insert Watch list Data
        await trx(tables.WATCH_LIST).insert(watchLists);

        // Insert Search History Data
        await trx(tables.SEARCH).insert(searchHistory);

        // Insert Transaction Data
        await trx(tables.TRANSACTION).insert(transactions);

        await trx(tables.USER_STOCK).insert(userStock);

        await trx.commit()

        console.log("[INFO]     Success!")
    } catch (error) {
        console.error(error.message);
        console.log("[INFO]     Fail!")
        await trx.rollback();
    } finally {
        trx.destroy();
    }
};
