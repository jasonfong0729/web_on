import { Knex } from "knex";
import { tables } from "../utils/models"

export async function up(knex: Knex): Promise<void> {
    await knex.schema.dropTable(tables.DAILY)
    await knex.schema.dropTable(tables.mins15)
    await knex.schema.createTable(tables.DAILY, (table) => {
        table.increments();
        table.integer("stock_id").notNullable();
        table.foreign("stock_id").references(`${tables.STOCK}.id`);
        table.decimal("open", 10).notNullable();
        table.decimal("close", 10).notNullable();
        table.decimal("high", 10).notNullable();
        table.decimal("low", 10).notNullable();
        table.decimal("volume", 15).notNullable();
        table.date("date").notNullable();
        table.timestamps(false, true);
    });
    await knex.schema.createTable(tables.mins15, (table) => {
        table.increments();
        table.integer("stock_id").notNullable();
        table.foreign("stock_id").references(`${tables.STOCK}.id`);
        table.decimal("open", 10).notNullable();
        table.decimal("close", 10).notNullable();
        table.decimal("high", 10).notNullable();
        table.decimal("low", 10).notNullable();
        table.decimal("volume", 15).notNullable();
        table.timestamp("date_time").notNullable();
        table.timestamps(false, true);
    });
};


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(tables.DAILY)
    await knex.schema.dropTable(tables.mins15)
    await knex.schema.createTable(tables.DAILY, (table) => {
        table.increments();
        table.integer("stock_id").notNullable();
        table.foreign("stock_id").references(`${tables.USER}.id`);
        table.decimal("open", 10, 2).notNullable();
        table.decimal("close", 10, 2).notNullable();
        table.decimal("high", 10, 2).notNullable();
        table.decimal("low", 10, 2).notNullable();
        table.date("date").notNullable;
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.mins15, (table) => {
        table.increments();
        table.integer("stock_id").notNullable();
        table.foreign("stock_id").references(`${tables.USER}.id`);
        table.decimal("open", 10, 2).notNullable();
        table.decimal("close", 10, 2).notNullable();
        table.decimal("high", 10, 2).notNullable();
        table.decimal("low", 10, 2).notNullable();
        table.timestamp("date_time").notNullable();
        table.timestamps(false, true);
    });
};