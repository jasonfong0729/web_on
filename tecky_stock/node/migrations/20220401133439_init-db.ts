import { Knex } from "knex";
import {tables} from "../utils/models"


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(tables.USER, (table) => {
        table.increments();
        table.string("first_name").notNullable();
        table.string("last_name").notNullable();
        table.string("username").notNullable().unique();
        table.string("email").notNullable().unique();
        table.string("password").notNullable();
        table.string("profile_image").notNullable().defaultTo("default.png")
        table.boolean("keep_login").notNullable().defaultTo(false);
        table.boolean("is_online").notNullable().defaultTo(false);
        table.boolean("notification").notNullable().defaultTo(true);
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.WALLET, (table) => {
        table.increments();
        table.integer("user_id").notNullable();
        table.foreign("user_id").references(`${tables.USER}.id`);
        table.string("action").notNullable();
        table.decimal("amount", 19, 2).notNullable();
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.STOCK, (table) => {
        table.increments();
        table.string("stock_code").notNullable();
        table.string("stock_name").notNullable();
        table.integer("ranking").notNullable().unsigned();
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.DAILY, (table) => {
        table.increments();
        table.integer("stock_id").notNullable();
        table.foreign("stock_id").references(`${tables.USER}.id`);
        table.decimal("open", 10, 2).notNullable();
        table.decimal("close", 10, 2).notNullable();
        table.decimal("high", 10, 2).notNullable();
        table.decimal("low", 10, 2).notNullable();
        table.date("date").notNullable;
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.mins15, (table) => {
        table.increments();
        table.integer("stock_id").notNullable();
        table.foreign("stock_id").references(`${tables.USER}.id`);
        table.decimal("open", 10, 2).notNullable();
        table.decimal("close", 10, 2).notNullable();
        table.decimal("high", 10, 2).notNullable();
        table.decimal("low", 10, 2).notNullable();
        table.timestamp("date_time").notNullable();
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.SEARCH, (table) => {
        table.increments();
        table.integer("user_id").notNullable();
        table.foreign("user_id").references(`${tables.USER}.id`);
        table.integer("stock_id").notNullable();
        table.foreign("stock_id").references(`${tables.STOCK}.id`);
        table.timestamps(false, true);
    });


    await knex.schema.createTable(tables.WATCH_LIST, (table) => {
        table.increments();
        table.integer("user_id").notNullable();
        table.foreign("user_id").references(`${tables.USER}.id`);
        table.integer("stock_id").notNullable();
        table.foreign("stock_id").references(`${tables.STOCK}.id`);
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.TRANSACTION, (table) => {
        table.increments();
        table.integer("user_id").notNullable();
        table.foreign("user_id").references(`${tables.USER}.id`);
        table.integer("stock_id").notNullable();
        table.foreign("stock_id").references(`${tables.STOCK}.id`);
        table.string("action").notNullable();
        table.integer("number_of_stock").notNullable();
        table.decimal("price_of_stock", 9, 2).notNullable();
        table.decimal("amount", 19, 2).notNullable();
        table.timestamps(false, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(tables.TRANSACTION);
    await knex.schema.dropTable(tables.WATCH_LIST);
    await knex.schema.dropTable(tables.SEARCH);
    await knex.schema.dropTable(tables.mins15);
    await knex.schema.dropTable(tables.DAILY);
    await knex.schema.dropTable(tables.STOCK);
    await knex.schema.dropTable(tables.WALLET);
    await knex.schema.dropTable(tables.USER);
}

