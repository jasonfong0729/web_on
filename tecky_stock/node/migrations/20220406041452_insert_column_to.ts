import { Knex } from "knex";
import {tables} from "../utils/models"

export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable(tables.USER)) {
        await knex.schema.table(tables.USER, (table) => {
          table.text("description").defaultTo("I am using Tecky Stock")
        });
      }
}


export async function down(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable(tables.USER)) {
        await knex.schema.table(tables.USER, (table) => {
          table.dropColumn("description")
        });
      }
}

