import { Knex } from "knex";
import { tables } from "../utils/models"


export async function up(knex: Knex): Promise<void> {
    await knex.schema.table(tables.USER, function (table) {
        table.decimal("cash_amount", 16).defaultTo(0);
    })
    await knex.schema.createTable(tables.USER_STOCK, function (table) {
        table.increments();
        table.integer('user_id').notNullable();
        table.foreign("user_id").references(`${tables.USER}.id`);
        table.integer('stock_id').notNullable();
        table.foreign("stock_id").references(`${tables.STOCK}.id`);
        table.integer('amount').notNullable();
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(tables.USER_STOCK)
    await knex.schema.alterTable(tables.USER, function (table) {
        table.dropColumn("cash_amount");
    })
}

