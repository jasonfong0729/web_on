import express from 'express';
import { signUpController } from '../main';

export const signUpRoute = express.Router();

signUpRoute.post("/",signUpController.createNewUser)