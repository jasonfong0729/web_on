import express from 'express';
import { userController } from '../main';
// import { Request, Response } from 'express';
export const userRoute = express.Router();

userRoute.post("/login", userController.login)
userRoute.get("/login/google", userController.loginGoogle)
userRoute.get("/logout", userController.logout)

// userRoute.get("/checkKeepLogin",userController.keepLogin)