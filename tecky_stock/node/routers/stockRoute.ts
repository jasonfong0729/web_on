import express from 'express';
import { stockController } from '../main';

export const stockRoute = express.Router();



stockRoute.get("/get",stockController.getAllStock)
stockRoute.get("/getSingle",stockController.getSingleStockData)
stockRoute.post("/buying",stockController.stockBuy)
stockRoute.post("/selling",stockController.stockSell)
stockRoute.put("/search",stockController.changeSession)

