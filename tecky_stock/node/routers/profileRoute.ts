import express from 'express';
import { profileController } from '../main';
import multer from "multer";
import path from "path";

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve("./private/images/users"));
    },
    filename: function (req, file, cb) {
        const {id} = req.session["user"]
        cb(null, `User_ID_${id}-logo-${Date.now()}.${file.mimetype.split("/")[1]}`);
    },
});
const profileUpload = multer({ storage });
export const profileRoute = express.Router();
(`[INFO]     FROM Profile Router------------------------------------`)
profileRoute.get("/",profileController.getProfile)
profileRoute.get("/transaction",profileController.getTransaction)
profileRoute.post("/updateProfile",profileUpload.single("profile_image"),profileController.updateProfile)