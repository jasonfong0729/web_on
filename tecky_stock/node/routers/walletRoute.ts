import express from 'express';
import { walletController } from '../main';

export const walletRoute = express.Router();



walletRoute.get("/stock",walletController.userStockWithLatestPrice)
walletRoute.get("/cash",walletController.userCashBalance)
walletRoute.post("/deposit",walletController.deposit)
walletRoute.post("/withdraw",walletController.withdraw)

