import { Page, chromium, Browser } from 'playwright';
import '../main';
// import { server } from '../main';
import path from "path"
import Knex from 'knex';
import * as knexfile from "../knexfile"
const configMode = "test"
const knex = Knex(knexfile[configMode]);    // Now the connection is a testing connection.
describe('SignUp', () => {
    let page: Page;
    let browser: Browser;
    beforeAll(async () => {
        await knex.seed.run();
        browser = await chromium.launch();
        page = await browser.newPage();
    })

    it('should display "Sign Up" text on title', async () => {
        await page.goto('http://localhost:8080/signup.html')
        const title = await page.title();
        expect(title).toContain('Signup');
    });

    it('should successfully signup', async () => {
        await page.goto('http://localhost:8080/signup.html')

        await page.evaluate(() => {

            const firstName = document.querySelector('[name=first-name]');
            const lastName = document.querySelector('[name=last-name]');
            const username = document.querySelector('[name=username]');
            const password = document.querySelector('[name=password]');
            const reConfirmPassword = document.querySelector('[name=password-conform]');
            const email = document.querySelector('[name=email]');
            if (username && password) {
                (firstName as HTMLInputElement).value = "jasonfong1";
                (lastName as HTMLInputElement).value = "jasonfong1";
                (email as HTMLInputElement).value = "jasonfong1@gmail.com";
                (username as HTMLInputElement).value = "jasonfong1";
                (password as HTMLInputElement).value = "IamHandsome";
                (reConfirmPassword as HTMLInputElement).value = "IamHandsome";

            }

            const submit = document.querySelector('[type=submit]');
            if(submit){
              (submit as HTMLInputElement).click();
            }
        });
        // await page.fill("[name=first-name]","hihi")

        await page.waitForNavigation();

        await page.screenshot({ path: path.join(__dirname, '/screenshot1.png') });

        let h1 = await page.mainFrame().waitForSelector('.form-slogan');
        // console.log(h1)
        expect(await h1.innerHTML()).toContain('Start Trading In Tecky Stock With AI Analysis');
        // expect(await h1.innerHTML()).toContain('Let\'s Register Account');

    });

    afterAll(async () => {
        await knex.destroy();
        await browser.close();
        // server.close();
    });

})