import { Knex } from "knex";
import express from "express";
import { UserController } from "../controllers/userController";
import { UserService } from "../services/userService";
import { Server as SocketIO } from "socket.io";
import { GoogleUserData, User } from '../utils/models'


jest.mock("../services/userService");
jest.mock('node-fetch');

describe("User Controller Test Case", () => {
    let controller: UserController;
    let service: UserService;
    let req: express.Request;
    let res: express.Response;




    beforeEach(() => {
        // Knex -> Service -> Controller
        service = new UserService({} as Knex, {} as SocketIO);

        service.login = jest.fn((username: string, password: string, keepLogin: boolean) => Promise.resolve(
            {} as false |
            {
                id: 1;
                first_name: "test",
                last_name: "test",
                username: "test",
                email: "test",
                password: "test",
                profile_image: "test",
                description: "test"
            }));

        service.loginGoogle = jest.fn((googleUserData: GoogleUserData) => Promise.resolve(
            {} as User));

        controller = new UserController(service);

        // Default
        req = {
            params: {},
            query: {},
            body: {},
            session: {},
        } as express.Request;

        res = {
            status: jest.fn(() => res),
            json: jest.fn(),
            redirect: jest.fn(),
        } as any as express.Response;

        // Default
        req.body = {
            username: "hihi",
            password: "hihi",
            keepLogin: true
        }

        // for testing google login
        // req.session = {
        //     grant: {response:{access_token:"test"}}
        // } as any

        

    })

    test("login - Success", async () => {
        service.login = jest.fn((username: string, password: string, keepLogin: boolean) => Promise.resolve(
            {
                id: 1,
                first_name: "test",
                last_name: "test",
                username: "test",
                email: "test",
                password: "test",
                profile_image: "test",
                description: "test"
            }));

        await controller.login(req, res)
        const { username, password, keepLogin, } = req.body
        expect(service.login).toBeCalledTimes(1);
        expect(service.login).toBeCalledWith(username, password, keepLogin);
        expect(res.json).toBeCalledTimes(1);
        // How to write the test case for req.session
        // expect(req.session).toBeCalledTimes(2);
        expect(res.json).toBeCalledWith({ message: "success" });

    })

    test("login - Fail", async () => {
        service.login = jest.fn((username: string, password: string, keepLogin: boolean) => Promise.resolve(
        false));
        
        await controller.login(req, res)
        const { username, password, keepLogin, } = req.body
        expect(service.login).toBeCalledTimes(1);
        expect(service.login).toBeCalledWith(username, password, keepLogin);
        expect(res.json).toBeCalledTimes(1);
        // How to write the test case for req.session
        // expect(req.session).toBeCalledTimes(2);
        expect(res.json).toBeCalledWith({ message: "Incorrect Username or Password" });

    })

    // test("loginGoogle success", async () => {
    //     service.loginGoogle = jest.fn((googleUserData: GoogleUserData) => Promise.resolve(
    //         {
    //             id: 1,
    //             first_name: "test",
    //             last_name: "test",
    //             username: "test",
    //             email: "test",
    //             password: "test",
    //             profile_image: "test",
    //             description: "test"
    //         }));
        
    //     (fetch as jest.Mock).mockReturnValue("on99")
        
    //     res.json = jest.fn().mockReturnValue({
    //         id: '107193482896823935703',
    //         email: 'johnfong279@gmail.com',
    //         verified_email: true,
    //         name: 'J FONG',
    //         given_name: 'J',
    //         family_name: 'FONG',
    //         picture: 'https://lh3.googleusercontent.com/a/AATXAJxbhIJzGy9VEVTQ5ItfDNcNgN5ElZDZY64iOF9w=s96-c',
    //         locale: 'en-GB'
    //     })
    //     await controller.loginGoogle(req, res)
    //     const googleUser = res.json()
    //     expect(service.loginGoogle).toBeCalledTimes(1);
    //     expect(service.loginGoogle).toBeCalledWith(googleUser);
    //     expect(res.redirect).toBeCalledTimes(1);
    //     expect(res.redirect).toBeCalledWith("/");

    // })
});