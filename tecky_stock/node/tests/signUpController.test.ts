import { Knex } from "knex";
import { SignupController } from '../controllers/signUpController';
import { SignupService } from "../services/signUpService";
import express from "express";

jest.mock("../services/signUpService");

describe("Signup Controller Test Case", () => {
    let controller: SignupController;
    let service: SignupService;
    let req: express.Request;
    let res: express.Response;

    beforeEach(() => {
        // Knex -> Service -> Controller
        service = new SignupService({} as Knex);

        service.createNewUser = jest.fn((firstName: string, lastName: string, username: string, password: string, email: string) => Promise.resolve(
            {} as "username" | "email" | object));
        // Default
        req = {
            params: {},
            query: {},
            body: {},
            session: {},
        } as express.Request;
        controller = new SignupController(service);
        res = {
            status: jest.fn(() => res),
            json: jest.fn(),
        } as any as express.Response;

        // Default
        req.body = {
            firstName: "hihi",
            lastName: "hihi",
            username: "hihi",
            password: "hihi",
            email: "hihi"
        }
    })

    test("create user - Success", async () => {
        service.createNewUser = jest.fn((firstName: string, lastName: string, username: string, password: string, email: string) => Promise.resolve(
            {} as object));

        await controller.createNewUser(req, res);
        const { firstName, lastName, username, password, email } = req.body
        expect(service.createNewUser).toBeCalledTimes(1);
        expect(service.createNewUser).toBeCalledWith(firstName, lastName, username, password, email);
        expect(res.json).toBeCalledTimes(1);
        expect(res.json).toBeCalledWith({ message: "success" });
    });

    test("create user - fail", async () => {
        service.createNewUser = jest.fn((firstName: string, lastName: string, username: string, password: string, email: string) => Promise.resolve(
            "username"));

        await controller.createNewUser(req, res);
        const { firstName, lastName, username, password, email } = req.body
        expect(service.createNewUser).toBeCalledTimes(1);
        expect(service.createNewUser).toBeCalledWith(firstName, lastName, username, password, email);
        expect(res.json).toBeCalledTimes(1);
        expect(res.json).toBeCalledWith({ message: "Sorry, name has been used already." });
    });

    test("create user - fail", async () => {
        service.createNewUser = jest.fn((firstName: string, lastName: string, username: string, password: string, email: string) => Promise.resolve(
            "email"));

        await controller.createNewUser(req, res);
        const { firstName, lastName, username, password, email } = req.body
        expect(service.createNewUser).toBeCalledTimes(1);
        expect(service.createNewUser).toBeCalledWith(firstName, lastName, username, password, email);
        expect(res.json).toBeCalledTimes(1);
        expect(res.json).toBeCalledWith({ message: "Sorry, email has been used already." });
    });

    // test("not enough information from req.body", async () => {
    //     req.body = {
    //         firstName: "hihi",
    //         lastName: "hihi",
    //         username: "hihi",
    //         password: "hihi",
    //     }
    //     service.createNewUser = jest.fn((firstName: string, lastName: string, username: string, password: string, email: string) => Promise.resolve(
    //         {} as object));
    //     await controller.createNewUser(req, res);
    //     expect(controller.createNewUser).toThrow();
    // })
});