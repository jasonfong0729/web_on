import Knex from 'knex';
import * as knexfile from "../knexfile"
import { UserService } from '../services/userService';
import { Server as SocketIO } from "socket.io";

import { checkPassword } from "../utils/hash"
const configMode = "test"
const knex = Knex(knexfile[configMode]);    // Now the connection is a testing connection.

describe("Test User Service", () => {

    beforeAll(async () => {
        await knex.seed.run();
    })

    let userService: UserService;
    let io = {
        emit: jest.fn()
    }
    beforeEach(async () => {
        userService = new UserService(knex as any, io as any as SocketIO);
    })

    test("login - found user with correct password", async () => {
        const loginData = {
            username: "danielho1",
            password: "tecky",
            keepLogin: true
        } 
        const foundUser:any = await userService.login(loginData.username, loginData.password,loginData.keepLogin)
        const checking = await checkPassword(loginData.password,foundUser.password)
        expect(typeof foundUser).toBe("object");
        expect(foundUser).not.toEqual(false)
        expect(checking).toBe(true)
    })

    test("login - found user with incorrect password", async () => {
        const loginData = {
            username: "danielho1",
            password: "tecky22",
            keepLogin: true
        } 
        const foundUser:any = await userService.login(loginData.username, loginData.password,loginData.keepLogin)
        // const checking = await checkPassword(loginData.password,foundUser.password)
        expect(typeof foundUser).toBe("boolean");
        expect(foundUser).toEqual(false)
        // expect(checking).toBe(false)
    })

    test("login - unfounded user", async () => {
        const loginData = {
            username: "danielho1213",
            password: "tecky22",
            keepLogin: true
        } 
        const foundUser:any = await userService.login(loginData.username, loginData.password,loginData.keepLogin)
        expect(typeof foundUser).toBe("boolean");
        expect(foundUser).toEqual(false)
    })


    //login google test

    
    afterAll(async () => {
        await knex.destroy();
    });
})