import Knex from 'knex';
import * as knexfile from "../knexfile"
const configMode = "test"
const knex = Knex(knexfile[configMode]);    // Now the connection is a testing connection.
import { SignupService } from "../services/signUpService";


describe("Test SignupService", () => {

    beforeAll(async () => {
        await knex.seed.run();
    })

    let signupService: SignupService;

    beforeEach(async () => {
        signupService = new SignupService(knex as any)
    })

    it("hihi user should be created", async () => {

        const newUser = {
            first_name: "hihi",
            last_name: "hihi",
            username: "hihi",
            password: "hihi",
            email: "hihi"
        }
        const foundUsername = await knex("users").select("id").where("username", newUser.username).first()
        await signupService.createNewUser(newUser.first_name,newUser.last_name,newUser.username,newUser.password,newUser.email)
        

        const userID:any = (await knex("users").select("id").where('username',newUser.username))[0].id

        expect(foundUsername).toBe(undefined);
        expect(userID).toBeDefined
    })

    it("'danielho1' username should have been created", async () => {

        const newUser = {
            first_name: "hihi",
            last_name: "hihi",
            username: "danielho1",
            password: "hihi",
            email: "hihi"
        }
        const foundUsername = await knex("users").select("id").where("username", newUser.username).first()
        const result = await signupService.createNewUser(newUser.first_name,newUser.last_name,newUser.username,newUser.password,newUser.email)
        
        expect(foundUsername).toBeDefined;
        expect(result).toMatch("username")
    })

    it("'danielho1@gmail.com' user should have been used", async () => {

        const newUser = {
            first_name: "hihi",
            last_name: "hihi",
            username: "danielho13213",
            password: "hihi",
            email: "danielho1@gmail.com"
        }
        const foundEmail = await knex("users").select("id").where("email", newUser.email).first()
        const result = await signupService.createNewUser(newUser.first_name,newUser.last_name,newUser.username,newUser.password,newUser.email)
        
        expect(foundEmail).toBeDefined;
        expect(result).toMatch("email")
    })

    afterAll(async () => {
        await knex.destroy();
    });
})