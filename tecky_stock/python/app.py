import os
import csv
from turtle import st
import talib
import yfinance as yf
import pandas
from datetime import datetime, timedelta
from flask import Flask, redirect, render_template, request
from pattern import candlestick_patterns
from predict import plot_graph

app = Flask(__name__)


@app.route('/download')
def download():

    today = datetime.today().strftime('%Y-%m-%d')
    sixMonthAgo = (datetime.now() - timedelta(180)).strftime('%Y-%m-%d')

    with open('datasets/symbols.csv') as f:

        for line in f:
            if "," not in line:
                continue
            symbol = line.split(",")[0]
            data = yf.download(symbol, start=sixMonthAgo, end=today)
            data.to_csv('datasets/daily/{}.csv'.format(symbol))

    return redirect('/')

@app.route('/predict')
def predict():

    ticker = request.args.get('ticker', False)
    plot_graph(ticker)

    return redirect('/')

@app.route('/')
def index():

    # plot_graph()
    pattern = request.args.get('pattern', False)
    # arg = request.args('pattern')
    # print(pattern)
    stocks = {}

    with open('datasets/symbols.csv') as f:

        for row in csv.reader(f):
            stocks[row[0]] = {'company': row[1]}
            # print(stocks[row[0]])

    if pattern:
        for filename in os.listdir('datasets/daily'):
            df = pandas.read_csv('datasets/daily/{}'.format(filename))
            pattern_function = getattr(talib, pattern)
            symbol = filename.split('.')[0]

            try:
                results = pattern_function(
                    df['Open'], df['High'], df['Low'], df['Close'])
                last = results.tail(1).values[0]
                df[pattern] = results
                
   
                if last != 0:
                    pattern_happen_days = df[df[pattern] != 0]
                    stocks[symbol]['pattern_happen_days'] = pattern_happen_days['Date'].values  
                    stocks[symbol]['pattern_value'] = pattern_happen_days[pattern].values
                    stocks[symbol]['data_update_on'] = pattern_happen_days['Date'].tail(1).values
                    # print(pattern_happen_days["Date"].head().values + "\n")

                    print(stocks[symbol]['data_update_on'])
                    # print(stocks[symbol]['pattern_value'])

                if last > 0:
                    stocks[symbol][pattern] = 'Bullish'
                elif last < 0:
                    stocks[symbol][pattern] = 'Bearish'
                else:
                    stocks[symbol][pattern] = None
            except Exception as e:
                print('failed on filename: ', filename)

    return render_template('index.html', candlestick_patterns=candlestick_patterns, stocks=stocks, pattern=pattern)


if __name__ == '__main__':
    app.debug = False
    app.run()
