import talib
import yfinance as yf
import pandas as pd

data = yf.download("SPY", start="2020-01-01", end="2020-08-01")
# print(data)

morning_star = talib.CDLMORNINGSTAR(data['Open'], data['High'], data['Low'], data['Close'])


engulfing = talib.CDLENGULFING(data['Open'], data['High'], data['Low'], data['Close'])


print("------------------------------------------------------------------------------------")
# data['Morning Star'] = morning_star
data['Engulfing'] = engulfing

# print(data['Engulfing'])
print("------------------------------------------------------------------------------------")

engulfing_days = data[data['Engulfing'] != 0]

# engulfing_days_df = pd.DataFrame(engulfing_days)
print(engulfing_days['Engulfing'])


# print(engulfing_days.tail(5).values[0])