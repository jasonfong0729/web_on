import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
import keras
from collections import deque
from yahoo_fin import stock_info as si
import tensorflow as tf

# ticker = "TSLA"
N_STEPS = 49
SCALE = True
LOOKUP_STEP = 1
FEATURE_COLUMNS = ["adjclose", "volume", "open", "high", "low"]
model = keras.models.load_model('./model')


def load_data(ticker, n_steps=N_STEPS, scale=True,  lookup_step=LOOKUP_STEP,
              feature_columns=['adjclose', 'volume', 'open', 'high', 'low']):
    """
    Loads data from Yahoo Finance source, as well as scaling, shuffling, normalizing and splitting.
    Params:
        ticker (str/pd.DataFrame): the ticker you want to load, examples include AAPL, TESL, etc.
        n_steps (int): the historical sequence length (i.e window size) used to predict, default is 50
        scale (bool): whether to scale prices from 0 to 1, default is True
        shuffle (bool): whether to shuffle the dataset (both training & testing), default is True
        lookup_step (int): the future lookup step to predict, default is 1 (e.g next day)
        split_by_date (bool): whether we split the dataset into training/testing by date, setting it 
            to False will split datasets in a random way
        test_size (float): ratio for test data, default is 0.2 (20% testing data)
        feature_columns (list): the list of features to use to feed into the model, default is everything grabbed from yahoo_fin
    """
    # see if ticker is already a loaded stock from yahoo finance
    if isinstance(ticker, str):
        # load it from yahoo_fin library
        df = si.get_data(ticker)
    elif isinstance(ticker, pd.DataFrame):
        # already loaded, use it directly
        df = ticker
    else:
        raise TypeError(
            "ticker can be either a str or a `pd.DataFrame` instances")

    # this will contain all the elements we want to return from this function
    result = {}
    # we will also return the original dataframe itself
    result['df'] = df.copy()

    # make sure that the passed feature_columns exist in the dataframe
    for col in feature_columns:
        assert col in df.columns, f"'{col}' does not exist in the dataframe."

    # add date as a column
    if "date" not in df.columns:
        df["date"] = df.index

    if scale:
        column_scaler = {}
        # scale the data (prices) from 0 to 1
        for column in feature_columns:
            scaler = preprocessing.MinMaxScaler()
            df[column] = scaler.fit_transform(
                np.expand_dims(df[column].values, axis=1))
            column_scaler[column] = scaler

        # add the MinMaxScaler instances to the result returned
        result["column_scaler"] = column_scaler

    # add the target column (label) by shifting by `lookup_step`
    df['future'] = df['adjclose'].shift(-lookup_step)

    # last `lookup_step` columns contains NaN in future column
    # get them before droping NaNs
    last_sequence = np.array(df[feature_columns].tail(lookup_step))

    # drop NaNs
    df.dropna(inplace=True)

    sequence_data = []
    sequences = deque(maxlen=n_steps)

    for entry, target in zip(df[feature_columns + ["date"]].values, df['future'].values):
        sequences.append(entry)
        if len(sequences) == n_steps:
            sequence_data.append([np.array(sequences), target])

    # get the last sequence by appending the last `n_step` sequence with `lookup_step` sequence
    # for instance, if n_steps=50 and lookup_step=10, last_sequence should be of 60 (that is 50+10) length
    # this last_sequence will be used to predict future stock prices that are not available in the dataset
    last_sequence = list([s[:len(feature_columns)]
                         for s in sequences]) + list(last_sequence)
    last_sequence = np.array(last_sequence).astype(np.float32)
    # add to result
    result['last_sequence'] = last_sequence

    return result


def predict(model, data):

    # retrieve the last sequence from data
    last_sequence = data["last_sequence"][-N_STEPS:]
    # print(last_sequence)
    # print("---------------------------------------------------------------------")
    # expand dimension
    last_sequence = np.expand_dims(last_sequence, axis=0)
    # print(last_sequence)
    # get the prediction (scaled from 0 to 1)
    prediction = model.predict(last_sequence)
    # get the price (by inverting the scaling)
    if SCALE:
        predicted_price = data["column_scaler"]["adjclose"].inverse_transform(prediction)[0][0]
    else:
        predicted_price = prediction[0][0]
    return predicted_price




# print(data)
# tmr_price = predict(model, data)
# print(f"{ticker} price after {LOOKUP_STEP} day is ${tmr_price:.2f}")


def get_final_df(model, data):

    # X_test = data["X_test"]
    # print(X_test)
    # y_test = data["y_test"]
    # data["predict_data"]= np.array(data["last_sequence"])

    # print(data)
    # np.expand_dims(last_sequence, axis=0)
    # print( np.expand_dims(data["last_sequence"], axis=0))
    # perform prediction and get prices
    # print(np.shape (data["df"]))
    # print("----------------------------------------------------------")
    # print(np.shape(data["last_sequence"]))
    # print("-------------------------------------------------------")
    # print(data["last_sequence"])
    prediction = model.predict(np.expand_dims(data["last_sequence"], axis=0))
    if SCALE:
        # y_test = np.squeeze(data["column_scaler"]["adjclose"].inverse_transform(
        #     np.expand_dims(y_test, axis=0)))
        prediction = np.squeeze(data["column_scaler"]
                            ["adjclose"].inverse_transform(prediction))
    real_df = data["df"]
    
    # add predicted future prices to the dataframe
    real_df[f"adjclose_{LOOKUP_STEP}"] = prediction
    # add true future prices to the dataframe
    # real_df[f"true_adjclose_{LOOKUP_STEP}"] = y_test
    # sort the dataframe by date
    real_df.sort_index(inplace=True)
    final_df = real_df

    return final_df





def plot_graph(ticker):
    """
    This function plots true close price along with predicted close price
    with blue and red colors respectively
    """

    data = load_data(ticker, N_STEPS, scale=SCALE,
                 lookup_step=LOOKUP_STEP, feature_columns=FEATURE_COLUMNS)

    final_df = get_final_df(model, data)
    final_df = final_df.tail()
    print(final_df)

    plt.plot(final_df['adjclose'], c='b')
    plt.plot(final_df[f'adjclose_{LOOKUP_STEP}'], c='r')
    plt.xlabel("Days")
    plt.ylabel("Price")
    plt.legend(["Actual Closed Price", "Predicted Price after the lastest 1 day line"])
    plt.title( f'{ticker} Predicted Price after 1 day' )
    plt.xticks(rotation=20)
    plt.show()
