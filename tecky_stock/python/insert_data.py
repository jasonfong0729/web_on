# %%
import yfinance as yf
import psycopg2
import os
from dotenv import load_dotenv
import pandas as pd
import schedule
import time
load_dotenv()

# %%
# count is use for control trying time
count=0

def get_stock(cur):
    try:
        cur.execute("SELECT stock_code,id from stocks order by id")
        stocks = cur.fetchall()
        return stocks
    except (Exception, psycopg2.DatabaseError) as error:
        print('SELECT STOCK CODE FALSE')
        print(error)


def insert_daily_prices(cur, ticker, ticker_string):
    #try 5times if error find
    global count
    while True|count<5:
        try:
            print("START DAILY PRICES INSERT METHOD")
            # get data on yfinance
            data = yf.download(
                # insert code of stock
                tickers=ticker_string,

                # use "period" instead of start/end
                # valid periods: 1d,5d,1mo,3mo,6mo,1y,2y,5y,10y,ytd,max
                # (optional, default is '1mo')
                period="10y",

                # fetch data by interval (including intraday if period < 60 days)
                # valid intervals: 1m,2m,5m,15m,30m,60m,90m,1h,1d,5d,1wk,1mo,3mo
                # (optional, default is '1d')
                interval="1d",

                # group by ticker (to access via data['SPY'])
                # (optional, default is 'column')
                group_by='level=0',

                # adjust all OHLC automatically
                # (optional, default is False)
                auto_adjust=True,

                # download pre/post regular market hours data
                # (optional, default is False)
                prepost=True,

                # change to decimal 2
                rounding=True)

            # Date handling
            cur.execute("SELECT MAX(date) FROM stock_daily_price")
            db_latest_date = cur.fetchone()
            date_list = data.index.array

            # insert data into the PostgreSQL
            print(f"INSERTING {len(date_list)*len(ticker)} LINES")
            for stock in ticker:
                frame = data[stock[0]].loc[:]
                frame.loc[:, 'date'] = date_list
                frame.loc[:, 'stock_id'] = stock[1]
                df = frame.values.tolist()

                # insert 200000+ times .....................
                for daily in df:
                    # only insert the latest
                    if db_latest_date[0]!=None:
                        if daily[-2].date()>db_latest_date[0]:
                            cur.execute("""INSERT INTO stock_daily_price(open,close,high,low,volume,date,stock_id)
                            values (%s,%s,%s,%s,%s,%s,%s) """, daily)
                    else:
                        cur.execute("""INSERT INTO stock_daily_price(open,close,high,low,volume,date,stock_id)
                        values (%s,%s,%s,%s,%s,%s,%s) """, daily)

                # try later to make fast
                # records_list_template = ','.join(['%s'] * len(df))
                # args = ','.join(cur.mogrify("(%s,%s,%s,%s,%s,%s,%s)", i).decode('utf-8')for i in df)
                # print(args)
                # insert_query = 'INSERT INTO stock_daily_price(open,close,high,low,volume,date,stock_id) values {}'.format(records_list_template+',')
                # cur.execute(insert_query, args)
            print("INSERT SUSSCESS")
            #reset count
            count=0
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            count=count+1
            print(f'INSERT DAILYPRICES FALSE: {count} time/s')
            continue
        break


def insert_mins_price(cur, ticker, ticker_string):
    #try 5times if error find
    global count
    while True|count<5:
        try:
            print("START MINS PRICES INSERT METHOD")
            cur.execute("DELETE from stock_15mins_price")
            cur.execute("ALTER SEQUENCE stock_15mins_price_id_seq RESTART WITH 1")
            # get data on yfinance
            data = yf.download(
                # insert code of stock
                tickers=ticker_string,

                # use "period" instead of start/end
                # valid periods: 1d,5d,1mo,3mo,6mo,1y,2y,5y,10y,ytd,max
                # (optional, default is '1mo')
                period="5d",

                # fetch data by interval (including intraday if period < 60 days)
                # valid intervals: 1m,2m,5m,15m,30m,60m,90m,1h,1d,5d,1wk,1mo,3mo
                # (optional, default is '1d')
                interval="5m",

                # group by ticker (to access via data['SPY'])
                # (optional, default is 'column')
                group_by='level=0',

                # adjust all OHLC automatically
                # (optional, default is False)
                auto_adjust=True,

                # download pre/post regular market hours data
                # (optional, default is False)
                prepost=True,

                # change to decimal 2
                rounding=True)

            # check date
            date_list = data.index.array

            # insert data into the PostgreSQL
            print(f"INSERTING {len(date_list)*len(ticker)} LINES")
            for stock in ticker:
                frame = data[stock[0]].loc[:]
                frame.loc[:, 'date'] = date_list
                frame.loc[:, 'stock_id'] = stock[1]
                df = frame.values.tolist()

                for mins in df:
                    cur.execute("""INSERT INTO stock_15mins_price(open,close,high,low,volume,date_time,stock_id)
                    values (%s,%s,%s,%s,%s,%s,%s) """, mins)
                # try later to make fast
                # records_list_template = ','.join(['%s'] * len(df))
                # args = ','.join(cur.mogrify("(%s,%s,%s,%s,%s,%s,%s)", i).decode('utf-8')for i in df)
                # print(args)
                # insert_query = 'INSERT INTO stock_daily_price(open,close,high,low,volume,date,stock_id) values {}'.format(records_list_template+',')
                # cur.execute(insert_query, args)
            print("INSERT SUSSCESS")
            #reset count
            count=0
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            count=count+1
            print(f'INSERT DAILYPRICES FALSE: {count}time/s')
            continue
        break


def db_connect(method):
    conn = None
    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(
            database=os.getenv('DB_NAME'),
            user=os.getenv('DB_USER'),
            password=os.getenv('DB_PASSWORD'))

        print('Connecting to the PostgreSQL database...')

        # create a cursor
        cur = conn.cursor()

        # execute a statement
        print('PostgreSQL database version:')
        cur.execute('SELECT version()')
        # display the PostgreSQL database server version
        db_version = cur.fetchone()
        print(db_version)

        # get ticker & id on the PostgreSQL database
        stocks = get_stock(cur)
        ticker = []
        # ticker_id=[]
        for stock in stocks:
            ticker.append(stock[0])
            # ticker_id.append(stock[1])
        ticker_string = " ".join(ticker)

        # run method function
        print('Method run')
        method(cur, stocks, ticker_string)
        # close the communication with the PostgreSQL
        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')


if __name__ == '__main__':
    #auto update data 
    print("SCHEDULE START")
    db_connect(insert_mins_price)
    db_connect(insert_daily_prices)
    schedule.every(10).minutes.do(db_connect,insert_mins_price)
    schedule.every(1).day.at("12:30").do(db_connect,insert_daily_prices)
    while True:    
        schedule.run_pending()
        time.sleep(1)





# %%
