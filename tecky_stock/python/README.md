To run flask app

1. create a virtual environment with commend ""python -m venv myenv""

2. activate virtual environment "myenv\Scripts\Activate.ps1"

3. install required module "pip install -r requirements.txt"
3.1 ta-lib is special to install see the website: "https://blog.quantinsti.com/install-ta-lib-python/#:~:text=To%20install%20Ta%2DLib%2C%20you,all%20there%20is%20to%20it. " 

4. run the web server "flask run" and go to "http://127.0.0.1:5000/"

5. download the stock data "http://127.0.0.1:5000/download"

6. enjoy the ta web