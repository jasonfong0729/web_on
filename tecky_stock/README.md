# tecky_stock


## Enviroment Set up

#### Node
- [ ] Install node modules

``yarn install``
- [ ] Create Database according .env.example 

- [ ] Migration

``yarn knex migrate:latest`` 

#### Python 

1. create a virtual environment with commend ""python -m venv myenv""

2. activate virtual environment "myenv\Scripts\Activate.ps1"

3. install required module "pip install -r requirements.txt"
3.1 ta-lib is special to install see the website: "https://blog.quantinsti.com/install-ta-lib-python/#:~:text=To%20install%20Ta%2DLib%2C%20you,all%20there%20is%20to%20it. " 

#### Database seeding
- [ ] Node 
``yarn knex see:run``
- [ ] Python
``python3 inset_data.py

***

## Usage
###Web App

- [ ] yarn start

- [ ] Go to localhost:8080

### AI prediction and Candle pattern Screener 

In python environment

1. run the web server "flask run" and go to "http://127.0.0.1:5000/"

2. download the stock data "http://127.0.0.1:5000/download"

3. enjoy the ta web]

### Testing 

1. There are three tests unit test integration tests and E2E test 

``yarn test`` 

